3rd party copy (TPC)
===================

## Intro
The key to scalable data distribution is the 3rd party copy (TPC) - passing data
directly from source to destination, bypassing the client. FTS will do this
automatically for protocols with native support, in particular GridFTP and XrootD, 
and for certain storages implementation HTTP TPC is also supported.

## GridFTP
GridFTP extends the standard FTP protocol to provide a high-performance, secure, reliable protocol for bulk data transfer.

GridFTP is a protocol defined by Global Grid Forum Recommendation GFD.020, RFC 959, 
RFC 2228, RFC 2389, and a draft before the IETF FTP working group. Key features include:

 * Performance - GridFTP protocol supports using parallel TCP streams and multi-node 
   transfers to achieve high performance.

 * Checkpointing - GridFTP protocol requires that the server send restart markers (checkpoint) to the client.

 * Third-party transfers - The FTP protocol on which GridFTP is based separates control and data channels, 
enabling third-party transfers, that is, the transfer of data between two end hosts, mediated by a third host.

 * Security - Provides strong security on both control and data channels. Control channel is encrypted by default. 
Data channel is authenticated by default with optional integrity protection and encryption.

The original implementation of the GridFTP protocol ( Server and Client ) was performed by the Globus Alliance. In 2017 they decided to
drop the support of the open source components, which is now under the support of the GridCF community (https://gridcf.org/)

Many storages use the GridCF implementation of the GridFTP server (https://gridcf.org/gct-docs/), 
except for dCache which has a dedicated implementation.

FTS supports GridFTP TPC since its first release via the gfal2 gridftp plugin which internally uses the GridCF implementation 
of the GridFTP client. More info at

https://dmc-docs.web.cern.ch/dmc-docs/gfal2/plugins.html#gfal2-plugin-gridftp

The plugin supports also plain FTP TPC, but for security reason most of the servers have this feature disabled.

## XrootD
The XrootD project (http://xrootd.org) has been developed at SLAC since the early 2000s.

The XRootD software framework is a fully generic suite for fast, low latency and scalable data access, which can serve natively any kind of data,
organized as a hierarchical filesystem-like namespace, based on the concept of directory. 

The framework is used as a building block in other storage implementations like DPM and EOS, and the protocol has been also implemented in dCache.

As GridFTP, XrootD supports natively the TPC protocol (http://xrootd.org/doc/dev49/tpc_protocol.htm) and recently, since version 4.9, it implements 
as well the GSI credentials delegation which perfectly fits with FTS.

FTS integrates XrootD via the gfal2 XrootD plugin ( https://dmc-docs.web.cern.ch/dmc-docs/gfal2/plugins.html#gfal2-plugin-xrootd), which is build 
on top of the Posix-like API provided by the Xrootd client implementation (XrdPosixXrootd and XrdCl)

XrootD TPC with GSI credential delegation is supported starting from FTS v3.8.3 and gfal2 2.16.3

## HTTP
The HTTP specification does not include any provision for 3rd party copy,
however, the DPM and dCache teams first, followed by Xrootd and StoRM, have agreed upon, 
and implemented, an extension, which is supported by the FTS stack, i.e. FTS itself, gfal2 and davix.
More details at:

https://twiki.cern.ch/twiki/bin/view/LCG/HttpTpcTechnical

With gfal2 < 2.15, to instruct FTS to attempt a 3rd party transfer over HTTP,
you must use a special protocol scheme, `davs+3rd`. You could use this for the
source, destination or both.

With gfal2 >= 2.15, there is no need for this, and regular `davs` protocol
scheme will trigger a third party copy. `davs+3rd` remains supported for backwards
compatibility.

For example, to execute a 3rd party push transfer into an object store;

```
$ fts-transfer-submit -s <fts endpoint> davs://source.org/path/file s3://destination.org/path
```

Only one endpoint of the two needs to be TPC enabled, the other can be another
type of grid storage system, or simply a vanilla HTTP server.

Note - you need an RFC proxy for HTTP TPC to work.

### FTS behavior
By default, gfal2 will try to perform first a "pull" third party copy. If this
fails with an error like "operation not supported", then it will try a "push"
copy. Finally, if both fail, it will stream the copy via the client node
(i.e. FTS).

However, gfal2 2.15 introduces the configuration parameter `ENABLE_STREAM_COPY`,
which on an FTS may be disabled. In this case, copies will fail when performed between
two HTTP storages without third party copy support.

The details can be found in the [code](https://gitlab.cern.ch/dmc/gfal2/blob/develop/src/plugins/http/gfal_http_copy.cpp#L545).

**Note:** Errors like "permission denied", "file not found" or similar will
cause a *definitive* failure.

### Versions and configuration of storage systems

Please check the DOMA HTTP TPC wiki

https://twiki.cern.ch/twiki/bin/view/LCG/HttpTpc

### Gfal2 and Davix
FTS uses gfal2 for interactions with storage systems, and for the HTTP protocol,
gfal2 uses Davix. These tools can be used standalone for executing transfers and
investigating problems.

#### gfal2
```bash
gfal-copy davs://storage.org/path/file01 s3://objbkt1.s3.amazonaws.com/file01
```

Use the `-v`, `-vv` or `-vvv` flags for debugging info.

Gfal2 can operate either on a pre-signed URL, or you can configure keys in
`/etc/gfal2.d/http_plugin.conf`.

#### Davix
Davix uses TPC by default.

```bash
davix-cp -P grid --s3secretkey <secret> --s3accesskey <access>
davs://storage.org/path/file01
s3s://objbkt1.s3.amazonaws.com/file01
```

There are various `--trace` options for debugging, e.g. `--trace header`.

The copy mode, pull or push, can be specified with `--copy-mode`.

## How do I know if a storage system supports TPC?

There is no clean way. Try with `gfal-copy -vv` and look for

```
INFO: Trying copying with mode Pull/Push
INFO: Performing a HTTP third party copy
```

In the case of no support, the errors should be as follows;

#### DPM

```
Error: HTTP 405 : Method Not Allowed, Permission refused
```

#### dCache

tbc.

### Push and pull

Early implementations of TPC operated in push mode. Later implementations added
a pull mode for the copies. There may be some storage instances which are
"push-only".

### How can I check if a transfer was performed with TPC?

Since gfal2 2.16 and FTS 3.7, with normal verbosity, the transfer logs will
have an entry similar to

```
INFO    Thu Dec 14 18:00:00 2017; [1513268390000] BOTH HTTP	TRANSFER:TYPE	3rd push
```

The string after `"TRANSFER:TYPE` will let you know what's happening behind:

* `3rd push` The source is pushing the file into the destination
* `3rd pull` The destination is pulling the file from the source
* `streamed` FTS is reading from the source, and writing to the destination

### Performance

Various studies have indicated that HTTP-based transfers can be high-performance.
FTS is able to schedule many simultaneous transfers to compensate for low
bandwidth in a single stream.

However, performance will not necessarily be the same as gridftp
(it may even be better), as the transfer may interact with different protocol
gateways and be subject to different treatment by routers and firewalls.

## What actually happens

FTS will issue a COPY with a special extra header (`Destination:` or `Source:`).
A credential delegation is performed to the active storage system, which
then performs the copy on behalf of the client.

## Macaroons and Scitokens support

Starting from FTS 3.8, it has been added support for 3rd party and streaming HTTP 
transfers using tokens, in particular [Macaroons](https://ai.google/research/pubs/pub41892)
and [Scitokens](https://scitokens.org/).

FTS is making use of an external library (x509-scitokens-issuer-client) for this purpose,
that can be downloaded from the FTS repos, and it has the following behaviour:

   * in case the user submit transfers specifying the Scitokens token issuers for 
     source or destination via the `source-issuer` and `dest-issuer` file metadata,
     it tries to acquire the tokens before running the transfers and use them to 
     contact the storages.
   * in case of http/davs transfers (where the token issuers is not specified) FTS tries 
     to acquire a Macaroon token contacting directly the storage using the delegated
     X509 credentials. If this operation succeds FTS uses the acquired Macaroons to contact
     the storages, otherwise falls back to X509 credentials

A subset of the Storage currently supports Macaroons and Scitokens. 
For Macaroons support has been added to `DPM`, `DCache`,  `XRootD` and `StoRM`, while Scitokens is 
supported by `XRootD`.
