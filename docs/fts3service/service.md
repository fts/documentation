FTS3 CERN ADMIN DOC
==============
This part of the documentation is focused on the operations of the CERN FTS instance, therefore does not apply to a generic FTS installation

# Puppet Infrastructure
## Hostgroups
	
Two puppet hostgroups are defined for the 3 CERN FTS instances

* `fts/pilot/live`, for the Pilot Infrastructure
* `fts/fts3lb/live`, for the LHC Production Infrastructure
* `fts/atlas/live`, for the ATLAS Production Infrastructure
* `fts/daq/live`, for the DAQ Production Infrastructure
* `fts/public/live`, for the FTS Public Infrastructure

The VMs belonging to those hostgroups are in the `production` environment + one for each cluster in the `qa` environment, in order to test changes in the fts or other puppet modules directly.
	
## Modules

As for any other service there are 2 modules:

* FTS module  : https://gitlab.cern.ch/ai/it-puppet-module-fts
* FTS hostgroup module : https://gitlab.cern.ch/ai/it-puppet-hostgroup-fts

### FTS Module 

The FTS module allows the installation and configuration of one FTS server.
By default it installs and configures the following components:
* fts-server
* fts-msg-bulk
* fts-records-cleaner
* fts-!bdii-cache-updater
* fts-bringonline
* httpd
* !bdii and fts-info-publisher

The parameters are mainly configuring the `/etc/fts3/fts3config` and `/etc/fts3/fts3rest.ini` files  with dedicated types:

* `fts3config`
* `fts3restconfig`

for instance one can set the FTS3 port via 

```puppet
'fts3config{'/Port':                value => 9000}'
```

Via module is possible also to configure the version of the FTS packages to install.
These 3 variables are available (also in hiera) :

* `fts3_version`
* `fts3_monitoring_version`
* `fts3_rest_version`

### FTS Hostgroup Module

The FTS Hostgroup module allows the configuration of the 2 clusters, together with the integration with the rest of the CERN infrastructure

In particular it takes care of:

* Teigi integration in order to retrieve secrets  ( e.g. DB Passowrds),
* the installation of DB clients (which depend also on the OS),
* certificate management,
* Lemon metrics,
* DNS Load Balancer configuration,
* Firewall,

#### Teigi integration

Teigi is used in order to store the FTS DB password, each cluster has a dedicated DB with a password retrieved and configured as follows in the manifest:

```puppet
  #Set pass for fts.
  ::teigi::secret::ini_setting{'db_pass':
     section   => '',
     setting   => 'DbPassword',
     path      => '/etc/fts3/fts3config',
     teigi_key => 'db_pass',
     before    => [Service['fts-server'],Service['fts-bringonline']],
  }
```

the password management is done via the Teigi client from the aiadm host:

* to set the DB password for the specific hostgroup:

```bash
tbag set --hg fts/fts3lb db_pass
```

* to get the DB password for the specific hostgroup:

```bash
tbag show --hg fts/fts3lb db_pass

```

#### DB Clients

Mysql or Maria DB client are installed depending on the OS as follows:

```puppet
  $_mysqlpkg = $::operatingsystemmajrelease ? {
     '6'   => 'mysql',
   default => 'mariadb'
  }
```
#### Certificate Management

The certificates needed by the FTS server are automatically requested and renewed via the 'certmgr' puppet module as follows:

```puppet
  $alias = hiera('fts3_host_alias',$::fqdn)
  if $alias != $::fqdn {
    $san = $alias 
  } else {
    $san = undef
  }
  $extra_sans = hiera('fts_extra_sans',[])
  $all_sans   = join(flatten([$extra_sans,$::fqdn,$san]),',')

  certmgr::certificate{"cert_${::fqdn}_${alias}":
     san    => "${all_sans}",
     before => [File['/etc/grid-security/fts3hostkey.pem'],File['/etc/grid-security/fts3hostcert.pem']],
     notify => Service['httpd'] 
  }
```

The certificates is requested with additional SANs that can be speficied via Hiera with the variables 'fts3_host_alias' and 'fts_extra_sans' 

####  Lemon Metrics

FTS3 has some dedicated Lemon Sensor metrics defined via the Lemon Metric Manager interface https://metricmgr.cern.ch:

* `fts_server` : https://metricmgr.cern.ch/metric/13116/
* `fts_bringonline` : https://metricmgr.cern.ch/metric/13258/

Both count the number of server and bringonline process on the host, and 2 derived metrics, which notifies in case the number of processes is < 1 

####  DNS Load Balancer configuration

Both clusters make use of a DNS Load Balancer in order to spread the load to the different cluster hosts.

* `fts3lb.cern.ch` for Production
* `fts3-pilotlb.cern.ch` for Pilot

hosts can be added/removed from the load balancer just changing their hostgroups ( i.e. from fts/fts3lb/live to fts/fts3lb/spare )

The DNS Load balancer client is configured to remove the host from the LB alias upon Lemon metrics value changes:

* /var partition full
* / partition full
* Load average > 12.5
* CPUs count/CPU declared < 1

####  Host and CERN Firewall

Host Firewall openings are configured as follows both for IPV4 and IPV6:

```puppet
 firewall{"100 Allow ${port} access to fts":
    proto  => 'tcp',
    state  => 'NEW',
    dport  => $port,
    action => 'accept'
  }

 firewall{"100 Allow ${port} access to fts ipv6":
    provider => 'ip6tables',
    proto  => 'tcp',
    state  => 'NEW',
    dport  => $port,
    action => 'accept'
  }
```
so that they depends on the configured port.  

CERN external firewall is managed by a dedicated LanDB set:

https://network.cern.ch/sc/fcgi/sc.fcgi?Action=DisplaySet&setId=3487

The set is configured via hiera as follows:

```puppet
cernfw_landbset: cern_cc_fts3
```

so that new host is automatically added to the LanDB set when is deployed.

# Pilot Infrastructure

## Repo

The Pilot infrastructure is installed and updated automatically from the FTS RC repos:

http://fts-repo.web.cern.ch/fts-repo/rc/

## Upgrade

The upgrade in the pilot is performed automatically as soon new versions are available in the repo

## Create a VM
The scripts for the VMs creation can be found in the repo:`it-fts.git`.

There are two scripts:

* SLC6 VM creation: create-fts6-machine.sh
* CC7 VM creation:  create-fts7-machine.sh

Both of them will be created with the keypair fts3ops and the hostgroup by default is fts/pilot/spare.
The fts machines will be created with the prefix fts + number. This number is provided during the execution of the scripts.

For example:

```bash
[marsuaga@aiadm058 it-fts]$./create-fts7-machine.sh 703
```

## Drain a node

The templates for draining and activate a node can be found in the fts hostgroup: it-puppet-hostgroup-fts/code/templates
* drain-fts.sh.erb
* activate-fts.sh.erb
Both templates use curl in order to indicate the drain status and stop or activate lemon. The status in the monitoring can be seen in `https://fts3-pilot.cern.ch:8449/fts3/ftsmon/#/statistics/servers?vo=&source_se=&dest_se=&time_window=144`

## Monitoring-Notifications

* Foreman : https://judy.cern.ch/hosts?utf8=%E2%9C%93&search=%09fts%2Fpilot%2Flive
* Meter hosts : https://meter.cern.ch/public/_plugin/kibana/#/dashboard/temp/AVIXjh_UTThqvmkD4bAk


# Production Infrastructure
## Repo

The Production infrastructure is installed and updated from the FTS repositories:

https://fts-repo.web.cern.ch/fts-repo/fts3-el7.repo
https://fts-repo.web.cern.ch/fts-repo/fts3-depend-el7.repo


## Upgrade

The version of the packages is defined in puppet and there is not automatic update defined

## Create a VM
The scripts for the VMs creation can be found in the repo:`it-fts.git`.

There are two scripts:

* SLC6 VM creation: create-fts6-machine.sh
* CC7 VM creation:  create-fts7-machine.sh

Both of them will be created with the keypair fts3ops.

The hostgroup by default should be `fts/fts3lb/spare`.

The fts machines will be created with the prefix fts + number. This number is provided during the execution of the scripts.

For example:

```bash
[marsuaga@aiadm058 it-fts]$./create-fts7-machine.sh 703
```

## Drain a node

The Draining of a node is needed in case of particular upgrades or dismissal of a node

The templates for draining and activate a node can be found in the fts hostgroup: it-puppet-hostgroup-fts/code/templates
* drain-fts.sh.erb
* activate-fts.sh.erb

Both templates use curl in order to indicate the drain status and stop or activate lemon. The status in the monitoring can be seen in `https://fts3.cern.ch:8449/fts3/ftsmon/#/statistics/servers?vo=&source_se=&dest_se=&time_window=144`

## Monitoring-Notifications

The Production Cluster can be monitored in several ways:

* Foreman : https://judy.cern.ch/hosts?utf8=%E2%9C%93&search=fts%2Ffts3lb%2Flive
* FTS service monitoring : https://fts3.cern.ch/fts3/ftsmon/
* FTS DAQ service monitoring : https://fts3-daq.cern.ch/fts3/ftsmon/
* Meter host metric dashboard: https://meter.cern.ch/public/_plugin/kibana/#/dashboard/temp/AVIXjh_UTThqvmkD4bAk
* Meter notification dashboard : https://meter.cern.ch/public/_plugin/kibana/#/dashboard/temp/AVIXk1lHI9Z3fMbmguUd
* Meter Syslog : https://timber.cern.ch/public/_plugin/kibana/#/dashboard/temp/AVIXl3Hn1znguAIDzX1b

Alarms are disabled by default to avoid tickets to be opened automatically

# DB-On-Demand

Both Pilot and Production FTS clusters rely on MySQL DB-On-Demand for their databases:

https://dbondemand.web.cern.ch/DBOnDemand/

## PilotDB

The pilot db is 'fts3_plt' : https://dbondemand.web.cern.ch/DBOnDemand/instance.zul?instance=fts3_plt

having the following parameters:

* host: itrac5136.cern.ch
* port: 5503
* user: fts3lb
* version: 5.6.17

Automatic Backup is disabled 

## ProductionDB

The Production db is 'fts3' : https://dbondemand.web.cern.ch/DBOnDemand/instance.zul?instance=fts3

having the following parameters:
 
* host: itrac5152.cern.ch
* port: 5503
* user: fts3lb
* version: 5.6.17

Automatic Backup is disabled

## ATLASProductionDB

The ATLAS Production db is 'fts3_atl' : https://dbod.web.cern.ch/instance.zul?instance=fts3_atl

having the following parameters:

* host: dbod-fts3-atl.cern.ch
* port: 5507
* user: fts3atlas
* version: 5.6.17

Automatic Backup is disabled

## DAQDB

The Production db is 'fts3_daq' : https://dbondemand.web.cern.ch/DBOnDemand/instance.zul?instance=fts3_daq

having the following parameters:

* host: itrac5115.cern.ch
* port: 5523
* user: fts3
* version: 5.7.15

Automatic Backup is enabled, everyday at 13:00


# Procedures

## DB Backup

The backup of the LHC production (including the ATLAS specific instance) and Pilot DBs are performed via a custom process (i.e. not using the DBOnDemand backups).

This is to avoid the backup of the whole tables (which is not needed) and the lock the whole database while dumping the tables.

This process has already provoked some incidents on the production instance where the tables kept locked.

The backups are performed nightly from the VM 'fts-backup.cern.ch', by dumping only the configuration tables under the volume 

monuted at /backup.

The DAQ instance instead uses the DBonDemand automatic backups as the size of the DB is rather small.

## Downtime, Interventions (SNOW, GOCDB)

Any service Downtime or Interventions should be reported in SNOW and published in the IT SSB

https://cern.service-now.com/service-portal/ssb.do?area=IT

this means creating a SSB record ( from SSB, Service Report and Alerts->Create SSB Report)

filling up the details of updates, intervention, downtimes

Downtimes must be also declared in the GOCDB

https://goc.egi.eu/portal/index.php?Page_Type=Add_Downtime&site=270

some VOs ( e.g. LHCb ) will be automatically notified then.

## SLS ACRON

SLS Board for IT : https://cern.service-now.com/service-portal/sls.do

It reports the status of the different Services at CERN IT, FTS is one of them:

https://cern.service-now.com/service-portal/service-element.do?name=file-transfer

The update of the FTS SLS is done via acron running as 'fts3ops' user.

The scripts acron runs every 5 min are available in the https://git.cern.ch/web/it-fts.git repo
 
## HAProxy
 
The FTS pilot instance and the production one are configured with 2 instances of HAProxy each.

This deployment is needed in order to implement properly load balancing also with IPV6.

### Pilot

The 2 Haproxy nodes for the pilot are configured under the hostgroup 'fts/pilot/hap' whith 'haproxy' as environment.

The services to contact under 'fts/pilot/live'  are  automatically retrieved via puppet and  the conf is updated accordingly.

The 2 nodes are also configured behind the LB alias fts3-pilot.cern.ch.

### Prod

The 2 Haproxy nodes for the pilot are configured under the hostgroup 'fts/fts3lb/hap' whith 'haproxy' as environment.

The services to contact under 'fts/fts3lb/live'  are  automatically retrieved via puppet and  the conf is updated accordingly.

The 2 nodes are also configured behind the LB alias fts3lb.cern.ch.

###Configuration

In order to monitor the behaviour of the HA Proxy services the 'lemon-sensor-haproxy' package is installed,

and the following metrics/exception  have been configured

* https://metricmgr.cern.ch/metric/33579/
* https://metricmgr.cern.ch/metric/13296/
* https://metricmgr.cern.ch/metric/13573/
* https://metricmgr.cern.ch/metric/13033/

The service logs can be found under '/var/log/haproxy.log'

and the status of the service can be checked with  'systemctl haproxy status'
