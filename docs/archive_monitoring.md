Archive Monitoring
==================

Archive monitoring is a feature of the FTS server where, upon transferring a file to a tape system,
it follows the file's transition from disk buffer to tape. When this feature is enabled, 
the transfer is considered successful only when the file successfully arrives onto tape.

Archive monitoring is enabled for the SRM and XRootD (CERN-CTA) protocols.
The featured is recommended to use when submitting a file to be archived.
For non-tape destination systems, the feature should not be requested, 
as it will result in the submission being marked as failed, even if the transfer took place.

## Enabling archive monitoring

### CLI

Specify the `--archive-timeout <seconds>` parameter with `fts-rest-transfer-submit`. 
The value passed as parameter is the archive monitoring timeout, expressed in seconds.
If the file does not make it onto tape within this timeout, then the transfer is marked as failed.

Note: the `fts-transfer-submit` client does not support this feature 
(the plan is to deprecate the C++ CLI clients)

### REST API

Set the `archive_timeout` parameter to a value > -1.

```json
{
    "files": [],
    "params": {
        "archive_timeout": 86400
    }
}
```

## Enabling destination file report 

> Note: available starting with v3.11

When requesting a file to be archived (`--archive-timeout`), an additional option is available, 
namely the `--dst_file_report`. If the transfer fails because the destination 
file exists, FTS will produce a report about the existing file, 
including information such as file size, checksum and disk/tape residency.

The destination file report is added as a JSON field to the `file_metadata`, 
under the `dst_file` key. The field may be queried either via the REST API.
Alternatively, this information will be part of the monitoring messages
FTS sends to the transfer complete ActiveMQ topic, where a listener 
could look for this field.

**Requesting destination file report**

Submitting via the command line:
```
$ fts-rest-transfer-submit --archive-timeout=86400 --dst-file-report -s https://<fts-server>:8446/ <src> <dst>
<job-id>
````
or via the REST API directly:
```json
{
    "files": [],
    "params": {
        "archive_timeout": 86400
        "dst_file_report": true
    }
}
```

**Checking the destination file report**

```
$ davix-get --cert <proxy> https://<fts-server>:8446/jobs/<job-id>/files | jq .[].file_metadata 
``` 
```json
{
    "dst_file": {
        "checksum_type": "ADLER32",
        "file_on_disk": false,
        "file_on_tape": true,
        "checksum_value": "7e24b403",
        "file_size": 100000000
    }
}
```
