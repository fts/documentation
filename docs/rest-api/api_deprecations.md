API Deprecations
================

The main interface with FTS is done via the REST API. Clients can interact directly
with the REST API using an HTTP client (e.g. curl) or use the FTS clients,
provided either via the `fts-rest-client` RPM or via PyPi ([fts3](https://pypi.org/project/fts3/)).

This page is dedicated to developers interacting with the FTS REST API.
API field / endpoint deprecations or changes will be announced and documented here.

#### Rename of "spacetoken" field in FTS3 module job submission

**Command line clients**

The `--source-token` / `--dest-token` fields were renamed to `--source-spacetoken` / `--destination-spacetoken` in FTS v3.13.0.
This change is effective right away, with no plans to keep the old options around for backwards compatibility.

```diff
-$ fts-rest-transfer-submit --source-token <spacetoken> -s https://fts3-devel.cern.ch:8446/ <src> <dst>
+$ fts-rest-transfer-submit --source-spacetoken <spacetoken> -s https://fts3-devel.cern.ch:8446/ <src> <dst>

-$ fts-rest-transfer-submit --dest-token <spacetoken> -s https://fts3-devel.cern.ch:8446/ <src> <dst>
+$ fts-rest-transfer-submit --destination-spacetoken <spacetoken> -s https://fts3-devel.cern.ch:8446/ <src> <dst>
```

**FTS3 Python module**

The `spacetoken` field was renamed to `destination_spacetoken` in `fts3.new_job(..)`
and `fts3.new_staging_job(..)` methods, found in the FTS3 Python module:

- Renamed `spacetoken` to `destination_spacetoken` in FTS v3.13.0
- A deprecation notice was added in FTS-REST-Client v3.13.1
- The `spacetoken` field will be dropped completely in FTS v3.14.0

FTS-REST-Client v3.13.1 brings backwards-compatibility for the `spacetoken` submission field.
This was done to allow existing software frameworks to continue to work with
the FTS v3.13.x series. Without this fix, any invocation of `fts3.new_job(..)`
or `fts3.new_staging_job(..)` containing the `spacetoken` parameter
would result in a Python syntax error.

The maintainers of such libraries are encouraged to remove the use of `spacetoken`
from their codebase.

Example:
```python
import fts3.rest.client.easy as fts3

fts3.new_job(.., spacetoken=None, ..) # Results in error / deprecation warning for fts3 >= v3.13.1
fts3.new_job(.., destination_spacetoken=None, ..) # Ok
fts3.new_job(..) # Ok
```
