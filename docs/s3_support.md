S3 Support
==========

FTS3 includes support for transfers involving S3 storages.

Submitting these transfers is not different than submitting any other transfer, but getting them to work requires some fine-grained configuration to be done server-side first.

This page starts with some general S3 considerations, as well as examples for submitting clients. The rest of the page is intended to walk FTS instance administrators through configuring and getting S3 transfers to work.

## General Considerations

S3 storages do not authenticate based on x509 proxy certificates and/or tokens. Instead, they rely on signed S3 URLs, which contain the signature in the URL itself.
When the S3 storage is accessed, it will check the URL for the signature, verify it and if valid, return the content for the operation.  If the signature does not pass verification, an access denied message is returned.

The S3 signature is time-bound and considered a secret, which is why signed-URLs **MUST NOT** be submitted directly to FTS. Instead, FTS must be configured with the access/secret key credentials,
so that it can produce signed-URLs when it needs them. This is an important consideration, as it means the FTS instance must be trusted enough for credentials to be shared.

Additionally, S3 storages do not interpret many HTTP verbs we are accustomed to in Grid Storages, such as `HEAD`, `PROPFIND`, `MKCOL` and `COPY` (stat, make directory and HTTP-TPC commands).
As a work-around:

- To find the size of a file, FTS will use the verb `GET` to obtain the size of a file, by parsing the response headers
- To start the transfer, FTS must send the `COPY` command to the other party and instruct it to transfer the file from/to the S3 endpoint
- In case both parties are S3 storages, then FTS will stream the file

Finally, S3 storage space is usually split into buckets, which can be:

1. Domain-based buckets (e.g.: `s3s://bucket.s3.example.org/<file-path>`), where the bucket is a sub-domain, part of the hostname
2. Path-based (e.g.: `s3s://s3.example.org/bucket/<file-path>`), where the bucket is the first element as part of the path

The last relevant concepts are the signature version (S3v2, largely deprecated, and S3v4) and region (very AWS specific).


## Submitting S3 Transfers

Submitting S3 transfers does not differ from any other submission. Whether the transfer will work depends for the most part on the FTS server-side configuration.

Two things are important to specify as the client, namely:

- The copy mode (or in other words, who is the active party in the ThirdPartyCopy)
- The `--strict-copy` option, which instructs FTS to bypass all directory checking/creation and issue directly only the `COPY` command

Submit a ThirdPartyCopy from an S3 storage to a Grid Storage (active party):
```bash
$ fts-rest-transfer-submit --strict-copy -s https://fts3-pilot.cern.ch:8446/ s3s://s3.example.org/bucket/file-path https://eospublic.cern.ch/eos/opstest/dteam/file.100mb
```

Submit a ThirdPartyCopy from a Grid storage (active party) to an S3 Storage:
```bash
$ fts-rest-transfer-submit --strict-copy -s https://fts3-pilot.cern.ch:8446/ https://eospublic.cern.ch/eos/opstest/dteam/file.100mb?copy_mode=push s3s://s3.example.org/bucket/file-path
```

Note the `?copy_mode=push` in the opaque data of the Grid Storage. Adding the `?copy_mode=<push|pull>` opaque data is how the client can specify the ThirdPartyCopy mode:

- A `push` copy mode means the source is the active party
- A `pull` copy mode means the destination is the active party
- The default value is copy-mode `pull`

The `s3s://` protocol is required, which will tell FTS to use TLS connection. Use of simply `s3://` is discouraged.


## Configuration

Before any S3 transfer can be done, the S3 storage must be registered. This can be done either via the REST-API config interface (e.g.: `https://fts3-pilot.cern.ch:8446/config/cloud_storage`) or via the database directly.
This document will follow configuration via the database.

#### Adding a new S3 storage

A new S3 storage must be configured. The S3 syntax is `S3:` + domain part of the URL (e.g.: `https://s3.example.org` --> `S3:s3.example.org`).
```sql
> INSERT INTO t_cloudStorage (cloudStorage_name) VALUES ("S3:s3.example.org");
```

#### Setting access/secret pair and user permissions

The new S3 storage has been added. Now it is time to configure the access/secret pair and user permissions.

Within FTS, each user is identified by its own ID (certificate `DN` or token `sub`), as well as VO data (certificate `VO` + `FQANs` or token `issuer` + `wlcg.groups`).
When configuring a given S3 storage, a mapping is done between `<user-identity>` --> `<s3-credentials-pair>`. This allows the user with the given `<user-identity>` to access the S3 storage with the configured `<s3-credentials-pair>`.

The `<user-identity>` for S3 configuration is composed of either the user ID (certificate `DN` or token `sub`) or the VOMS data (certificate `FQANs` or token `wlcg.groups`). The exact values can be verified via the `/whoami` endpoint:
```bash
# Obtain user ID value
$ davix-get --cert ${X509_USER_PROXY} https://fts3-pilot.cern.ch:8446/whoami | jq .user_dn

# Obtain VOMS data value (merge results into a single line)
$ davix-get --cert ${X509_USER_PROXY} https://fts3-pilot.cern.ch:8446/whoami | jq -r .voms_cred[] | tr '\n' ' ' | xargs
```

To identify the user, you will need to provide both the user ID and user VOMS data. A `*` is permitted, either to replace the user ID or the VOMS data.
Example configuring (`<user-identity>`, `<s3-credentials-par>`) pairs:
```sql
> INSERT INTO t_cloudStorageUser (cloudStorage_name, user_dn, vo_name, access_token, access_token_secret) VALUES ("S3:s3.example.org", "<user_dn>", "<voms_cred>", "<access_key>", "<secret_key>");
> INSERT INTO t_cloudStorageUser (cloudStorage_name, user_dn, vo_name, access_token, access_token_secret) VALUES ("S3:s3.example.org", "<user_dn>", "*", "<access_key>", "<secret_key>");
> INSERT INTO t_cloudStorageUser (cloudStorage_name, user_dn, vo_name, access_token, access_token_secret) VALUES ("S3:s3.example.org", "*", "<voms_cred>", "<access_key>", "<secret_key>");
```

#### Configuring Gfal2 settings

By this point, the FTS + S3 credentials have been configured. Final thing to configure is whether the storage needs to use domain-based or path-based buckets, as well as S3v2 or S3v4 signatures.
Unfortunately, these cannot be configured via the FTS database. Instead, this configuration needs to be applied at the Gfal2 layer.

Gfal2 is configured via files stored under the `/etc/gfal2.d/` directory. These files follow an `.ini` like format.

We suggest creating a new file `/etc/gfal2.d/s3.conf`. For each S3 storage that needs configuration, add a new section, where the section title contains the storage hostname:

- Bucket access is controlled via the `ALTERNATE=<true|false>` setting, where `true` means path-based access and `false` is for sub-domain-based access (default `false`)
- S3v2 signature is used by default. If S3v4 is needed, this is enabled automatically if the S3 storage configuration in Gfal2 contains the `REGION=<region>` setting

Example (S3 storage, path-based access, S3v4 signature):
```ini
[S3:S3.EXAMPLE.ORG]
REGION=<region>
ALTERNATE=true
```


## Verifying your S3 transfer works

Now that all the configuration is done, it's time to verify that you can properly access and transfer your data. We will follow a bottoms-up approach,
starting from the low-level HTTP client Davix and working our way to Gfal2 access, then finally to FTS.

#### Checking Davix access

The following command downloads an S3 file via Davix:
```bash
$ davix-get --s3accesskey <access-key> --s3secretkey <secret-key> --s3alternate [--s3region <region>] --trace s3,header s3s://s3.example.org/bucket/file-path > /tmp/file.s3
```

Uploading a file via Davix:
```bash
$ davix-put --s3accesskey <access-key> --s3secretkey <secret-key> --s3alternate [--s3region <region>] --trace s3,header /tmp/file.upload s3s://s3.example.org/bucket/file-path
```

Start a transfer between Davix and the S3 storage:
```bash
$ davix-cp --cert ${X509_USER_PROXY} --s3accesskey <access-key> --s3secretkey <secret-key> --s3alternate [--s3region <region>] --copy-mode pull --trace s3,header s3s://s3.example.org/bucket/file-path https://eospublic.cern.ch/eos/opstest/dteam/file.s3
```

If the above commands didn't work, play with the `--s3alternate` and `--s3region` settings. If you are sure you are using the correct values, then check the root cause with the S3 storage.

#### Checking Gfal2 access

Time to turn the successful Davix command into a Gfal2 equivalent. The example will cover only the transfer part:

```bash
$ gfal-copy --cert ${X509_USER_PROXY} --just-copy \
    -D"S3:S3.EXAMPLE.ORG:ACCESS_KEY=<access-key>" \
    -D"S3:S3.EXAMPLE.ORG:SECRET_KEY=<secret-key>" \
    -D"S3:S3.EXAMPLE.ORG:ALTERNATE=true" \
    [ -D"S3:S3.EXAMPLE.ORG:REGION=<region>" ] \
    -vvv --log-file=/tmp/gfal2.log \
    s3s://s3.example.org/bucket/file-path https://eospublic.cern.ch/eos/opstest/dteam/file.s3?copy_mode=pull
```

Notice the `--just-copy` option and `?copy_mode=pull` opaque query.

If the above command works, let's move the Gfal2 configuration to a config file:
```bash
$ cat /etc/gfal2.d/s3.conf
[S3:S3.EXAMPLE.ORG]
ACCESS_KEY=<access-key>
SECRET_KEY=<secret-key>
ALTERNATE=true
REGION=<region>
```

Time to repeat the same command:
```bash
$ gfal-copy --cert ${X509_USER_PROXY} --just-copy -vvv --log-file=/tmp/gfal2.log s3s://s3.example.org/bucket/file-path https://eospublic.cern.ch/eos/opstest/dteam/file.s3?copy_mode=pull
```

#### Checking FTS submission

Finally, if the Gfal2 copy command worked, it's time to move to the FTS submission:
```bash
$ fts-rest-transfer-submit --strict-copy -s https://fts3-pilot.cern.ch:8446/ s3s://s3.example.org/bucket/file-path https://eospublic.cern.ch/eos/opstest/dteam/file.s3?copy_mode=pull
```

If everything worked correctly, after some time, your transfer should have finished successfully:
```bash
$ fts-rest-transfer-status -s https://fts3-pilot.cern.ch:8446/ <job-id>
$ fts-rest-transfer-status --json -s https://fts3-pilot.cern.ch:8446/ <job-id> | jq .files[].file_state
```


## Known Limitations

- It is acknowledged that the configuration steps require an FTS admin and may seem too complex at times
    - Correctly mapping users is tricky
    - The Gfal2 configuration is very low-level and should be hidden by FTS
- The REST-API config interface (`/config/cloud_storage`) text descriptions and fields don't match with what is expected
- There is the limitation that FTS needs to store the S3 credentials in its database
- There is no way to have the same user access path-based buckets with separate S3 credentials on the same S3 storage
