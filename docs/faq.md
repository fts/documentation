FAQ
===

### The transfer starts, but fails with some SSL error
HTTP third party transfers require [RFC proxies](http://www.rfc-base.org/rfc-3820.html)
Try creating your proxy with the option --rfc

Please, note that the transfer may remain failing until the already
non-rfc delegated proxy expires, as they will have the same delegation id.


## Who can apply various configuration rules in FTS3?
Anyone with a valid proxy from one experiment with "Role=production"

## How can I slow down traffic to/from my storage?
You can either cap the maximum number of allowed transfers.
See [Link ranges](../fts-rest/docs/config_alternatives.md#link-ranges) for more information on how to do this.

## Why do I need to specify the protocol when I apply a rule to FTS3?
Because fts3 treats `srm://mystorage.cern.ch` differently from `gsiftp://mystorage.cern.ch`,
each endpoint is considered as unique.

##One of my storage endpoints started failing really badly, how can I debug FTS3 transfers?
You can issue `fts-set-debug -s  https://{FTS3_ENDPOINT}:8443 srm://mystorage.cern.ch (0|1|2|3)`
to enable/disable debug logging in FTS3 for the given endpoint.
Logs can be collected from the web monitoring. You will also find a file with extension
`*.debug` which contains information received from GridFTP control channels
(if, of course, the transfer is gsiftp)

