Managing Timeouts
=================

Various operations which FTS performs are governed by timeouts, some of which are accessible to the client and others of which are configured on the FTS server or on the storage systems.

# FTS Client

## CLI

With `fts-rest-transfer-submit`

* **--timeout** : After this number of seconds running, the transfer will be aborted.
* **--bring-online** : After this number of seconds, the staging operation will be cancelled, and the transfer will not take place.
* **--archive-timeout** : After this number of seconds, the archive monitoring operation will be canceled. The transfer is marked as `FAILED`, however actual writing to tape may very well go through (FTS won't abort the write-to-tape operation)

## REST Interface

### Per transfer

The job submission json file allows you to set the following parameters:

*  **"max_time_in_queue"**  : After this number of hours on the queue, the transfer will be cancelled. A suffix such as 's', 'm' or 'h' is accepted.
*  **"timeout"** : After this number of seconds running, the transfer will be aborted.
*  **"bring_online"** : After this number of seconds, the staging operation will be canceled, and the transfer will not take place.

### General or VO configuration

The server configuration json file allows you to set the following parameters:

* **sec_per_mb** : When a transfer has no timeout set, FTS will calculate it based on the file size, multiplied by this factor, plus an offset to account for metadata and checksum operations.
* **global_timeout** : Timeout to use if the transfer does not specify one. Leave it to 0 to let FTS calculate the timeout from the file size.
* **max_time_in_queue** : max time for a job to remain in the queue, in hours.

Note that the REST interface also offers a graphical interface for setting these parameters.

# FTS Server

## FTS3

FTS3 has a transfer inactivity timeout which triggers if the fts_url_copy process has not given a sign of life for a certain time.

`/etc/fts3/fts3config`

```
# Check for fts_url_copy processes that do not give their progress back
CheckStalledTransfers = true
# Stalled timeout, in seconds
CheckStalledTimeout = 900
```

## gfal2

The following are in `/etc/gfal2.d/gfal2_core.conf` and may be overridden on a per-protocol basis.

* **NAMESPACE_TIMEOUT=300** : Namespace operations timeout in seconds.
* **CHECKSUM_TIMEOUT=1800** : Checksum timeout in seconds.

### gsiftp specific options

* **OPERATION_TIMEOUT** : Overrides NAMESPACE_TIMEOUT
* **CHECKSUM_CALC_TIMEOUT** : Overrides CHECKSUM_TIMEOUT
* **PERF_MARKER_TIMEOUT** : Abort transfer if no performance markers have been received in this time, or if perf markers indicate 0MB/s.

# Proxies and VOMS extensions

The proxy which the client delegates to FTS will eventually expire. The proxy typically also contains VOMS extensions (an Attribute Certificate) which assert the VO membership of the user - these extensions have their own timeouts which are independent of the proxy itself. The expiry of either of these credentials will cause transefers to fail. You can check this info with `voms-proxy-info`.

## When acquiring a proxy with `voms-proxy-init`

* **--hours** : proxy validity
* **--valid** : proxy and VOMS extension validity

The VOMS server used will have a maximum validity configured and may shorten the lifetime of the extensions.

## When delegating to FTS

Submitting a job to FTS triggers a proxy delegation if there is no existing delegation, or if the existing delegation will expire within 6 hours (this value is not configurable, see [ProxyCertificateDelegator.h](https://gitlab.cern.ch/fts/fts3/blob/develop/src/cli/delegation/ProxyCertificateDelegator.h#L95)).

The delegated proxy lifetime can be specified with 

`fts-rest-transfer-submit -e <lifetime in minutes>`

A new proxy can be delegated explicitly with `fts-rest-delegation-init`.

# Storage systems

## GridFTP

GridFTP servers have a range of timeouts which can be tuned. This is typically done through the file `/etc/gridftp.conf` or similar. Check the "Timeouts" section.

Of particular importance is the `control_idle_timeout` which should be set to at least `7200` (2 hours), otherwise the control connection may drop before the transfer finishes, causing problems for subsequent operations such as checksums.
