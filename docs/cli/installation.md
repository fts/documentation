Installation
============

Starting from version 3.12.0, Python2 support was dropped and the clients 
can only be found in Python3. The supported OS is RHEL7, but give the Python 
nature of the clients, they are expected to run on other platforms as well. 

### Install using RPMs

The clients are provided via the `fts-rest-client` package. We recommend installing it from the EPEL repository. They can be found
in EPEL7, EPEL8 and EPEL9.

Alternatively for EL7 platforms, they can also be installed from the FTS Production repository.

```ini
[fts3-el7]
name=FTS3 Production
baseurl=https://fts-repo.web.cern.ch/fts-repo/el7/$basearch
gpgcheck=0
enabled=1
protect=0
```

### Install using [pip](https://pypi.org/project/fts3/)

Starting from version 3.12.0, the clients can be installed from PyPI using `pip`. 
Some system packages will be required for `M2Crypto` (gcc, swig, openssl-devel, python3-devel). 
Please make sure you have them installed before starting the installation with `pip`.

```bash
virtualenv fts
source ./fts/bin/activate
pip intall --upgrade pip
pip install fts3
fts-rest-whoami -s "https://fts3-devel.cern.ch:8446"
User DN: /DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=ftssuite/CN=773756/CN=Robot: fts3 testsuite
VO: dteam
Delegation id: xxxxxxxxxxxxxxxx
```

Usage
-----
The usage documentation is inside the [cli](cli/README.md) subdirectory.

Bulk submission
---------------
You can also check everything you can do with the [bulk format](bulk.md).

*Hint*: Try `--dry-run` to see what would be sent to the server
