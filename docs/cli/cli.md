Command Line Tools
==================

## General
FTS transfers are managed via `fts-rest-transfer-*` commands.
The main commands are:

  1. `fts-rest-transfer-submit`
  2. `fts-rest-transfer-status`
  3. `fts-rest-transfer-list`
  4. `fts-rest-transfer-cancel`

> Note: there are another set of clients that start with `fts-transfer-*`.  
> These are the C++ clients, which are deprecated in favor of the Python3 clients.

## Proxies
You will need a valid proxy in order to use these commands.

On first use, the `fts-rest-transfer-submit` command will automatically delegate your
proxy to the FTS server (default lifetime is 12 hours).

When the remaining lifetime of the stored proxy passes under 2 hours,
`fts-rest-transfer-submit` will automatically delegate a new one.

## Querying a transfer
The following example shows a query to FTS to obtain information about the state of
a transfer job:

```bash
$ fts-rest-transfer-status \
    -s https://fts3-pilot.cern.ch:8446 \
    ebcdefa0-0440-11ed-b308-fa163ec6a1a1

Request ID: ebcdefa0-0440-11ed-b308-fa163ec6a1a1
Status: FINISHED
Client DN: /DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=ftssuite/CN=773756/CN=Robot: fts3 testsuite
Reason: 
Submission time: 2022-07-15T13:20:40
Priority: 3
VO Name: dteam
```

You may increase the verbosity level of the transfer report with the `-v` option.

The following example allows to query ongoing data transfers on a given FTS service, 
allowing filtering by VO or User DN.

```bash
$ fts-rest-transfer-list -s https://fts3-pilot.cern.ch:8446
```

## Submitting a transfer
The user can submit a transfer by specifying the source-destination pair
in the command line:

```bash
$ fts-rest-transfer-submit -s https://fts3-pilot.cern.ch:8446 \
    https://eospublic.cern.ch/eos/opstest/dteam/test.file
    https://eospps.cern.ch/eos/opstest/dteam/destination.file

c2e2cdb1-a145-11da-954d-944f2354a08b
```

Alternatively, a more sophisticated bulk submission file in JSON format may be used:

```bash
$ fts-rest-transfer-submit -s https://fts3-pilot.cern.ch:8446 \
    --file submission.json
```

where the input file `submission.json` looks like:

```json
{
  "files": [
    {
      "sources": [
        "https://eospublic.cern.ch/eos/opstest/dteam/test.file"
      ],
      "destinations": [
        "https://eospps.cern.ch/eos/opstest/dteam/destination.file"
      ],
      "checksum": "ADLER32"
    }
  ],
  "params": {
    "overwrite": true
  }
}
```

You can use the `--dry-run` option to instruct the FTS client to print the JSON request 
it would instead of sending it to the server. For more information, please visit 
the [bulk transfers](../../fts-rest/docs/bulk.md) section.

## Cancelling a transfer
An example of cancellation of a previously submitted data transfer job is shown here:

```bash
$ fts-rest-transfer-cancel -s https://fts3-pilot.cern.ch:8446 c2e2cdb1-a145-11da-954d-944f2354a08b
```

## Client installation
Please refer to [Installation](installation.md) on how to install the FTS clients.

## Support
For support please create a [GGUS](https://ggus.eu) ticket or email fts-support-AT-cern.ch.
