Releases
========
You can check the [release notes](https://fts.web.cern.ch/fts/releases.html) on our web.

You can find FTS3 in four different repositories:
EPEL, EPEL-Testing, Prod, RC and Devel (a.k.a Testing). If you want to know which version is where,
you have a [version matrix](http://fts3-docs.web.cern.ch/fts3-docs/versions/) available.

The release cycle goes as follows:

Devel => RC => Prod => EPEL-Testing => EPEL

There may be additional repositories for additional branches,
but normally you do not need to care about them.

## [Devel (or Testing)](https://fts-repo.web.cern.ch/fts-repo/fts3-devel-el7.repo)
Latest and greatest, but also unstable. You can find it running in fts3-devel.cern.ch
New features go here first.
You can find it running on `fts3-devel.cern.ch`

## [RC](https://fts-repo.web.cern.ch/fts-repo/fts3-rc-el7.repo)
Contains release candidates. The latest version available here is installed on
`fts3-pilot.cern.ch`, where it is validated. It is safer than devel, but might break when
devel is merged into RC.

Hotpatches for the production version are commited here if RC and Prod are the same.

## [Prod](https://fts-repo.web.cern.ch/fts-repo/fts3-el7.repo)
Contains stable releases, ready to run in production.
You can find it running on `fts3.cern.ch`.
This repository is populated via an `rsync` from the RC repo once it is blessed. Therefore, it
normally contains artifacts that have run already on pre-production.

## EPEL-Testing
Releases to EPEL need to go first to EPEL-Testing. Contains same
version, or slighly older version, as Prod.

## EPEL
After 15 days at least, the version from EPEL-Testing can be pushed to EPEL-Stable.

