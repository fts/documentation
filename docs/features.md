Features
========

## Out-of-the-box
FTS3 is easy to install and run.
You install the RPMs, create the MySQL database, and FTS3 is ready to go.

## Endpoint-centric
In FTS2, there were channels: an abstract link between two SE-s.
Channels had to be added and configured manually before even being able to run transfers.
This configuration was static and honoured regardless of the storage or network load level.

FTS3 doesn't need configuration. It just works as soon as it starts, and automatically
adjust the load based on the storage responsiveness, failure type and rate.

Of course, you can configure a specific endpoint or pair (a pair configuration would be
similar to the former channel concept), but FTS3 will continue to just work with non-configured
endpoints all the same.

## Multiprotocol
FTS3 builds on top of GFAL2, a new abstraction library that hides away - all that is possible -
underlying protocol details.

This means that FTS3 is capable of running a transfer as long as there is a GFAL2 plugin
available for the protocol. FTS3 doesn't particularly care if you are using GridFTP,
XRootD, SRM or HTTP.

## OIDC (starting from FTS 3.10)
FTS3 supports OpenID Connect based authentication, delegating the user authentication to an IAM infrastructure, 
for example [XDC IAM](https://iam.extreme-datacloud.eu/) or [WLCG IAM](https://wlcg.cloud.cnaf.infn.it/). 
The user needs supplies FTS3 with an authentication token provided by an IAM service.

If FTS3 is configured to recognize that provider, it will validate the token, either offline or online
and grant access. Behind the scenes, FTS generates a refresh token and an access token which will be used
at the time of the transfer.

## QoS (starting from FTS 3.10)
FTS3 supports QoS management on CDMI-enabled storages. The appropriate option has to be specified
when using the CLI and FTS3 will ensure that the resources specified will have the appropriate
QoS class when the operation finishes.

Note: For the moment, QoS management is done via the CDMI-QoS interface and relies on the Gfal2 QoS API.
As the topic gains interest within WLCG, FTS will evolve its QoS interface to meet the new requirements.
More documentation the [QoS Service page](staging/qos-service.md#qos-transitions).

## Multidimensional scheduler
Without configuration, FTS3 takes care of doing a fair-share between VOs that share a given
link.

Still, VOs can override FTS3 default behaviour in multiple ways:

  * VO shares allow to change the fair share, and give more slots to a given VO
  * Activity shares, within a VO, allow to divide the assigned slots according to
weights decided by the VO. 
  * Priorities allow to reshuffle, within a VO share and an activity share, particular
jobs, giving them a boost.

For instance, let's say FTS3 decides there are 100 available slots for a given pair.
It can be configured to give 80 to VO A, and 20 to VO B. Then, if VO A uses activity shares,
those 80 may be divided as 50 for express transfers, 20 for medium priority, and 10 for low
priority.
Inside the low priority, a job with priority 5 will go before a job with priority 2.

Please, note that VO shares and activity shares are designed and implemented to avoid
starvation: shares with lower weight will go through the queue slower, but they will
be processed.

Priorities can cause starvation. If you keep submitting jobs with priority 5, job with lower
priority will stay in the queue forever. Use priorities with caution.

## Reusable API's
FTS3, via its [REST API](../fts-rest/docs/api.md), allow the system to be integrated
into other frameworks, scripts or applications, so it can be basically used as a building
block of bigger systems.

Its optional support for OAuth authentication also allows to integrate it within
third party Web Applications.

You can also use the [messaging](messaging.md) for retrieving terminal statuses
by your backend asynchronously.

## Web Monitoring
Also, out of the box, FTS3 provides a nice Web interface to monitor the behaviour of your
instance. This monitoring facilitates debugging issues, seeing how the queue goes,
get some basic statistics, etc.

Please, note this monitoring is not intended for long-term statistics. It is just a window
over FTS3 internal status.

![Web Monitoring](monitoring.png)

In any case, the backend and frontend are decoupled, so you can consume the JSON
responses by your own monitoring probes.

## Web Configuration
Although normally most FTS3 run without configuration, or with as little as possible,
we provide a Web interface to make easier tinkering with FTS3 internal configuration,
visually and via browser.

Since browsers do not normally work with proxies, and, therefore, there are no VO
extensions, in order to use the Web Configuration interface you will need to grant
explicitly permission to your DN (or whoever DN) to perform configuration actions.

The DN needs to be configured on the DB:

insert into t_authz_dn values('DN', 'config');

Then you (or the admin) will be able to use the Web Configuration and add other DNs from the
'Static authorization' tab.

![Web Configuration](configuration.png)

