For developers
==============
This chapter of the documentation is targeted at FTS3 developers and
contributors.

Of course, you are welcome to go through it even if you do not plan to commit
any code to the service!

