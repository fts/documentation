Optimizer
=========
One of FTS3 killer features is its *zero configuration required*, and this is
possible thanks to the Optimizer.

This piece of logic, just at the heart of FTS, makes possible to run transfers
between any two random endpoints with good reliability and performance with
no, or very little, manual intervention.

Let's see how it works.

## Core ideas
Even if FTS had detailed knowledge of the network topology and available
resources, it still would have to compete with other systems for the same
resources, and it would have to react to changes on service availability.

For instance, it could happen that a storage was under heavy load due to
experiments' analysis jobs, or due to transfers done directly by a user,
unplanned downtime, etc.

So, given that it is hard to know, and model, all that can go wrong (or right),
FTS Optimizer goes to a more empirical direction, somewhat similar to the TCP
congestion control algorithms.

Normally, to maximize throughput on a WAN link we want to keep a number of
parallel connections. Increasing the parallelism, up to a level, improves
throughput, but beyond that we can introduce too much
overhead, and the throughput decreases again.

## Improve throughput
The [bandwidth-delay product](https://en.wikipedia.org/wiki/Bandwidth-delay_product)
basically gives us how many bytes can be on the network on a given moment.
Saturating the network means always having the link transferring something.

For networks with high latency and bandwidth, the number of bytes that "fit"
on the link can be quite high. Imagine a 10Gb/s link, with an RTT of 140ms.

```
B * D = (10 * 10^9 bits/s) * (140 * 10^-3 s)
B * D = 140 * 10 ^ 7 bits = 1.4 * 10 ^ 9 bits
B * D = 1.4 Gbits
B * D = 175 MBytes (M = 10^6)
```

To fill the pipe with a single connection, the source storage element would
need to have a TCP buffer size of 175 MBytes (and a file big enough to keep
it full).

Here is a plot showing the effect of a bigger buffersize.

![Effect of buffer size](buffersize.png)

There are also other factors at play: TCP takes a while to
[ramp up](https://en.wikipedia.org/wiki/TCP_congestion_control#Slow_start),
specially with such high latency. Also, if there were packet loss, each drop
means halving the achieved throughput and ramping up linearly again.

That's why parallel connections are used. If we had 10 connections,
each one only needs 17.5 MBytes of buffer size, packet loss effects
are minimized, and the ramping up is faster.

Here is another example showing the effect of parallel streams.

![Effect of multiple streams](streams.png)

In all cases the maximum is reached, but the more streams, the faster
it converges.

It is important to note that Linux systems have a default maximum buffer size,
which would determine, before hand, the number of connections we theoretically
need to fill the pipe. For a value of 4MB (assuming the default max TCP buffer
size for RHEL6), for our previous example 44 connections should be enough
(assuming no packet loss, files big enough to have time to ramp up, etc.).

However, if we transferred one file with 44 parallel streams we would be
giving trouble to the hard drives on both ends of the transfers. Besides,
memory used by the kernel for these connections can *not* be swapped.

It is easy to see, then, that having 44 files in parallel is likely better,
since the storages will have the chance to spread them evenly between different
disk nodes / gateways.

**In summary**, the FTS Optimizer increases the number of parallel transfers
to maximize the network usage between two storages.

## Avoid overload
On the other hand, each additional connection consumes resources on both
storages and network link. If we kept increasing the number of connections,
we may suffer a performance degradation due to the overhead or to resource
exhaustion (i.e. connections or memory).

Of course, we also need to consider that FTS is not the only user of the
storages and the network, so external processes can cause a limitation on
the resources available for the transfers.

There are two symptoms that FTS looks for to detect these conditions:
an increase of "recoverable" failures, and a decrease of throughput.

A recoverable failure is an error potentially caused by resource exhaustion.
For instance, an aborted connection, a timout, etc. Generally speaking, a server
or networking error.

A non recoverable failure is an error bound to the task itself. For instance,
an access denied, or a file not found. Retrying the transfer will not help.

**In summary**, the FTS Optimizer decreases the number of parallel transfers
when there is an increase of recoverable errors, or the throughput worsens
(both potential symptoms of saturation).

## Working range
With FTS 3.5 we introduced the concept of "working range" on the optimizer.
The working range is just the minimum and maximum number of transfers allowed
to run on a given link, and the optimizer will work inside those limits.

When there is no configuration, the low limit is always 2 (or 10 for LAN).
The upper limit is the minimum value between the maximum configured for the
source, the destination, and the link (using global defaults if not specifically
set).

If there is a range configured, then whatever is configured is used as limiting
factors.

Reasons to put limits are:

  * **Maximum** To give room to other processes/users that may be using the resources.
  * **Minimum** Sometimes FTS can be overly conservative when a storage or link
    has trouble (akin to TCP halving the throughput on packet losses). In some
    cases the link is known to be able to perform better than that, and FTS'
    conservatism makes it run at 100% success rate, but slower than it should,
    so underlying networking or storage issues are hidden when they should be
    blatant.

### Optimizer
The optimizer algorithm is as follows:

1. Take a sample of throughput and success rate (excluding non recoverable errors)
2. Calculate the throughput [exponential moving average](https://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average) (EMA)
  * This is done to reduce the sensitivity to fluctuations
2. Get the working range
  * If min == max, exit and take that value
3. If the throughput is higher than any throughput limitation for source and destination
  * Decrease actives
4. If the success rate is low (< 97%)
  * If improving, keep actives
  * If worse or the same, decrease actives
5. If the success rate is worsening
  * Decrease actives
6. If the success rate is 100%, or more than 97% and improving
  * If the new throughput EMA is higher than the previous one, increase actives
  * If the new throughput EMA is lower than the previous one
    * If the average file size has decreased, increment actives
    * Compare new and previous EMA rounding their logarithm
    * If smaller, decrease actives
7. Adjust number of actives so it is within working range

**Note:** There may actually be less actives scheduled if the storage happens to
have slots consumed on other links.

For instance, for a decision of 50 from A to B, and a storage limit of 60 for A,
if A to C has 30, A to B won't go beyond 30.

## Examples
Let's assume the following configuration
  * Global storage limit: 100
  * Global link limit: 60
  * `srm://srm-atlas.cern.ch` has a storage limit of 30

| Source                         | Destination                       | Range  |
|--------------------------------|-----------------------------------|--------|
| `gsiftp://eosatlasftp.cern.ch` | `srm://srm-atlas.cern.ch`         | N/A    |
| `gsiftp://eosatlasftp.cern.ch` | `srm://mgse1.physik.uni-mainz.de` | 50/150 |
| `gsiftp://eoscmsftp.cern.ch`   | `srm://pcncp22.ncp.edu.pk`        | 70/70  |

### Working ranges
#### eosatlasftp to srm-atlas
Since there is no range configured, the globals will be picked.

* It is a LAN transfer, so min = 10 (otherwise it would be 2).
* For the maximum, we pick `min(source limit, dest limit, link limit) => min(100, 30, 60) => 30`

Therefore, the optimizer will play between 10 and 30.

#### eosatlasftp to mgse1.physik.uni-mainz.de
The value as configured: between 50 and 150.

#### eoscmsftp.cern.ch to pcncp22.ncp.edu.pk
The value as configured: between 70 and 70. This obviously means there is only
a possible value: 70. This is equivalent to the concept of "fixed actives" in
FTS versions previous to 3.5.

The optimizer will not run for this one, since there isn't much choice.

### Optimizer
Via the web monitoring, it is possible to see the historical evolution of the
optimizer for a given pair.

![Monitoring view](monitoring_view.png)

On the top of the page there are three plots:

* The first one shows the throughput sample (blue), and the throughput
exponential moving average.
(green) which softens the measure peaks, and the optimizer decision (red).
* The second one shows the success rate (green) and the optimizer decision (red).
* The third one shows the throughput EMA (green) and the average file size with
the standard deviation (blue).

The table shows the same information textually, and the explanation for
the decision taken by the optimizer.

In this example we can see that at the beginning the success rate was low,
so the optimizer decreases the number of actives in each iteration.

Once the success rate recovers to 100%, the optimizer inverts
the trend, and grows the number of actives. Since the throughput also increases,
during the following iterations the optimizer increases slowly the number of
transfers until the grow tendency changes and degrades. In that moment, the
optimizer starts backing off again.

### Optimizer tuning
The optimizer is auto-tuned based on the performance history of the source-destination link. You could experiment with different auto-tuning plans by changing the optimizer mode to any of the following values:

* 1: The default value, conservative mode: adjusts the transfer concurrency, number of TCP streams beased on the file size.
* 2: More aggressive: experiments with number of TCP streams (1-16)
* 3: Even more aggressive.
