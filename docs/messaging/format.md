Messaging format
================
FTS3 generates four types of messages: start messages, completion messages, state change messages and optimiser decision messages.

## Start messages
They are sent immediately before the actual copy is issued to the remote storage.
The message has a JSON format:

```javascript
{    
	"agent_fqdn": "FTS3 host that is running the transfer",
	"transfer_id": "YEAR-MONTH-DAY-HOURMINUTE__sourcese__destse__file_id__job_id",
	"endpnt": "FTS3 endpoint",
	"timestamp": 0, // Start time, in milliseconds
	"src_srm_v": "Source SRM version, always 2.0 if srm is used",
	"dest_srm_v": "Destination SRM version, always 2.0 if srm is used",
	"vo": "Virtual Organization",
	"src_url": "Source URL",
	"dst_url": "Destination URL",
	"src_hostname": "Source hostname",
	"dst_hostname": "Destination hostname",
	"src_site_name": "", // Always empty
	"dst_site_name": "", // Always empty
	"t_channel": "source_protocol://source_host__dest_protocol://dest_host",
	"srm_space_token_src": "Source space token, if any",
	"srm_space_token_dst": "Destination space token, if any",
	"user_dn": "User that submitted the job",
	"file_metadata": "File metadata set by the user at submission",
	"job_metadata": "Job metadata set by the user at submission"
}
```

## Completion messages
They are sent immediately after a transfer has finished, either successfully or not.
Please, note that they may also be sent without an opening start message is
the job has been canceled, force-failed,...

```javascript
{
	"tr_id": "YEAR-MONTH-DAY-HOURMINUTE__sourcese__destse__file_id__job_id",
	"job_id": "job_id" // (starting with v3.12.0)
	"file_id": 123456, // (starting with v3.12.0)
	"endpnt": "FTS3 endpoint",
	"src_srm_v": "Source SRM version, always 2.0 if srm is used",
	"dest_srm_v": "Destination SRM version, always 2.0 if srm is used",
	"vo": "Virtual Organization",
	"src_url": "Source URL",
	"dst_url": "Destination URL",
	"src_hostname": "Source hostname",
	"dst_hostname": "Destination hostname",
	"src_se": "source_protocol://source_host", // (starting with v3.12.4)
	"dst_se": "dest_protocol://dest_host",  // (starting with v3.12.4)
	"protocol": "davs", // destination protocol first, source protocol fallback (starting with v3.12.4)
	"src_site_name": "", // Always empty
	"dst_site_name": "", // Always empty
	"t_channel": "source_protocol://source_host__dest_protocol://dest_host",

	"timestamp_tr_st": 0, // Timestamp of the start of *only the transfer part* (excluding preparation), in milliseconds
	"timestamp_tr_comp": 0, // Timestamp of the completion of *only the transfer part* (excluding preparation), in milliseconds
	"timestamp_chk_src_st": 0, // Timestamp when started the validation of the source checksum, in milliseconds
	"timestamp_chk_src_ended": 0, // Timestamp when finished the validation of the source checksum, in milliseconds
	"timestamp_checksum_dest_st": 0, // Timestamp when started the validation of the destination checksum, in milliseconds
	"timestamp_checksum_dest_ended": 0, // Timestamp when finished the validation of the destination checksum, in milliseconds
	"t_timeout": 0, // Timeout used for the transfer
	"chk_timeout": 0, // Timeout used for the checksum operations

	"t_error_code": 0, // Error code: an errno value (i.e. ENOENT)
	"tr_error_scope": "Error scope, empty if ok",
	"t_failure_phase": "Error phase, empty of ok",
	"tr_error_category": "Error category, empty if ok",
	"t_final_transfer_state": "Ok|Error|Abort",
	"t_final_transfer_state_flag": 1, // 1 - "Ok" / 0 - "Error" / -1 - "Abort" (starting with v3.12.4)
	"t__error_message": "Error message, empty if ok",

	"tr_bt_transfered": 0, // How many bytes have been transferred
	"nstreams": 0, // How many streams have been used
	"buf_size": 0, // TCP buffer size used (for backwards compatibility)
	"tcp_buf_size": 0, // TCP buffer size used
	"block_size": 0, // Unused
	"scitag": 65, // SciTag flow label (value in the [65, 65535] inclusive range) (starting with v3.12.11)
	"f_size":  0, // Filesize (to be deprecated)
	"file_size": 0, // Filesize (starting with v3.12.4)

	"time_srm_prep_st": 0, // Timestamp of the start of the SRM GET operation, if any, in milliseconds
	"time_srm_prep_end": 0, // Timestamp of the completion of the SRM GET operation, if any, in milliseconds
	"time_srm_fin_st": 0, // Timestamp of the start of the SRM PUT operation, if any, in milliseconds
	"time_srm_fin_end": 0, // Timestamp of the completion of the SRM PUT operation, if any, in milliseconds

	"srm_space_token_src": "Source space token, if any",
	"srm_space_token_dst": "Destination space token, if any",

	"tr_timestamp_start": 0, // Timestamp of the whole copy process start, in milliseconds
	"tr_timestamp_complete": 0, // Timestamp of the whole copy process completion, in milliseconds

	"transfer_time": 0, // Duration of the transfer part, in milliseconds: "timestamp_tr_comp" - "timestamp_tr_st" (starting with v3.12.4) 
	"operation_time": 0, // Duration of the whole copy process, in milliseconds: "tr_timestamp_complete" - "tr_timestamp_start" (starting with v3.12.4)
	"throughput": 0, // Throughput expressed in bytes / second , -1 if "transfer_time" is 0 (starting with v3.12.4)

	"srm_preparation_time": 0, // Time spent in SRM preparation stage, in milliseconds (starting with v3.12.4)
	"srm_finalization_time": 0, // Time spent in SRM finalization stage, in milliseconds (starting with v3.12.4)
	"srm_overhead_time": 0, // Time spent in SRM preparation and SRM finalization stage, in milliseconds (starting with v3.12.4)    
	"srm_overhead_percentage": 0, // Percentage of "srm_overhead_time" from "operation_time", -1 value if "operation_time" is 0 (starting with v3.12.4)

	"timestamp_checksum_src_diff": 0, // Time spent getting source checksum, in milliseconds (starting with v3.12.4)
	"timestamp_checksum_dst_diff": 0, // Time spent getting destination checksum, in milliseconds (starting with v3.12.4)

	"channel_type": "urlcopy", // Always
	"user_dn": "User that submitted the job",
	"file_metadata": "File metadata set by the user at submission",
	"job_metadata": "Job metadata set by the user at submission",

	"retry": 0, // When retries are enabled, which retry is this transfer
	"retry_max": 0, // When retries are enabled, max number of retries for this transfer
	"job_m_replica": false, // true if this transfer belongs to a multiple replica job
	"job_multihop": false, // true if this transfer belongs to a multihop job (starting with v3.12.0)
	"transfer_lasthop": false, // true if this transfer is the last one of a multihop job (starting with v3.12.0)
	"is_archiving": false, // true if after the transfer file will be moved to the archiving polling state
	"job_state": "Job state, if known",
	"is_recoverable": false, // true if FTS3 considers this transfer could be retried (depends on the error code)
	"ipv6": false, // true if the transfer took place over IPv6
	"ipver": "ipv4|ipv6|unknown", // TPC connection IP-type (starting with v3.12.4)
	"eviction_code": 0, // Return code for eviction operation: 0 = success, > 0 = failed with error code, -1 = eviction not set (starting with v3.12.0)  
	"cleanup_code": 0, // Return code for the final clean-up deletion operation: 0 = success, > 0 = failed with error code, -1 = clean-up not performed (starting with v3.12.9)
	"final_destination": "host:ipaddress:port", // TPC destination host, field available only for GridFTP transfers with PASV mode enabled
	"transfer_type": "streamed|3rd pull|3rd push", // How the transfer was done
	"auth_method": "certificate|oauth2", // Authentication method (starting with v3.12.11)
	"overwrite_on_disk_flag": false, // Overwrite-when-only-on-disk feature requested (starting with v3.13.1)
	"overwrite_on_disk_deletion_code": 0 // Return code for the overwrite-when-only-on-disk deletion operation: 0 = success, > 0 = failed with error code, -1 = deletion not performed (starting with v3.13.1)
}
```

## State change
When a transfer changes state (i.e. SUBMITTED -> ACTIVE), a message like the one below is generated.

```javascript
{
	"user_dn": "User that submitted the job",
	"src_url": "Source URL",
	"dst_url": "Destination URL",
	"vo_name": "Virtual Organization",
	"source_se": "source_protocol://source_host",
	"dest_se": "dest_protocol://dest_host",
	"job_id": "Job ID",
	"file_id": 0, // File ID (a 64 bits unsigned integer)
	"job_state": "Job State",
	"file_state": "File state",
	"retry_counter": 0, // Number of retries done
	"retry_max": 0, // Maximum number of retries
	"user_filesize": 0, // Filesize set by the user
	"timestamp": 0, // Message timestamp, in milliseconds
	"staging": false, // true if this file passed via a staging phase
	"staging_start": 0, // Timestamp of the start of the staging request
	"staging_finished": 0, // Timestamp of the completion of the staging request
	"archiving": false, // true if this file passed via a archiving polling phase
	"archiving_start": 0, // Timestamp of the start of the archiving polling phase
	"archiving_finished": 0, // Timestamp of the completion of the archiving polling phase
	"submit_time": 0, // Timestamp of the submission (when it was received)
	"reason": "Error reason, empty on successful or intermediate states",

	"job_metadata": "Job metadata set by the user at submission",
	"file_metadata": "File metadata set by the user at submission",
}
```

You can have a look at the [job and transfer state machines](../state_machine.md)
documentation.

### Optimizer
```javascript
{
	"source_se": "source_protocol://source_host",
	"dest_se": "dest_protocol://dest_host",
	"timestamp": 0, // Message timestamp, in milliseconds
	"throughput": 0.0, // Instant throughput, in bytes per second
	"throughput_ema": 0.0, // Exponential Moving Average for the throughput
	"duration_avg": 0.0, // Average transfer duration, in seconds
	"filesize_avg": 0.0, // Average file size, in bytes
	"filesize_stddev": 0.0, // Standard deviation for the filesize
	"success_rate": 0.0, // A number between 0 and 1, where 1 = 100%
	"retry_count": 0.0, // How many transfers have been retries
	"active_count": 0, // How many transfers are running
	"submitted_count": 0, // How many transfers are queued
	"connections": 0, // Optimizer decision
	"rationale": "Explanation for the decision"
}
```
