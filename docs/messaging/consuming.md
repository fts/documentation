Consuming
=========
For consuming messages coming from an FTS3 service, you will need to connect to a suitable
broker, or set of brokers, and subscribe to three possible topics:

  1. Transfer start
  2. Transfer completion
  3. Status changes

All of this is configurable, so you will need to contact the system administrator of your
FTS asking for these parameters.

## WLCG (and probably others)
Most (if not all) known FTS3 instances publish to the broker `dashb-mb.cern.ch`,
and use the topics:

  1. `transfer.fts_monitoring_start`
  2. `transfer.fts_monitoring_complete`
  3. `transfer.fts_monitoring_state`

Port for consuming is 61123, uses TLS and requires client authentication via X509, plus authorization
on the server side.

If you want to be able to subscribe with a new user certificate, you will need to ask for permissions.

## Notes on connecting to dashb-mb.cern.ch
When connecting to `dashb-mb.cern.ch`, it is important to know that for receiving all messages,
you will need to connect independently to *all* nodes behind the DNS alias.
You will also need to perform independent subscriptions in each one of them.

For a hint on how to do this in Python, you can have a look at the
[implementation](https://gitlab.cern.ch/fts/fts-tests/blob/develop/lib/messaging.py) we use for our
tests.

