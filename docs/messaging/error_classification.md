Recoverable and non recoverable errors
======================================
FTS3 messages provide an error classification between recoverable and non recoverable errors.

## Recoverable errors
Errors regarding performance issues, timeouts and network issues are considered recoverable errors and they would be retried. 
Examples of these errors can be: performance marker, name or service not known, connection timed out, end-of-file was reached, end of file occurred, SRM_INTERNAL_ERROR, was forcefully killed, operation timeout, etc ...


## Non recoverable errors

Authentication errors, files that are not found or checksum mismatch would be considered as non recoverable errors. 
Examples of these errors can be: proxy expired, file not found, file exists and overwrite, no such file or directory, the certificate has expired, the available CRL has expired, digest too big for rsa key, can not determine address of local host, permission denied, system error in write into HDFS, file exists, checksum do not match, CHECKSUM MISMATCH, SRM_INVALID_PATH, SRM Authentication failed, SRM_DUPLICATION_ERROR, SRM_AUTHENTICATION_FAILURE, SRM_NO_FREE_SPACE, etc ...


