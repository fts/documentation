Monitoring API
==============
The FTS3 Web Monitoring is written using [AngularJS](https://angularjs.org/),
interacting with a back-end that generates the requested data in Python.
This information can be retrieved directly by the user, so it can be reused
for custom-made scripts, monitoring probes, etc.

# Base URL
When you access an instance of the web monitoring, you will usually see it
finishes in something like <base>/#/. Then, to access an API call you need
to write <base>/api-call.

For instance, assuming our web view is `https://fts3-pilot.cern.ch:8449/fts3/ftsmon/#/`,
then, to get the overview in JSON we need to request
`https://fts3-pilot.cern.ch:8449/fts3/ftsmon/overview`

Some of the resources allow filtering. This means, a set of query parameters can be
added. For instance, `https://fts3-pilot.cern.ch:8449/fts3/ftsmon/overview?vo=atlas`

# Resources

## /overview
  * **Description** Returns the status overview of the service.
  * **Filters**  vo, source_se, dest_se, page, time_window (in hours)
  * [Check it out!](https://fts3-pilot.cern.ch:8449/fts3/ftsmon/overview)

## /jobs
  * **Description** Get a list of jobs
  * **Filters** startdate, enddate, time_window (in hours), vo, source_se, dest_se, state (comma-separated list of states: FAILED,FINISHED)
  * [Check it out!](https://fts3-pilot.cern.ch:8449/fts3/ftsmon/jobs)

## /jobs/<job-id>
  * **Description** Get information about a job

## /jobs/<job-id>/files
  * **Description** Get the list of transfers within the given job
  * **Filters** state (comma-separated list of states: FAILED,ACTIVE), page

## /transfers
  * **Description** Get a list of file transfers
  * **Filters** source_se, dest_se, vo, time_window (in hours), state (comma-separated list of states: FINISHEDDIRTY,CANCELED,FAILED), page
  * [Check it out!](https://fts3-pilot.cern.ch:8449/fts3/ftsmon/transfers)

## /config/audit
  * **Description** Get the configuration audit of the server
  * **Filters** page, user, contains
  * [Check it out!](https://fts3-pilot.cern.ch:8449/fts3/ftsmon/config/audit)

## /config/links
  * **Description** Get the link configurations active on the server
  * **Filters** page
  * [Check it out!](https://fts3-pilot.cern.ch:8449/fts3/ftsmon/config/links)

## /config/server
  * **Description** Get the server configuration as stored in the database. i.e max time in queue and retries
  * **Filters**
  * [Check it out!](https://fts3-pilot.cern.ch:8449/fts3/ftsmon/config/server)

## /config/debug
  * **Description** Get the list of pairs that have debug enabled
  * **Filters**
  * [Check it out!](https://fts3-pilot.cern.ch:8449/fts3/ftsmon/config/debug)

## /stats
  * **Description** General statistics of the servers, as success rate in the last hour, files per state, etc.
  * **Filters**
  * [Check it out!](https://fts3-pilot.cern.ch:8449/fts3/ftsmon/stats)

## /stats/servers
  * **Description** Number of transfers received, executed and active per machine (in the last 12 hours)
  * **Filters**
  * [Check it out!](https://fts3-pilot.cern.ch:8449/fts3/ftsmon/stats/servers)

## /stats/vo
  * **Description** Number of transfers per state and per vo
  * **Filters**
  * [Check it out!](https://fts3-pilot.cern.ch:8449/fts3/ftsmon/stats/vo)

## /stats/profiling
  * **Description** Internal server profiling. Intended for debugging purposes.
  * **Filters**

## /optimizer
  * **Description** General overview of the optimizer status.
  * **Filters** source_se, dest_se, time_window (in hours)
  * [Check it out!](https://fts3-pilot.cern.ch:8449/fts3/ftsmon/optimizer/)

## /optimizer/detailed
  * **Description** Detailed view of the optimizer status for a given pair.
  * **Filters** source, destination, time_window (in hours)

## /errors/
  * **Description** Number of errors per pair
  * **Filters** source_se, dest_se, reason (search for the given text inside the error message)
  * [Check it out!](https://fts3-pilot.cern.ch:8449/fts3/ftsmon/errors/)

## /errors/list
  * **Description** List of errors for a given pair
  * **Filters** source_se, dest_se, reason

## /unique/(activities|destinations|sources|vos|hostnames) ¶
  * **Description** Returns unique vo_name, source_se, dest_se, vos or hostnames (limited to the last 12 hours)
  * **Filters**
  * [Check it out!](https://fts3-pilot.cern.ch:8449/fts3/ftsmon/unique/vos)
