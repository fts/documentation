The QoS Service configuration
=============================

> Note: Starting with version 3.10, the QoS Service replaces the Bringonline daemon.
  For the moment, the staging parameters apply to all QoS transitions

As any FTS3 configuration parameters, the QoS service configuration is available in the dedicated [section](../install/fts3.md).

This section explains more in-depth some of the QoS Service parameters

## StagingBulkSize (default: 200)

Staging request bulk size. If the value is too small, the performance of the
service will decrease, but if it is too big it will be heavier to serialize
and deserialize requests and responses. Also, the remote storage may reject
the request.

It is recommended to leave it between 100 and 1000.

## StagingConcurrentRequests (default: 1000)

Maximum number of concurrent active bulk staging requests. The higher the value, the more
files can be sent to a storage, but higher the load on the DB.

## StagingWaitingFactor (default: 300)

In seconds, how long should FTS3 wait until it issues a staging request.
This can be used to let FTS3 group several files on a single bulk request.

## StagingPollRetries (default: 3)

Retry this number of times if a poll fails.
