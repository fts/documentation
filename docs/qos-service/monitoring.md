Monitoring of QoS transitions
=============================

FTS3 QoS transitions can be monitored either via the FTS3 Web Monitoring or via the Grafana Dashboard at CERN, 
in case the service is configured to send messages to the CERN Monitoring message brokers. 

## Web Monitoring

The Web Monitoring overview (i.e.: [FTS3-pilot overview](https://fts3.cern.ch:8449/fts3/ftsmon/#/)) 
reports for each link the files the number of files that are:
- in the FTS staging queue (`Staging` column)
- in active staging status, where the tape endpoint retrieves the file and FTS performs polling (`S.Active` column)
- in archive monitoring status (`Archiving` column)

## Grafana

TODO
