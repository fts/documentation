QoS and Tape
============
This chapter includes documentation related to one of the main features of FTS: 
requesting QoS transitions. When it comes to Tape systems, the most common transitions
are Staging (retrieving a file from tape) and Archiving (writing a file to tape). 

The chapter describes the [QoS Service](qos-service.md), responsible for the file staging and archive monitoring. 
It also offers insights into [Configuration](configuration.md) and [Monitoring](monitoring.md) details.
