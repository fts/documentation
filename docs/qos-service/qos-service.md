The QoS Service
===============

> Note: Starting with version 3.10, the QoS Service replaces the Bringonline Service. 
  The previous Bringonline documentation page can be found [here](../staging/bringonline.md).

The QoS service is responsible for staging from tape and archive monitoring,
using the Gfal2 Bringonline and Archiving API. Supported protocols are
SRM, XRootD (CERN-CTA) and HTTP (Tape REST API).

> Note: FTS v3.12.0 does not support the QoS staging operation over HTTP. 
  The full support of QoS operations over HTTP will be added in a later release.*

The service is usually deployed on each node of the FTS cluster. 
It queries the FTS Database in order to get files that have a QoS transition requested by the clients.
This means `STAGING` for tape retrieval, `ARCHIVING` for archive status monitoring 
and `QOS_REQUEST_SUBMITTED` fo the more general QoS transition.

The service is optimized to send bulk requests to the storage system.  
*Note: exception to this is the SRM archive status monitoring, which polls per file.*

Each file within a transition is treated independently. As soon as the file's transition is done, 
it continues to the transfer workflow. For example, if the client wants to stage a bunch of files 
and then copy them to a destination, as soon as a single file (that is part of a bulk request) 
becomes available on disk, it is queued for transfer and will eventually be scheduled.

By checking the [State Machine](../state_machine.md) implemented by the QoS Service for each 
transfer, it's evident the link between the QoS activity and the transfer activity.

**Example for staging**  
- Each transfer submitted by the client to be staged has the `STAGING` state. Once the QoS daemon submits 
  the staging request to the Storage, the transfer moves to the `STARTED` state. 
- If the client requests only the staging of the file in the local buffer, 
  the state either goes to `FAILED` or `FINISHED` depending on the outcome of the staging.
- In case the client requests a further copy of the file to a different destination, 
  once the staging is successfully completed, the transfer moves to the `SUBMITTED` state 
  and then the `FTS Server Service` takes over, responsible for the scheduling and transfer part

## Bringonline requests

Each QoS service instance queries the Database every minute in order to retrieve 
transfers that have been submitted for staging (`STAGING` file state in the Database),
structuring them into `<voname, storage>` pairs.

The service sends bulk requests to the storage endpoints (200 files in default bulk size request). 
As the clients may request staging gradually, the service waits an amount of time (5 min by default, configurable)
before sending the request to the storage system, in order to accumulate more files for the bulk request.

In addition, the service honours a maximum number of active bulk requests per storage (1000 by default). 
Example: if each bulk request has a maximum of 200 files, 
the service maintains maximum 200k bringonline file requests per storage.

The number of active bringonline file requests per storage can be configured separately
(more details in the [storage configuration](../../fts-rest/docs/config_alternatives.html).

When either the configured storage limit or the maximum number of active bulk requests is reached, 
the service stops sending bringonline requests to the Storage, so further client requests are queued.

### SRM copy pin management

In the case of SRM protocol, when submitting the bringonline request, 
FTS also sends a `copy-pin-lifetime` parameter to the storage.
The parameter expresses the lifetime of the disk copy on the storage system, once the file has been staged.
Unless specified by the client, the default value of 28800 secs (8 hours) is used.

Note: The copy pin lifetime is not honoured by all storage implementation (i.e. Castor).
In some cases the disk copy can be garbage collected by the storage system to make room for new files 
regardless of the `copy-pin-lifetime` value requested by FTS.

Starting with v3.9.4, once a file is successfully staged and transferred to the destination, 
FTS will request a pin release on the SRM source storage. 

## Bringonline polling

Once a staging request is accepted by the server, the service moves to *bringonline polling*. 
This means the server will query the file to check whether it arrived at the disk buffer (i.e.: has a disk replica).
Polling is available for the SRM and XRootD (CERN-CTA) protocols and is done via bulk requests.
Polling time increases exponentially by a factor of 2, reaching a maximum of 10 minutes between polls.

The Service is also able to ignore bringonline polling errors up to a configurable number (3 by default). 
This prevents failing long-lasting requests due to temporary network or storage glitches.

*Note: In FTS v3.12.0, bringonline and bringonline polling operations 
       are not yet available for HTTP operations.* 

## Archive polling

This feature works in a very similar fashion to the *bringonline polling*.
It is available for the SRM, XRootD (CERN-CTA) and HTTP protocols and is done via bulk requests.

Once a file is successfully transferred to the Tape destination storage's disk buffer, FTS begins archive monitoring. 
This means the server will query the file, checking whether it arrived successfully on tape.
Polling time increases exponentially by a factor of 2, reaching a maximum of 10 minutes between polls.


## QoS transitions

The QoS service brings the more general concept of a QoS transition.
These transitions rely on the Gfal2 QoS API, at the moment implemented for the HTTP protocol,
according to the [CDMI-QoS](https://indigo-dc.gitbook.io/cdmi-qos/user-guide/api-walkthrough) specification.

To submit a QoS transition, you must specify the desired QoS during the job submission.
This tells the service it is dealing with a QoS transition. 

A transition moves through the following states:
- `QOS_REQUEST_SUBMITTED`: initial state, waiting to be picked up by the QoS service
- `QOS_TRANSITION`: QoS request has been sent to the endpoint. Polling begins
- `FINISHED` or `FAILED` (error encountered during polling) 

Note: Although the general mechanism is implemented, the QoS discussion is far from being finalized.
As the topic gains interest within WLCG, FTS will evolve its QoS interface to meet the new requirements.
For the moment, this feature should be considered in prototype status.

**QoS transition submission example**  
Using the CLI client
```
fts-rest-transfer-submit --target-qos=<qos> -s https://<fts3-server>:8446/ <surl> <same_surl>
```
Via REST API
```json
{
    "files": [
        {
            "sources": [
                "<file_surl>"
            ],
            "destinations": [
                "<same_file_surl>"
            ]
        }
    ],
    "params": {
      "target_qos": "<qos_class>"
    }
}
```
