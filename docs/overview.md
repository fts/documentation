FTS3 Documentation
==================
FTS3 is a bulk data mover, created to distribute globally the multiple
petabytes of data from the LHC at CERN.

Its purpose is to efficiently schedule data transfers, maximising the use
of available network and storage resources while ensuring that any policy
limits are respected.

FTS3 can be used in a number of different ways:

  * An individual or small team can access the web interface to FTS to
schedule transfers between storage systems. They can browse the contents
of the storage, invoke and manage transfers, and leave FTS to do the rest.
  * A team's data manager can use the FTS command line interface or the
REST API to schedule bulk transfers between storage systems.
  * A data manager can install an FTS service for local users. The service
is equipped with advanced monitoring and debugging capabilities which enable
her to give support to her users.
  * Maintainers of frameworks which provide higher level functionality can
delegate responsibility for transfer management to FTS by integrating it using
the various programmatic interfaces available, including a REST API. The users
thus continue to use a familiar interface while profiting from the power of
FTS transfer management.


You can have a look at what FTS3 provides on the [Features](features.md) page.

You can follow the [Quick Setup](install/quick.md) to install and configure
your own FTS3 server.

You can use the [Command Line Tools](cli.md) to start transferring your
first files.

If you want to integrate FTS3 on your own application, framework or set of
scripts, go to the [REST API](../fts-rest/docs/api.md) documentation.
Or, even easier, just use the [Easy Bindings](../fts-rest/docs/easy/README.md)

For asynchronously digesting the transfer results, see the
[Messages Format](messaging/format.md)

To visually see what's going on, and to ease debugging the service, try the
[Web Monitoring](fts3mon.md).


And, if you want to contribute code, have a look at the
[Developer Guide](developers.md).
