Staging from Tapes
==================

## What
FTS supports requesting staging requests from Storages supporting tape systems 
via the SRM protocol, XRootD protocol, in the case of CERN-CTA, and since
version 3.12.2 the HTTP protocol.

## When
When a file is stored in a Tape system, in order to access its content it needs first to be copied to disk.
Eventually after the first copy, which is typically to a buffer area on the same storage system, the files can be also copied to a different destination

## How

### CLI

Specify the `--bring-online <seconds>` parameter with `fts-transfer-submit` or `fts-rest-transfer-submit`. 
The value passed as parameter is the bringonline timeout, expressed in seconds.
If the file is not staged before the timeout expires, the staging operation will be canceled and the transfer will not take place.

In case you want just to stage the file to the local storage buffer, the source and destination url passed via CLI must be the same.

Optionally a pin lifetime value can be also specified, to express the lifetime of the replica on disk, using the `--copy-pin-lifetime <arg>` parameter.
If not specified, the default value used by the system and sent to the storage is 28800 secs (8 hours). 

### REST API

Set the `bring_online` parameter to a value > -1, and optionally the `copy_pin_lifetime` parameter to a value > -1.  
Note: the value of `copy_pin_lifetime` is not always honoured by the Storage systems is only available for storages using the SRM interface.

```json
{
  "files": [
    {
      "sources": [
        "https://eosctaatlas.cern.ch/eos/example/file"
      ],
      "destinations": [
        "https://eospps.cern.ch/eos/example/file"
      ],
      "staging_metadata": {
        "atlas-eoscta@cern.ch": {
          "activity": "expe_t0_daq"
        }
      }
    }
  ],
  "params": {
    "bring_online": 108000,
    "copy_pin_lifetime": 64000
  }
}
```
* **staging_metadata** The metadata to be used in the staging operation. This parameter is optional and must be valid JSON object.
