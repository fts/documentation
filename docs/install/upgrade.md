Upgrade guide
=============

Most FTS upgrades should be straight forward, requiring no service downtime.
This is normally true for minor releases, but definitely should hold true for patch releases.

## General steps
For the majority of upgrades, what you need to do is

1. Upgrade RPMs
2. Restart all FTS services
    * `systemctl restart httpd`
    * `systemctl restart fts-server`
    * `systemctl restart fts-qos` (>= v3.10.0)
    * `systemctl restart fts-bringonline` (< v3.10.0)
    * `systemctl restart fts-msg-bulk`
    * (or equivalents with `service <service> restart`)

## Releases with schema changes 
Some releases may include backwards incompatible changes, or be a bit more tricky than usual.
These are documented here:

### FTS 3.5
FTS 3.5 introduces database schema changes that are partially incompatible with older versions of FTS.
However, we have taken care of making the changes in a way that facilitates the upgrade path.

For more information, please, refer to the [FTS 3.5 upgrade](upgrades/3.5.md) guide.

### FTS 3.6
FTS 3.6 introduces database schema changes that are fully incompatible with older versions of FTS.
For more information, please, refer to the [FTS 3.6 upgrade](upgrades/3.6.md) guide.

### FTS 3.7
FTS 3.7 introduces database schema changes that are fully incompatible with older versions of FTS.
For more information, please, refer to the [FTS 3.7 upgrade](upgrades/3.7.md) guide.

### FTS 3.8
For more information, please, refer to the [FTS 3.8 upgrade](upgrades/3.8.md) guide.

### FTS 3.9
FTS 3.9 introduces database schema changes that are fully incompatible with older versions of FTS.
For more information, please, refer to the [FTS 3.9 upgrade](upgrades/3.9.md) guide.

### FTS 3.10
FTS 3.10 introduces database schema changes that although are compatible with older versions of FTS, upgrading is encouraged.
For more information, please, refer to the [FTS 3.10 upgrade](upgrades/3.10.md) guide.

### FTS 3.11
FTS 3.11 introduces database schema changes that although are compatible with older versions of FTS, upgrading is encouraged.
For more information, please, refer to the [FTS 3.11 upgrade](upgrades/3.11.md) guide.

### FTS 3.12
FTS 3.12 brings the Python2 to Python3 migration. It also introduces database schema changes
that are incompatible with older versions of FTS. For more information, 
please refer to the [FTS 3.12 upgrade](upgrades/3.12.md) guide.

### FTS 3.12.10
FTS 3.12.10 introduces database schema changes that although are compatible with older versions of FTS, upgrading is encouraged.
For more information, please, refer to the [FTS 3.12.10 upgrade](upgrades/3.12.10.md) guide.
