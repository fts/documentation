 Upgrade to FTS 3.13 (Alma9 release)
====================================

FTS v3.13 brings support for Alma9.

From this moment on, the FTS project moves to Alma9 and there will be no more guarantees for CC7 functionality.
In fact, certain components, such as FTS Web Monitoring, are only provided for Alma9 platform.

FTS 3.13 introduces a schema upgrade:

* Schema 9.0.0 ([fts-diff-9.0.0.sql][1]): adds new columns and tables needed by the new software

Because of the different schema, the old software (FTS v3.12.x) won't work with the new schema.
After applying the new schema, you must also move forward with the Alma9 deployment at the same time.

In case a rollback is needed, we provide the following schema downgrade file: [fts-downgrade-9.0.0.sql][2].


## FTS v3.13 upgrade procedure

As opposed to most other FTS upgrade pages, this guide won't describe the process on how to upgrade an existing installation,
but instead it will assume installation starts from zero, on a new Alma9 machine.

The main steps will be:

* Database schema changes
  * Requires existing `schema-8.2.0`
* Start on a new Alma9 machine
* Given the large schema change, we recommend draining the existing CC7 nodes and stopping submission to the instance
* Plan for FTS instance downtime

**Notes on the upgrade duration**

The downtime will depend on the size, capacity and technology of your database server.
To reduce the size of the database, you may run `/usr/sbin/fts_db_cleaner`
just before doing the upgrade, in order to prune old records.

Even with MySQL8, the new schema needs to modify the largest `t_file` table, which may take some time.

### Perform database schema upgrade

Make sure to have the schema file ready:
```bash
$ wget https://gitlab.cern.ch/fts/fts3/-/raw/v3.13.0/src/db/schema/mysql/fts-diff-9.0.0.sql -O /tmp/fts-diff-9.0.0.sql
```

Connect to your database, and apply the schema diff file:
```sql
> source /tmp/fts-diff-9.0.0.sql
```

We recommend scheduling downtime and allowing sufficient time for the schema change.

### Install Alma9 environment pre-requisites

Starting on a brand new Alma9 machine, certain pre-requisites are needed
before setting up the FTS instance.

**Prepare DMC repository (optional)**

FTS requires DMC packages (e.g.: gfal2, davix, srm-ifce). These can be installed from EPEL or via the DMC production repository.

Our example installs the DMC production repository file:
```bash
$ wget https://dmc-repo.web.cern.ch/dmc-repo/dmc-el9.repo -O /etc/yum.repos.d/dmc-el9.repo
```

**Prepare FTS repository**

The FTS production repository, as well as the FTS dependencies repository are needed:
```bash
$ wget https://fts-repo.web.cern.ch/fts-repo/fts3-el9.repo -O /etc/yum.repos.d/fts3-el9.repo
$ wget https://fts-repo.web.cern.ch/fts-repo/fts3-depend.repo -O /etc/yum.repos.d/fts3-depend.repo
```

**Enable EPEL repository**

The EPEL repository brings additional packages which are not found in the base Alma9 repositories.
EPEL repository is required for FTS installation.
```bash
$ dnf install -y epel-release
```

**Setup CA certificates**

The Grid Root CAs must be setup for FTS to interpret certificates correctly.
The Grid ROOT CAs are provided by EGI Trustanchors (more details [here][3]).

To proceed, create a repo file `/etc/yum.repos.d/egi-trustanchors.repo` with the following content"
```
[EGI-trustanchors]
name=EGI-trustanchors
baseurl=https://repository.egi.eu/sw/production/cas/1/current/
gpgkey=https://repository.egi.eu/sw/production/cas/1/GPG-KEY-EUGridPMA-RPM-3
gpgcheck=1
enabled=1
```

Install the CAs now:
```bash
$ dnf install -y ca-policy-lcg
```

A tool to refresh the CRLs is also needed:
```bash
$ dnf install -y fetch-crl
$ fetch-crl
```

**Obtain your Grid Host certificate**

This step is custom and will depend upon the institute where you are deploying FTS.
Ultimately, you must make sure to provide host certificates in the following location:
```
/etc/grid-security/hostcert.pem
/etc/grid-security/hostkey.pem
```

The `hostkey.pem` must not be password-protected.


### Install FTS v3.13 on Alma9

With all the pre-requisites in-place, it's time to install FTS v3.13.

**FTS server installation**

Install the main FTS server. This is the component doing the transfers and tape (QoS) operations.
```bash
$ dnf install -y fts-server fts-server-selinux fts-mysql --enablerepo=crb
```

You may also install the package responsible with sending JSON messages to an ActiveMQ broker:
```bash
$ dnf install -y fts-msg
```

**FTS-REST server installation**

This is the front-end submission server. It allows submission and querying of transfers.
```bash
$ dnf install -y fts-rest-server fts-rest-server-selinux
```

**FTS Web Monitoring installation**

This is the component that displays transfers via the FTS Monitoring webpage.
```bash
$ dnf install -y fts-monitoring
```

**FTS-REST client installation (optional)**

Install the FTS client, provided as Python module and CLI tools:
```bash
$ dnf install -y fts-rest-client
```

### Configure your FTS v3.13 installation

It's time to configure the FTS installation:
```
/etc/fts3/fts3config
/etc/fts3/fts3restconfig
/etc/fts3/fts-msg-monitoring.conf
/etc/fts3web/fts3web.ini
```

You may copy the files from the CC7 installation to the new Alma9 machine.

### Start the service

Ensure your firewall settings allow openings for ports `8846` and `8449`.
Once that is done, start the FTS service:
```bash
$ systemctl start fts-server fts-qos fts-msg-bulk
$ systemctl start httpd
```

### Test out your new FTS installation

Run a transfer against the new machine:
```bash
$ voms-proxy-init
$ fts-rest-transfer-submit -s https://<fts-node>:8446 <source> <destination>
```

If the transfer submission worked, congratulations, you should now have your FTS Alma9 instance!


[1]: https://gitlab.cern.ch/fts/fts3/-/blob/v3.13.0/src/db/schema/mysql/fts-diff-9.0.0.sql
[2]: https://gitlab.cern.ch/fts/fts3/-/blob/v3.13.0/src/db/schema/mysql/fts-downgrade-9.0.0.sql
[3]: https://docs.egi.eu/providers/operations-manuals/howto01_using_igtf_ca_distribution/
