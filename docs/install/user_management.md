User Management
=============

Once you install FTS, you will need to add the DN of the users that are allowed to perform changes to the FTS server either using curl or via the web monitoring config at https://fts_host:8446/config

## Adding the first user via mysql
1. Connect to your mysql instance with `mysql -u your_fts_user -pYOUR_DB_PASS -h you_host your_database`  
2. Insert your User DN in order to be allowed to perform config changes `insert into t_authz_dn (dn, operation) values ('/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=ekaravak/CN=678523/CN=Edward Karavakis', 'config');`

If you have more users, you could either repeat the previous steps or add them via the Web config UI, that you now should have access to, at https://fts_host:8446/config/authorize
