Web Monitoring
==============
By default, the web monitoring can work out of the box as long as it is installed
together with FTS3 in the same nodes, since it also reads `/etc/fts3/fts3config`

## Running it in a separate machine
However, if you wish to install the web monitoring in some other node, you must
set up your database connection credentials in `/etc/fts3web/fts3web.ini`.
Look for the commented `[database]` section.

Also, in this scenario you will have to install Apache and grant it access to read
the FTS3 logs, so they are accessible via browser. Again, this is only necessary
if you are *not* installing fts-monitoring in your FTS3 nodes.

```bash
yum install httpd
cat >>/etc/httpd/conf.d/fts3logs.conf <<EOF
Alias /var/log/fts3 "/var/log/fts3"
<Directory /var/log/fts3>
  Order allow,deny
  Allow from all
  ForceType text/plain
</Directory>
EOF
service httpd restart 
```

## Non-standard port or path
By default, everything just works fine. However, if you modify the default port or
schema for the Web Monitoring (i.e. use plain HTTP), remember to modify accordingly
`/etc/fts3web/fts3web.ini`, since these values will be used to generate the URLS
to access the server logs.

```ini
[logs]
scheme = https
port   = 8449
base   = 
```
