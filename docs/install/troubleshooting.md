Troubleshooting
===============

This is a short cross-check list to go through if you have trouble booting up
your server:

  1. Check error messages when starting the service
    * If the database credentials are missing
      or have wrong permissions, the service will not start

  2. REST interface working? Try from the FTS3 host
    * `curl --capath /etc/grid-security/certificates -E /etc/grid-security/hostcert.pem --key /etc/grid-security/hostkey.pem https://``hostname -f``:8446/whoami`
    ```json
{"dn": ["/host/dn"], "vos_id": ["2255c82d-c8ce-539a-8221-3b65c8d0aefd"], "roles": [], "delegation_id": "xxxxxxxxxxxxxxxx", "user_dn": "/host/dn", "level": {"transfer": "all", "deleg": "all", "config": "all", "datamanagement": "all"}, "is_root": true, "base_id": "01874efb-4735-4595-bc9c-591aef8240c9", "vos": ["xxxxxxxxxx"], "voms_cred": [], "method": "certificate"}
    ```
  3. CRLs installed and up to date?
    * Run fetch-crl and restart httpd

  4. Firewall ports opened? Open
    * 8446 for REST
    * 8449 for the web monitoring

  5. Submit a transfer and check
    * `/var/log/fts3/fts3server.log contains` information about the transfer
    * `/var/log/fts3/{date}/{sourceSe_destSe}/logfile` contains information about the transfer

  6. Is web app monitoring accessible and showing the transfer just submitted?
    * `https://{FTS3_ENDPOINT}:8449/fts3/ftsmon/#/`

  7. If running more than one server on top of the db and behind an alias, check
     each one individually by submitting a transfer directly to it

  8. Test if the alias is working properly, both for transfer submission and
     monitoring web-app

  9. Make sure cron jobs/services have been started, at least those needed
     (publishing in the BDII, and publishing monitoring messages are optional outside WLCG)

