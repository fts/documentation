FTS3 Docker Containers
================
Together with standard rpm distributions, FTS3 components are also available starting from v3.5 via Docker Containers for CentOS7

## Docker version
We use Docker v 1.12 in order to build and to run our containers, therefore we recommend to use this version of Docker

## FTS Server Docker

The FTS server Docker container runs the following services:

   * FTS server
   * FTS Bringonline
   * FTS Messaging 

Only the Mysql DB backend is supported as we are stopping to support FTS with Oracle backend

The containers are publised at:

https://gitlab.cern.ch/fts/fts3/container_registry

   * latest: refers to the latest development version
   * vx.x.x: refers to a specific tag

The container needs the configuration files folder to be mounted as docker volumes

   * /etc/fts3: should contain the fts3config and fts-msg-monitoring.conf, please refer to the FTS3 Setup guide 
   for details on how to configure them	
   * /etc/grid-security: should contain the CA certificates and VOMS lcs files. Please refer to the Quick Setup guide for details

The following command is needed to run the container

```bash
docker run -d  -v /etc/fts3:/etc/fts3  -v /etc/grid-security:/etc/grid-security \
gitlab-registry.cern.ch/fts/fts3:latest
```

## FTS REST Docker

The FTS REST Docker container runs the apache server which hosts the FTS REST service

The containers are publised at:

https://gitlab.cern.ch/fts/fts-rest-flask/container_registry

   * latest: refers to the latest development version
   * vx.x.x: refers to a specific tag

The container needs the configuration files and the certificates folders to be mounted as docker volumes

   * /etc/fts3: should contain the fts3restconfig, please refer to the REST Setup guide
   for details on how to configure them
   * /etc/grid-security: should contain the server host certificate and key ( hostcert.pem and hostkey.pem) 
   together with CA certificates and VOMS lcs files. Please refer to the Quick Setup guide for details

The local container port 8446 is the one where the apache server is listening

In order to run the container the following command is needed

```bash
docker run -d -p 8446:8446 -v /etc/fts3:/etc/fts3  -v /etc/grid-security:/etc/grid-security \
gitlab-registry.cern.ch/fts/fts-rest-flask:latest
```

## FTS Web Monitoring Docker

The FTS Web Monitoring container runs the apache server which hosts the Web Monitoring service

The containers are published at

https://gitlab.cern.ch/fts/fts-monitoring/container_registry

   * latest: refers to the latest development version
   * vx.x.x: refers to a specific tag

The container needs the configuration files and the certificates folders to be mounted as docker volumes

   * /etc/fts3: should contain the fts3config file,please refer to the FTS3  Setup guide
   for details on how to configure it
   * /etc/grid-security: should contain the server host certificate and key ( hostcert.pem and hostkey.pem)
   together with CA certificates. Please refer to the Quick Setup guide for details

The local container port 8449 is the one where the apache server is listening

In order to run the container the following command is needed

```bash
docker run -d -p 8449:8449 -v /etc/fts3:/etc/fts3  -v /etc/grid-security:/etc/grid-security \
gitlab-registry.cern.ch/fts/fts-monitoring:latest
```

N.B. Web Monitoring running via container will not be able to access the FTS3 Server logs, unless they are making accessible to the container into the /var/logs/fts3 folder


