FTS3 configuration file
=======================
Stored under `/etc/fts3/fts3config`.

It is quite simple in FTS3, being the most important information the
database credentials and connection string, and log settings.

## DBType (default: mysql)
Database backend type.

Determines which DB backend plugin is loaded. Only 'mysql' is currently
supported.

## DbUserName (default: empty)
Database account username.

## DbPassword (default: empty)
Database account password.

## DBConnectString (no default)
Connect string for the used database account.

For MySQL, the connection string has to follow the format `host(:port)/db`
(i.e. `"mysql-server.example.com:5050/fts3db`)

## DbThreadsNum (default: 26)
Number of pooled database connections

## Alias (default: empty)
The alias of the FTS endpoint (used in monitoring)

## Infosys (default: lcg-bdii.cern.ch:2170)
Could be either host:port OR false if bdii access is not enabled

## InfoProviders (default: glue1)
Use the information system's schema for retrieving info.
Accepted values: glue1 and glue2.
Several can be specified separated by ';' (i.e. glue1;glue2)

## SiteName (default: empty)
Site name, e.g. CERN-PROD (will be published to GLUE2)

## MonitoringMessaging (default: true)
Enable or disable monitoring over messaging mechanism (enable=true / disable=false)

## MessagingDirectory (default: /var/lib/fts3)
Where the FTS inter-process messages are written.  
Note: Keep this value consistent between the Server, QoS and REST processes.

## TransferLogDirectory (default: /var/log/fts3)
Where the logs for each transfer are stored.

## ServerLogDirectory (default: /var/log/fts3)
Where the logs for the services are stored.

## LogLevel (default: INFO)
Possible values are:  
-- TRACE (every detail), DEBUG (internal behaviour), INFO (normal behaviour)   
-- NOTICE (final states), WARNING (things worth checking),  
-- ERR (internal FTS errors), CRIT (fatal errors, such as segmentation faults)    
For a production instance, INFO is enough unless you are trying to debug an issue.
If you want even less output (just issues on which you may need to act), then
use WARNING or ERR.

## SchedulingInterval (default: 2)
In seconds, how often to poll for transfers to run. The higher the value,
the lower the database load, but higher latency to run transfers.

## MessagingConsumeInterval (default: 1)
In seconds, how often to check for messages on disk. Should be less than
CheckStalledTimeout/2.

## MinRequiredFreeRAM (default: not set)
Minimum required free RAM (in MB) for FTS3. If the available RAM goes below the
limit, FTS3 will automatically enter drain mode.

## MaxUrlCopyProcesses (default: 400)
Maximum number of fts_url_copy processes to run on the host. This can be used
to protect against resource exhaustion.

## StagingBulkSize (default: 200)
Staging request bulk size. If the value is too small, the performance of the
service will decrease, but if it is too big it will be heavier to serialize
and deserialize requests and responses. Also, the remote storage may reject
the request.

It is recommended to leave it between 100 and 1000.

## StagingConcurrentRequests (default: 1000)
Maximum number of concurrent staging requests. The higher the value, the more
files can be sent to a storage, but higher the load on the DB.

## StagingWaitingFactor (default: 300)
In seconds, how long should FTS3 wait until it issues a staging request.
This can be used to let FTS3 group several files on a single bulk request.

## StagingPollRetries (default: 3)
Retry this number of times if a poll fails.

## HeartBeatInterval (default: 60)
In seconds, how frequently FTS3 nodes mark themselves as alive.

## HeartBeatGraceInterval (default: 120)
In seconds, how long do other FTS3 nodes wait until they consider a node gone.

## OptimizerInterval (default: 60)
In seconds, how often does the optimizer run. Increase the value to reduce the
load on the database (at the cost of a higher latency).

## OptimizerSteadyInterval (default: 300)
After this many seconds without new data, the optimizer will run anyway.

## OptimizerMaxStreams (default: 16)
Maximum number of streams per file.

## OptimizerHighSuccessRate (default: 100)
Maximum success rate, as a percentage. Only makes sense to be 100, really.

## OptimizerMedSuccessRate
If the success rate is higher, or equal, to this value, FTS3 considers the link
as being well performing. That means, it will check the throughput to decide
what to do next, regardless of changes on the success rate.

If the rate is lower, then FTS3 considers the link to be on a not so good state.
It will check only the evolution of the success rate, and ignore the throughput.

## OptimizerLowSuccessRate
Lower success rate, as a percentage. When the success rate is lower than this,
FTS3 considers that the links has a bad performance. This means that

* If the value is below OptimizerBaseSuccessRate, the number of actives will
  decrease in all cases.
* Between OptimizerBaseSuccessRate and OptimizerLowSuccessRate, the number of actives
  will increase *only* if the success rate is increasing.

## OptimizerBaseSuccessRate
See previous entry.

## BackupTables (default: true)
Enable or disable the `t_file` and `t_job` tables backup.
This happens during the database cleanup procedure.

## CleanBulkSize (default: 5000)
Number of old jobs deleted from the DB per iteration.

## CleanInterval (default: 7)
In days, how long should finished jobs be kept on the database.

## PurgeMessagingDirectoryInterval (default: 600)
In seconds, how often should old intermediate directories used by the internal
messaging be removed.

## CheckSanityStateInterval (default: 3600)
In seconds, how often should FTS3 run a self-check.

## MultihopSanityStateInterval (default: 600)
In seconds, how often to check for multihop inconsistencies.

## CancelCheckInterval (default: 10)
In seconds, how often should FTS3 check for canceled jobs.

## QueueTimeoutCheckInterval (default: 300)
In seconds, how often to check for expired jobs.

## ActiveTimeoutCheckInterval (default: 300)
In seconds, how often to check for stalled transfers.

## CheckStalledTransfers (default: true)
Check for inactive `fts_url_copy` processes.

## CheckStalledTimeout (default: 900)
In seconds, timeout for stalled `fts_url_copy` processes.

## UseFixedJobPriority (default: 0)
Configure the system to use a fixed Job Priority. 
By default, the system will honour user-specified priorities.

## CancelUnusedMultihopFiles (default: false)
When enabled, upon a multihop job failure, move all `NOT_USED` transfers to `CANCEL` file state.
If not enabled, the untouched transfers will remain in `NOT_USED` state.

## RetrieveSEToken (default: false)
When enabled, instructs the FTS Transfer Agent (`fts_url_copy`) to retrieve SE-issued tokens.
This feature should normally be disabled as SE-token retrieval is now fully incorporated in Gfal2 (since `v2.20.0`).

## BackwardsCompatibleProxyNames (default: true)
With the addition of OC11/GDPR changes in FTS, the temporary proxy file names 
generated by the server have changed naming convention:
`/tmp/x509up_h<hash><encoded_DN>` --> `/tmp/x509up_h<hash>_<dlg_id>`.  
For the brief transition period, this flag allows the server to detect proxy file names
following the previous naming convention.
