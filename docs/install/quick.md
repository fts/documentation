FTS3 Quick Setup
================

These are the basic steps to set up a running FTS3 from the distributed RPMs.
We recommend using some sort of automated tool to do this
(i.e. [Puppet](https://puppetlabs.com/)), but it is up to you.

## Supported platforms

#### FTS and DMC clients

The FTS and DMC clients are officially supported on Alma9.
Via EPEL publishing, they are also available on EL8, EL9, the two latest Fedora versions and Fedora Rawhide.

#### FTS Server

The FTS server is supported on Alma9 (from v3.13). Previous versions provide packages for CC7,
but after CC7 end-of-life support will no longer be offered for this platform.

> **Note**: Starting with v3.12.0, the FTS project moved fully to Python3.
> All previous`fts-rest-*` packages have been replaced by the single `fts-rest-server` package. 

#### Dropping support for CC7 (FTS v3.13)

Starting with v3.13.0, CC7 is no longer supported. This decision is brought by the
CC7 end-of-life, marked for June 2024. Previous FTS version RPMs (on CC7) will still
be available for some time, but the FTS team will no longer offer support for those installations.

## Decide which version to use

#### Gfal2 and dependencies

Gfal2 is a hard dependency of the FTS service. Along with Gfal2, 
the following DMC packages are needed: `davix`, `srm-ifce`, `cgsi-gsoap`.

We recommend installing the packages from EPEL for stability. Alternatively, 
to receive the latest updates as soon as possible, install them 
from the DMC Production repositories. Ultimately, for those running 
FTS integration instances, install them from the DMC Devel repositories.

 * **[EPEL Stable][1]**, updated from EPEL testing after 14 days
 * **[EPEL Testing][2]**, updated shortly after a release has been deployed to FTS@CERN production services
 * **DMC Production** ([EL9][3]), stable versions applied on FTS@CERN production services
 * **DMC RC** ([EL9][4]), release candidates for the DMC packages
 * **DMC Devel** ([EL9][5]), continuously built from the development branch

#### FTS Client

The FTS client is made available in the EPEL and FTS repositories.
We recommend installing it from the EPEL repositories.

The FTS client is a Python3-only package and made available on a variety
of platforms via EPEL. The FTS repositories only provide it for EL9.

> **Note**: Up to FTS v3.12.0, the `fts-client` package (C++) is available. 
> Starting with v3.12.0, the `fts-client` has been deprecated and
> replaced by the `fts-rest-client` package.
 
#### FTS Server

The FTS Server packages are only made available **only** via the FTS repositories.
Similarly, an `FTS Dependencies` repository is provided and necessary.

The FTS server is provided **only** for the EL9 platform.

  * **FTS Production** ([EL9][6]), stable versions applied on FTS@CERN production services
  * **FTS RC** ([EL9][7]), release candidates for the FTS packages
  * **FTS Devel** ([EL9][8]), continuously built from the development branch

No matter which version you select, yo will need the FTS dependencies repository
for packages that are custom-packaged by the FTS team:

  * **FTS Depend** ([link][9]), repository for specific FTS dependencies


## Install the FTS3 packages

#### Enable pre-requisites

The EPEL (Extra Packages for Enterprise Linux) and CRB (CodeReady Linux Builder) repositories
are needed to install FTS packages. They are not installed/enabled by default on Alma9. The
following commands should enable them:
```bash
$ dnf install epel-release
$ dnf config-manager --set-enabled crb
```

#### Base packages
```bash
$ dnf install fts-server fts-mysql fts-rest-client fts-rest-server fts-monitoring
```

#### SELinux rules
```bash
$ dnf install fts-server-selinux fts-rest-server-selinux fts-monitoring-selinux
```

#### Optional packages
```bash
$ dnf install fts-msg # To send messages to an ActiveMQ broker
```

## Root CA certificates

You will need to install the set of Root CA's that your server will support.
The Grid root CAs are governed by [IGTF](https://www.igtf.net/).

In order to install the Grid root CAs, a pragmatic approach
is creating the `/etc/yum.repos.d/EGI-trustanchors.repo` file with the following content:
```
[EGI-trustanchors]
name=EGI-trustanchors
baseurl=https://repository.egi.eu/sw/production/cas/1/current/
enabled=1
gpgcheck=1
gpgkey=https://repository.egi.eu/sw/production/cas/1/GPG-KEY-EUGridPMA-RPM-3
```
Upon creating the repository file, run the following command:
```bash
$ dnf install ca-policy-egi-core
```

#### Certificate Revocation lists

Install `fetch-crl`. By default, it installs a file under `/etc/cron.d`
to run it every 6 hours. Make sure to run it at least once
before starting the HTTPd daemon.

```bash
$ dnf install fetch-crl
$ fetch-crl
```

#### FTS3 server certificates

All FTS3 public APIs (REST, Monitoring) are accessible via SSL. For this,
you will need to generate a pair of public certificate and private key.
Store this pair as
`/etc/grid-security/hostcert.pem` and `/etc/grid-security/hostkey.pem`, owned by
`root`.

Remember that the private keys must be readable *only* by their owner.

#### VOMS LSC files

For each VO that is supported the directory `/etc/grid-security/vomsdir/<vo-name>`
should be created and should contain the corresponding VOMS server's LSC file,
named after the fully qualified hostname followed by a .lsc extension.

Unfortunately, this step must be done manually for each VO.

## Setting up the database

You will need a MySQL database already running somewhere (preferably
in a physical machine, different from the one running FTS3)

Before starting fts-server, a database must be created.
The respective SQL scripts are available at:

  * `/usr/share/fts-mysql/fts-schema-9.0.0.sql`

If you were coming from an older version of FTS, you may need to run diff scripts.
They must be called in order, starting with the first with a version greater than
the existing schema. For instance:

  * `/usr/share/fts-mysql/fts-diff-9.0.0.sql`

For convenience, you may use the script

  * `/usr/share/fts/fts-database-upgrade.py`

You will need to create the database and grant privileges to the MySQL FTS3 user:
```sql
CREATE DATABASE '<database>';
CREATE USER '<username>';
GRANT ALL ON <database>.* TO '<username>'@'%' IDENTIFIED BY '<password>';
FLUSH PRIVILEGES;
```

Also, SUPER privileges are required so FTS3 can change some parameters:
```sql
GRANT SUPER ON *.* to '<username>'@'%' IDENTIFIED BY '<password>';
FLUSH PRIVILEGES;
```

## Basic configuration of FTS3

There are only two configuration files in use by FTS3 server and FTS3 messaging daemon:

  * `/etc/fts3/fts3config`
  * `/etc/fts3/fts-msg-monitoring.conf`

Both config files come with predefined values (wherever possible). Check both files and
complete the missing values (database credentials and message broker credentials).
Please contact the `dashboard-support@cern.ch` mailing list to be provided with the
password to connect to message broker.

The FTS REST component uses the following configuration file:

  * `/etc/fts3/fts3restconfig`

This file also comes with predefined values, but you will have to fill the missing ones.
You can refer to the section [REST](fts3rest.md) to have more information about the configuration
parameters.

#### Open required ports in the firewall

  * 8446 for REST
  * 8449 for the Web Monitoring

## Start the service

```bash
# Core FTS3
systemctl start fts-server
systemctl start fts-qos
# REST API and Web Monitoring
systemctl start httpd
# Messaging (optional)
systemctl start fts-msg-bulk
```

#### Cronjobs

Additionally, FTS3 also ships with the following cronjobs:

  * `/etc/cron.daily/fts-records-cleaner` executes a process that archives or deletes old transfers
  * *`/etc/cron.hourly/fts-info-publisher` generates the .ldif for the BDII **(deprecated)** *
  * *`/etc/cron.daily/fts-bdii-cache-updater` updates the cache from the BDII **(deprecated)** *

> **Note**: On CC7, a regular restart of the Apache server was needed in order to work around a bug in `mod_ssl`
> when reloading the CRLs. On Alma9, it is presumed this is no longer needed, but not yet fully confirmed


[1]: https://fedoraproject.org/wiki/EPEL
[2]: https://fedoraproject.org/wiki/EPEL/testing
[3]: https://dmc-repo.web.cern.ch/dmc-repo/dmc-el9.repo
[4]: https://dmc-repo.web.cern.ch/dmc-repo/dmc-rc-el9.repo
[5]: https://dmc-repo.web.cern.ch/dmc-repo/dmc-devel-el9.repo
[6]: https://fts-repo.web.cern.ch/fts-repo/fts3-el9.repo
[7]: https://fts-repo.web.cern.ch/fts-repo/fts3-rc-el9.repo
[8]: https://fts-repo.web.cern.ch/fts-repo/fts3-devel-el9.repo
[9]: https://fts-repo.web.cern.ch/fts-repo/fts3-depend.repo
