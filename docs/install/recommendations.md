General recommendations
=======================

## Deployment considerations
FTS3 is designed to be easy to scale horizontally, so all FTS3 nodes are equal
to each other, and have the same configuration. The load is split evenly
across nodes both for staging and transferring.

These machines can be, and often are, virtualized. However, the database should
be installed on a physical machine with enough memory (for the cache) and good IO,
*not* shared with any other database or service if possible, if the foreseen load
is going to be high.

For the FTS3 nodes the factor to account for is mainly the number of concurrent
transfers to be sustained, since the memory imposes a limit on the number of
`fts_url_copy` processes that can run on a given moment. IO on these machines
will not be heavy, so they can easily be virtualized. However, remember that
disk space should be enough to store the service and transfer logs enough time
in order to use them to eventually debug possible problems.

It is recommended to have, at least, two or three independent FTS3 services,
rather than one single instance with enough memory to run the equivalent number
of transfers. Since FTS3 nodes will dynamically adapt when a node joins or leaves,
they will also adapt if a node dies, taking others automatically part of the share
allocated previously to the deceased node while it comes back. This increases the
overall service resiliency. Of course, this also allow to reduce the risk of over
or under provisioning the service, since nodes can be added or removed as needed.

## MySQL Configuration
FTS3 uses InnoDB as a storage engine for MySQL.

Innodb Buffer Pool (`innodb_buffer_pool_size`) is by far the most important option
for Innodb Performance and it must be set correctly. It is recommend to be set to
60%-70% of the available RAM.

Since the ratio of FTS3 queries to insert/delete/update is approximately 91% select
vs 9% insert/delete/update, we recommend to leave the default `innodb_flush_method`
(DSYNC). However, there is no rule of thumb here, you may experience better
performance using `O_DIRECT` in some cases.

## Database version
FTS3 at CERN runs with MySQL 8.0.28.

### Notes
InnoDB fragmentation may hurt FTS3 queries significantly, especially when data
is not in `buffer_pool` and the execution goes to read from disk. A recipe for
this is to execute "mysqlcheck -o <db name> --auto-repair --optimize" once every
month. Mind the database tables will be locked while reclaiming the lost space
by rebuilding the tables

Run 'grep -i "scheduled" /var/log/fts3/fts3server.log'. If the time-stamp gap
between log entries is more than 1 minute, it means something is causing latency,
probably fragmentation.

## MaxUrlCopyProcesses value
The objective of this configuration variable is to avoid having more `fts_url_copy`
processes than a host can handle, causing performance problems, and even a freeze.

The limitation is imposed by the amount of memory that a host has. If the memory
pressure is too high, the system will start swapping, degrading the overall
performance.

By default it is limited to 400. A good rule of thumb is to take the amount
of memory your system has, and divide by the average resident memory used by a
`fts_url_copy` process (~15 MB as of FTS 3.7).

For instance, if 400 transfers are scheduled on a single node, the memory
requirement will be, roughly, `400 * 15 MB = 6000 MB = 6 GB`.

Of course, you can reverse the equation. If your system has 12 GB to spare,
then a single host could run 800 transfers.
