Backup Policy
=============

These are just recommendations about how to handle backups of the FTS3 database. The final decision is left for the sites running the service.

## Discussion on failure consequences
In general, it is considered good practice to perform regular database backups. With them, the service can be recovered in case a failure with data corruption/loss affects the database.

However, in the case of FTS3, recovering the service as it was in a moment in time previous to the failure may be a very bad idea.

Imagine we do a backup at 10.00 AM. If we are lucky, the database is lost at 10.01 AM and the service goes down. We can recover the service with minor losses (still, recreating the database from backup will take time).

If we are not that lucky, and the database goes down for good several hours later, if we get to recover the database from the backup, FTS3 will start running with the queue **as it was hours before**. Which means transfers that had already been executed, will run again. This effect can be worsened if users/experiments decide to re-submit lost transfers to a independent FTS3 service (i.e. WLCG use case).

This transfer duplication can cause great trouble, specially if transfers that had succeeded, fail the second time. Files that were considered transferred and in place, all of the sudden are gone.

## Recommendations for the recovery
Do not *ever* start the FTS3 service again from a backup. Of course, if you can make the service resilient to database failures using replication on a cluster, much better. But if the database is completely gone and the data lost, let it be lost.

## Recommendations for the backup
One may argue that the backup can, at least, be used for a post-mortem, and finding out what was in the queue before the failure. This will not be, however, an accurate picture, since it will depend on how much time passed between the backup and the failure.

Additionally, backups impose an extra load on the MySQL server. Specially if the whole database is locked during the operation.

Therefore, if backups remain enabled, either use `–single-transaction` to avoid a read-only database during the backup, or perform it from a [slave replica](http://dev.mysql.com/doc/refman/5.7/en/replication.html), which will reduce the load further.

It still is possible to fully drop backups. This will depend, of course, on your users' preferences and how comfortable you are not having backups. But remember, records are purged periodically anyway, so most of the data in the FTS3 database is, by nature, transient.

## If backups are disabled
In the previous point we said "most of the data [...] is transient". There is, however, an exception worth mentioning: configuration.

Some of the tables hold data that is longer lived, and make sense to restore after a catastrophic failure. These tables are

* `t_server_config`
* `t_optimize_mode`
* `t_optimize`
* `t_config_audit`
* `t_debug`
* `t_se`
* `t_group_memebers`
* `t_link_config`
* `t_share_config`
* `t_activity_share_config`
* `t_bad_ses`
* `t_bad_dns`
* `t_vo_acl`
* `t_authz_dn`
* `t_cloudStorage`
* `t_cloudStorageUser`
* `t_oauth2_*` if present
