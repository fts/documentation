Messaging configuration
=======================
Stored under `/etc/fts3/fts-msg-monitoring.conf`

## ACTIVE
Enable (true) or disable (false) the submission of messages to the broker.

## Broker
Host and port of the AMQ broker.

## START, COMPLETE, CRON and STATE
Topics for start messages, completion messages, cron messages
(as overall service status, queue...) and state transition messages.

## LOGFILEDIR
Where to store the messaging daemon log.

## LOGFILENAME
The messaging log file name.

## TOPIC
Send to an AMQ topic (true) or queue (false)

## FQDN
FTS3 fully qualified domain name to use for the CRON messages.

## TTL
Time To Live. Time the message should be retained by the message system. in milliseconds.

## USERNAME
Contact dashboard-support@cern.ch (link sends e-mail) list for more details

## PASSWORD
Contact dashboard-support@cern.ch (link sends e-mail) list for more details

## USE\_BROKER\_CREDENTIALS
If true, use USERNAME and PASSWORD to connect to the broker. Otherwise, connect
anonymously.

## SSL
Set to true to enable SSL.

## SSL\_VERIFY
Set to false if you don't want to verify the peer certificate.

## SSL\_ROOT\_CA
Set to a .pem file containing the root CA for the peer.

## SSL\_CLIENT\_KEYSTORE
Set to a .pem file containing both the client certificate and private key.

## SSL\_CLIENT\_KEYSTORE\_PASSWORD
If the private key is password-protected, enter it here.
If you set this, make sure that only the user that runs fts3 is allowed to read this file!

