Installation
============
If you have the FTS repositories installed in your machine the following step should be enough to install
the REST interface:
```
yum install fts-rest-server
service httpd restart
```

If you have enabled SELinux, for convenience you can install fts-rest-server-selinux, which contains the rules needed to
have REST working (i.e. allow Apache to connect to the database, allow Apache to bind to 8446)

```
yum install fts-rest-server-selinux
```

If you are not installing the REST interface on the same machine where FTS3 is running, remember to install
the CA Certificates (`ca-policy-egi-core`) and `fetch-crl` first!

```
yum install ca-policy-egi-core fetch-crl
fetch-crl -v
```

Configuration
=============

The REST interface configuration file is stored under `/etc/fts3/fts3restconfig`.  The most important parameters are
the database connection details, logging settings and the OpenID parameters. The following list details some of these parameters.

* **DBType** (default: mysql):
  * Database backend type. Determines which DB backend plugin is loaded. Only 'mysql' is currently supported
* **DBConnectString** (no default):
  * Connect string for the used database account. For MySQL, the connection string has to follow the format
  `host(:port)/db` (i.e. `"mysql-server.example.com:5050/fts3db`)
* **DbUserName** (default: empty):
  * Database account username
* **DbPassword**  (default: empty):
  * Database account password
* **Alias** (default: empty)
  * The alias of the FTS endpoint (used in monitoring):
* **SiteName** (default: empty)
  * Site name, e.g. CERN-PROD
* **MonitoringMessaging** (default: true):
  * *Enable or disable monitoring over messaging mechanism (enable=true / disable=false)
* **AuthorizedVO** (default: *):
  * A ";" separated list of the VOs which will be allowed to submit to the FTS service 
  (e.g.: `AuthorizedVO=atlas;dteam;cms`)  
    Leave '*' to accept all VOs.
* **ValidateAccessTokenOffline** (default: True):
  * Boolean that indicates whether to validate access tokens offline using the cached JWK.
* **JWKCacheSeconds** (default: 86400):
  * The number of seconds the JWKs from the issuer should be cached
* **TokenRefreshDaemonIntervalInSeconds** (default: 600)
  * The interval in seconds at which fts-rest should refresh stored access tokens. Leave a '*'
  for accepting all VOs (default)
* **AutoSessionReuse** (default: false):
  * Enable or disable auto session reuse
* **AutoSessionReuseMinFiles** (default: 5):
  * Min number of files to enable auto session reuse
* **AutoSessionReuseMaxFiles** (default: 1000):
  * Max number of small files to enable auto session reuse
* **AutoSessionReuseMaxBigFiles** (default: 2):
  * Max number of big files to enable auto session reuse
* **AutoSessionReuseMaxSmallFileSize** (default: 104857600 [100MB]):
  * File size max limit to classify file as small, expressed in bytes
* **AutoSessionReuseMaxBigFileSize** (default: 1073741824 [1GB]):
  * File size max limit to classify file as big, expressed in bytes
* **Providers**:
  * Multiple providers supported. Under the configuration section `[providers]`, three lines must be specified
  for each provider
    ```
    [providers]
    xdc=https://iam.extreme-datacloud.eu
    xdc_ClientId=...
    xdc_ClientSecret=...
    wlcg=https://wlcg.cloud.cnaf.infn.it
    wlcg_ClientId=...
    wlcg_ClientSecret=...
    ```
    The first line should be 'providername=URL'
    The second line should be 'providername_ClientId=clientId'
    The third line should be 'providername_ClientSecret=clientSecret'
* **Roles**:
  * Different roles can be configured under the `[roles]` section.
      ```
      Public = transfer
      lcgadmin = vo:transfer
      production = all:config
      ```
      In general, it has the form:
      ```
      role = (scope:)privilege
      ```
      Public is a special role, granted to all users.

      If more than one privilege has to be assigned to a single role they must be
      semicolon-separated. e.g.

      ```
      lcgadmin = all:transfer;all:config.
      ```

      Currently supported privileges are `transfer` and `config`.

      If all the privileges have to be assigned to a single role an asterisk may be used (\*).

      Scopes explained:

      * Without a scope, the user with that role is allowed to handle his/her
        own transfers
      * With the scope 'vo', the user with that role is allowed to handle
        the transfers for the whole VO
      * With the scope 'all', the user with that role is allowed to handle
        all transfers

Additionally, the configuration file can also be used to
tune [SQLAlchemy parameters](https://docs.sqlalchemy.org/en/14/core/engines.html).

* `sqlalchemy.pool_size` can be increased to have more available connections.
* `sqlalchemy.pool_timeout` should be set to a reasonable low value (i.e. 10) to avoid clients hanging indefinitely on a connection when there are no DB connections available. Starting with FTS-REST 3.5, these timeouts will raise a `503 HTTP Service unavailable`,


Is it up?
=========
You can check the service is up and running just opening with your browser <https://yourhost:8446/whoami>.
If everything is properly set, you will see something like

```json
{
  "delegation_id": "123456789abcdef",
  "dn": [
    "/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=username/CN=12345/CN=Name"
  ],
  "level": {
    "config": "all",
    "transfer": "vo"
  },
  "roles": [],
  "user_dn": "/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=username/CN=12345/CN=Name",
  "voms_cred": [],
  "vos": [
    "123456789abcdef"
  ]
}
```
