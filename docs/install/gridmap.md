Gridmap configuration
=====================

Starting with FTS v3.12, FTS takes a step forward in implementing 
OC11/GDPR privacy polices. One of the main features affected is the way 
FTS "constructs" a VO for users who connect with a certificate that
contains no VOMS information.

The VO is a central piece in the FTS system. Permissions to query / update
transfer submissions are based on VO permissions. Internal scheduling also
uses VO to ensure fair scheduling. Furthermore, the VO is published to
monitoring systems, where grouping by VO can be done. With the VO
published to monitoring systems, it can be seen in both WLCG Transfer dashboards 
and in the FTS3 Web Monitoring webpages.

#### FTS-generated VO value

Since FTS always needs a VO identity, it does the following:
* Read and use the certificate VOMS data (if it exists)
* Artificially create a VO for the user when the certificate has no VOMS data

Previously, the VO construction used the user DN as input. 
The generated VO would resemble an e-mail address:
```
subject = /DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=ftssuite/CN=773756/CN=Robot: fts3 testsuite
FTS_constructed_vo = ftssuite@cern.ch
```

It was considered that the FTS-generated VO can expose sensitive information.

#### New VO generation

Starting with FTS v3.12, the newly constructed VO value will be the user's delegation ID:
```bash
subject = /DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=ftssuite/CN=773756/CN=Robot: fts3 testsuite
$ davix-get --cert ftssuite.cert https://fts3-pilot.cern.ch:8446/whoami | jq -r .vos[0]
c8caac807c7c4bdb
```

### The Gridmap table

When it comes to monitoring, the new change will prove intrusive for users 
that submit transfers without VOMS data. In order to accommodate this scenario,
the FTS software supports a way to override the VO-generation algorithm and
use pre-configured values. This is done via the `t_gridmap` table, which
allows mapping a `dn` <--> `vo`. 

Example:
```sql
INSERT INTO t_gridmap VALUES ("/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=ftssuite/CN=773756/CN=Robot: fts3 testsuite", "ftssuite@cern.ch");
```
```bash
$ davix-get --cert ftssuite.cert https://fts3-pilot.cern.ch:8446/whoami | jq -r .vos[0]
ftssuite@cern.ch
```
