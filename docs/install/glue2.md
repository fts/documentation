# Publishing GLUE2
This service is optional, and only required for WLCG.

The only requirements are:
  * Install `fts-infosys`
  * Install `bdii`
  * Start the `bdii` service

```bash
yum install fts-infosys bdii
service bdii start
```

`fts-infosys` installs a cronjob that runs hourly. This cronjob generates
a file under /var/lib/bdii/gip/ldif, which then will be used by the bdii
service.

