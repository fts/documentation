FTS OAuth2 Token Configuration
==============================

This page will describe how to enable and configure your FTS3 instance
for OAuth2 Token support. This guide applies from FTS v3.13.0, which includes
the fully-reworked token support, released as beta-version. FTS v3.14.0
is expected to officially release the reworked token support.

For general (FTS) token considerations, as well as how to submit token-based
transfers to FTS, please consult the [OAuth2 Token Support](../token_support.md) page.

## Obtain your client ID / secret from the TokenProvider

First and foremost, for the FTS deployment, you will need a
`client_id / client_secret` pair for each TokenProvider
you want to support. Usually, this will imply some kind of
registration process with the TokenProvider.

During the registration process, make sure you obtain the following for your FTS client:
- The `urn:ietf:params:oauth:grant-type:token-exchange` grant (*)
- The `refresh_token` grant
- The `offline_access` scope (required for the `token-exchange` part) (*)
- `Client secret over HTTP basic authentication` with the token endpoint
- The full list of scopes your FTS instance may encounter from the userbase.  
FTS will have to perform `token-exchange` for other users' tokens. When
FTS performs the `token-exchange`, it requests the same scope list as
the initial access token, which means FTS has to be granted permissions
to request those scopes.
- (Optional) Ask the TokenProvider to set up an `fts` scope (*)  
The FTS instance doesn't need the `fts` scope but the users which
will interact with the FTS instance may need to have the `fts` scope
in the `<fts-token>` (the token used by FTS to identify users and
assign transfer ownership)

**Note**: Operations marked with (*) will require TokenProvider administrator access

At the end of the registration process, you should have your
`client_id / client_secret` pair for this TokenProvider.

## Configure FTS token support

Most of the configuration will be done in the FTS-REST component, namely
the one that runs within Apache on port 8446. Some configuration must be done
in the core FTS server. The next step is enabling the FTS token daemons.
A final step is to add the TokenProviders information in the database.

### Configuring the FTS-REST component

The following settings must be applied to the `/etc/fts3/fts3restconfig` file:

1. Enable `OAuth2 = true`

2. Set `AuthorizedAudiences=<audience-list>`  
Set a list of authorized audiences that the `<fts-token>` must have.
The list is delimited by the semicolon `;` character. If not set,
the implicit `https://wlcg.cern.ch/jwt/v1/any` value is used
```
AuthorizedAudiences=fts3-pilot.cern.ch;https://wlcg.cern.ch/jwt/v1/any
```

3. Create a `[providers]` section
4. Fill-in the `client_id /client_secret` pairs for each supported TokenProvider.
The format is the following:
```
<name> = <issuer>
<name>_ClientId = <client-id>
<name>_ClientSecret = <client-secret>
<name>_OauthScopeFts = fts  # Optional
```
Example for the `dteam` provider:
```
dteam = https://dteam-auth.cern.ch
dteam_ClientId = <client-id>
dteam_ClientSecret = <client-secret>
dteam_OauthScopeFts = fts
```
The `<name>` value can be anything, but you will have to keep it the same
in the subsequent 2-3 fields. We recommend choosing a name without
special characters (especially `_`) in order to keep things simple.

The `<issuer>` value has to be that URL that can serve the
`<issuer>/.well-known/openid-configuration` endpoint.

When `_OauthScopeFts` is set, FTS will require this scope exist in the `<fts-token>`.

### Configuring the FTS server component

The following settings must be applied to the `/etc/fts3/fts3config` file:

1. Set the `TokenExchangeCheckInterval=60` value  
How often to run the TokenExchange service. Default and recommended value is `60` seconds.

2. Set the `LogTokenRequests=<true|false>` flag  
Flag whether to print the access tokens in the `/var/log/fts3/fts3server.log` file.
Default and recommend value is `false`.

### Enabling the FTS token daemons

At this point, the FTS server will be able to perform the `token-exchange`
on user-provided access tokens. The next step is to enable the FTS token daemons:
- The **FTS Token Refresher** daemon: responsible for periodically refreshing
access tokens. It does this by scanning the database for access tokens which
belong to active transfers and are soon to expire. The SQL query used
has been optimized for efficiency
- The **FTS Token Housekeeper** daemon: responsible for the periodical
token clean-up, removing old and unused token entries from the database

```bash
$ systemctl start ftstokenrefresherd
$ systemctl start ftstokenhousekeeperd
```

### Add TokenProvider information to the database

The `t_token_provider` table is used by the FTS server and FTS token daemons.
This table must have the same TokenProvider information as contained in the FTS-REST
config file (`/etc/fts3/fts3restconfig`).

Example for the `dteam` Token Provider:

```sql
> INSERT INTO t_token_provider VALUES ("dteam", "https://dteam-auth.cern.ch/", "<client-id>", "<client-secret>");

+-------+-----------------------------+-------------+-----------------+
| name  | issuer                      | client_id   | client_secret   |
+-------+-----------------------------+-------------+-----------------+
| dteam | https://dteam-auth.cern.ch/ | <client-id> | <client-secret> |
+-------+-----------------------------+-------------+-----------------+
```

Congratulations, once you've reached this point, your FTS instance should be
configured for FTS token support!

## Testing TokenProvider integration with FTS

FTS requires certain privileged capabilities from the TokenProvider,
such as being able to perform `token-exchange`. Due to this requirement,
FTS is considered a privileged actor by the TokenProvider.

Before configuring the FTS instance with the `client_id / client_secret` pair,
the same operations can be tested using an HTTP client to ensure the integration
will work. This section will describe exactly the steps involved.

Two pairs of `client_id / client_secret` will be needed. The user pair,
used to obtain the access token, and the FTS pair, which will mimic the
operations FTS needs. They are defined by the following nomenclature:
- `${USER_CLIENT_ID} / ${USER_CLIENT_SECRET}` -- the user pair
- `${FTS_CLIENT_ID} / ${FTS_CLIENT_SECRET}` -- the FTS pair

During all examples, the `dteam` issuer will be used, but these operations
should work with any compliant TokenProvider.

### Obtaining an access token

Using the user pair, obtain an access token from the TokenProvider:
```bash
$ export USER_CLIENT_ID=<client-id>
$ export USER_CLIENT_SECRET=<client-secret>
$ export ISSUER="https://dteam-auth.cern.ch/"

$ export SCOPE="storage.read:/ storage.create:/ offline_access"
$ export AUDIENCE="https://wlcg.cern.ch/jwt/v1/any"

$ export TOKEN_ENDPOINT=$(curl -s "${ISSUER}/.well-known/openid-configuration" | jq -r .token_endpoint)

$ curl -s -X POST --user "${USER_CLIENT_ID}:${USER_CLIENT_SECRET}" \
       -H "Content-Type: application/x-www-form-urlencoded" \
       --data "grant_type=client_credentials&scope=${SCOPE}&audience=${AUDIENCE}" \
    ${TOKEN_ENDPOINT} | jq

$ export TOKEN=<access-token>
```

**Notes**:
- We are retrieving an access token that will be used to access the
storage endpoints. FTS will perform `token-exchange` and `token-refresh`
operations only on the tokens meant for data access. For FTS to be able
to perform  the `token-exchange` operation, the tokens must have
the `offline_access` scope in them
- Please use different pairs for the user and the FTS client pairs,
as the `token-exchange` cannot be performed on a token you own
- You are encouraged to also obtain a token with the `fts` scope,
which will be used to interact with FTS, in order to verify your user pair
has the correct settings

### Reproducing the FTS `token-exchange` operation

For the `token-exchange` operation, FTS will mirror the initial access token's
`scope` and `aud` claims. After the `token-exchange` operation, FTS should
receive a JSON containing the refresh token and possibly a new access token.
The below example starts with zero previous knowledge about the access token,
extracting all the fields from the access token itself.

**Note**: For example purposes, a very simplistic token decoding function
is defined. It is not complete or expected to work in all scenarios. In practice,
you are encouraged to use something more robust such as PyJWT

```bash
# Define the token decoding function
$ token-decode() { echo "$1" | cut -d. -f2 | base64 --decode 2>/dev/null | jq ; }

$ export ISSUER=$(token-decode "${TOKEN}" | jq -r .iss)
$ export SCOPE=$(token-decode "${TOKEN}" | jq -r .scope)
$ export AUDIENCE=$(token-decode "${TOKEN}" | jq -r .aud)
$ export TOKEN_ENDPOINT=$(curl -s "${ISSUER}/.well-known/openid-configuration" | jq -r .token_endpoint)

$ export FTS_CLIENT_ID=<fts-client-id>
$ export FTS_CLIENT_SECRET=<fts-client-secret>

$ curl -s -X POST --user "${FTS_CLIENT_ID}:${FTS_CLIENT_SECRET}" \
       -H "Content-Type: application/x-www-form-urlencoded" \
       --data "grant_type=urn:ietf:params:oauth:grant-type:token-exchange&requested_token_type=urn:ietf:params:oauth:token-type:refresh_token&subject_token_type=urn:ietf:params:oauth:token-type:access_token&subject_token=${TOKEN}&scope=${SCOPE}&audience=${AUDIENCE}" \
    ${TOKEN_ENDPOINT} | jq

$ export TOKEN=<access-token>
$ export REFRESH_TOKEN=<refresh-token>
```

### Reproducing the FTS `refresh-token` operation

For the `refresh-token` operation, FTS will again mirror the access token's
`scope` and `aud` claims. After the `refresh-token` operation, FTS should receive
a JSON containing the refresh access token. For brevity, the `${ISSUER}`, `${SCOPE}`
and `${AUDIENCE}` values from the previous example are assumed to still be available.

```bash
$ curl -s -X POST --user "${FTS_CLIENT_ID}:${FTS_CLIENT_SECRET}" \
       -H "Content-Type: application/x-www-form-urlencoded" \
       --data "grant_type=refresh_token&refresh_token=${REFRESH_TOKEN}&scope=${SCOPE}&audience=${AUDIENCE}"
    ${TOKEN_ENDPOINT} | jq

$ export TOKEN=<access-token>
```

If everything worked up until here, congratulations, your TokenProvider is ready
to work with FTS! In case things did not work, please review the user and FTS client
configurations, as well as check with your TokenProvider on the support
they offer for the `token-exchange` and `refresh-token` grants.

### FTS Token Integration Compliance script

For automation and convenience, the above steps have been turned into an FTS Token
Integration Compliance script: [fts-token-compliance.py][1]

The script will need access to a configuration file holding the FTS client
and the location to a file where the user access token is written.

Example:
```bash
$ cat fts-dteam-integration.ini

[fts]
issuer = https://dteam-auth.cern.ch/
client_id = <client-id>
client_secret = <client-secret>

[token]
access_token_file = dteam.token
```
```bash
$ fts-token-compliance.py fts-dteam-integration.ini
```

If the `fts-token-compliance.py` script passes,
so should the TokenProvider and FTS integration.


[1]: https://gitlab.cern.ch/fts/scripts/-/blob/master/tokens/fts-token-compliance.py
