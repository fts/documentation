Session Reuse
=============

FTS3 has a functionality called "session support", which is well suited for use cases with a lot of small files between two endpoints.

## What
When a job is marked as a "session reuse" job, it means all the transfers belonging to that job will be done one after the other in a single `url copy` process.

This allows establishing the connection and doing the SSL handshake only once, or at least few times. For small files, the overhead of the TCP and SSL handshakes can be quite expensive due to the latency.

## When
When there are a lot of small files between A and B, group them in bulk jobs with session reuse enabled. This way, you exploit both the lower overhead and the parallelism.

Of course, there is still the question of how "small" is small, and how big a job should be. This is hard to tell, since it depends on the storages, network link, load... As a rule of thumb, consider using session reuse  **when the time to transfer a single file is of the order of 1 second, and the latency is high**.

Fill a single job with as many files it takes until the aggregated duration is on the order of few minutes.

## How
Via the CLI, specify the `-r/--reuse` flag with `fts-transfer-submit`, that's all it takes.

Via REST, set the `reuse` parameter to `true`.

```json
{
    "files": [],
    "params": {
        "reuse": true
    }
}
```

## Automatic Session Reuse
Starting from FTS 3.8, the Server can be configured to enable Automatic Session Reuse. This means that it can decide based on some parameters if it's worth trying to run an FTS job with session reuse enabled. 
For this, just do not specify any value for `reuse`, or leave it set to null, plus for each file part of the job the file size needs to be included (`filesize` parameter) as this is taken into account by the server to decide whether or not enabling the feature. Of course the job should contain only transfers with 2 endpoints.

Server side the following parameters have been added:

   * `AutoSessionReuse`: Enable or disable auto session reuse (default: false)
   * `AutoSessionReuseMaxSmallFileSize`: Max size in bytes for the small files part of the FTS job (default: 104857600, 100MB)
   * `AutoSessionReuseMaxBigFileSize`: Max size in bytes for the big files part of the FTS job (default: 1073741824: 1GB)
   * `AutoSessionReuseMaxFiles`: Max number of small size files in the FTS job (default: 1000)
   * `AutoSessionReuseMaxBigFiles`: Max number of big size files in the FTS job(default: 2)
