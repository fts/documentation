Tags
====

## Naming
Naming tags is straight forward. Just the version number preceded by v. For instance: `v3.5.6`

## Signing
Signing tags is *not* mandatory. It doesn't hurt, though. The only constraint is that the tag
*must* be signed with the key that belongs to the person doing the tag.
Not by any shared key, nor a robot key.

Arguably, this adds a bit of extra accountability, since people would be able to verify the
code they are running is the code that was tagged.

See [Signing Your Work](https://git-scm.com/book/es/v2/Git-Tools-Signing-Your-Work) for a guideline
on how to sign tags.

