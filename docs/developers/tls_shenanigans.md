TLS/X509 Shenanigans
====================

Authentication in the Grid is usually based on X509 certificates and proxies.
It is beyond the scope of this document to enter into the cryptographic details
of these ([more details](https://en.wikipedia.org/wiki/Public-key_cryptography)),
but there are some particularities worth mentioning.

Most users are already familiar with server side certificates, since
more and more web sites move towards HTTPS. The fact that the connection is
secure not only means that the data is encrypted, but also that the server
is who it says it is. The way this works is: the server gives the client a
X509 certificate, signed by an entity - called a CA - which the client
trusts. For instance, the CERN CA.

> Of course, the *connection* is secured against *third parties*. This mechanism
> does *not* protect against malicious or compromised sites

Only `fts3.cern.ch` can have a certificate signed by a CA saying it is, indeed
`fts3.cern.ch`. How the CA verifies the identity doesn't really matter here.
The fact is, the client trusts it.

> Again, this is assuming the private key that corresponds to the certificate
> has not been stolen.

Client authentication works in the same manner: the server will present the
client a certificate, and ask the client for its own. Then, it will trust
the client is who it says it is if the certificate is signed by a CA
the server trusts.

It is fairly straight-forward, really.

Now, the complication comes two ways:

1. On the Grid, users normally generate "proxy certificates"
2. Client certificate support is amazingly poor


If a "normal" certificate chains trust like this:

`User => Intermediate CA => ROOT CA`

A "proxy" certificate looks like this:

`Proxy => User => Intermediate CA => ROOT CA`

Plus [VOMS](https://en.wikipedia.org/wiki/VOMS) extensions and the like
(group membership).


So, normally a server has the intermediate CA installed ("trusted"). So if
a user shows a regular certificate, the server immediately sees it is signed
by a trusted entity.

But, obviously, if the user shows a proxy, the server has no way of trusting it.
The user needs to send the proxy *and* the certificate, so the server can follow
the chain up to a trusted link.

## NSS vs OpenSSL
And here there is a problem when it comes to NSS vs OpenSSL. And the `curl`
shipped in different systems link with different TLS backends. For instance,
OpenSSL in "Debian & Family", NSS in "CC7 & Family".

* OpenSSL will send the full chain, so the server will be able to validate the proxy
* NSS will *not* send the full chain, *unless* the CA file (`--cacert` parameter
  for `curl`) points points to the proxy as well.

OK!, so just make sure `--cacert` is used with `curl` in CC7 and the like, right?
Well, yes but no. If the verification of the remote certificate is disabled (`--insecure`),
NSS ignores the CA file, thus, it won't send the whole chain, breaking the
authentication.

In summary:

* curl with OpenSSL: all good
* curl with NSS: With proxies, `--cacert` required, and *can't* use `--insecure`

Note, this applies to PyCURL as well, of course.

## M2Crypto
The REST Python Bindings rely on M2Crypto for handling the client certificates.
**Always** create the virtual environment, if you do, with `--system-site-packages`,
and install M2Crypto with the system packager.

The installation of M2Crypto via `pip` may work, but it will break at runtime
on Ubuntu due to the deprecation of SSLv3. It will complain about a missing
symbol.

In summary:
* Install M2Crypto from the system repositories
