# Clion Remote Development Setup

> **Disclaimer**: this section applies only for CERN account holders 
> who want to develop and run their FTS instance on their remote OpenStack VM

Developers may use [Clion](https://www.jetbrains.com/clion/) using the
[Remote Mode](https://www.jetbrains.com/help/clion/remote-projects-support.html)
to write code on their local machine, but sync the changes to and operate on
their VM over SSH.

It is advisable to make a folder `/workspace/remote` and put each project
syncing between local and remote there, for example
`/workspace/remote/fts-rest-flask`. Ensure you don't install anything under the
`/root` directory as other system users won't have access, which can cause
hard to track down bugs.

Create a new toolchain and add your SSH config to the remote machine. Make sure
the development tools are installed on that machine and that they're picked up.

![toolchains](toolchains.png "Build,Execution,Deployment > Toolchains window with settings")

This _should_ automatically make a new deployment type.

![deployment-connection](deployment-connection.png "Build,Execution,Deployment > Deployment window with settings")

Link your local copy of the code and where you want it on the deployed machine.

![deployment-mappings](deployment-mappings.png "Build,Execution,Deployment > Deployment window with settings")

Exclude paths that are large, change often, and aren't code e.g.

![deployment-paths](deployment-paths.png "Build,Execution,Deployment > Deployment window with settings")

Create a CMake profile that uses this toolchain.

![cmake](cmake.png "Build,Execution,Deployment > CMake window with settings")

Create a new run configuration that targets all build targets, and runs
`fts_server -r -n` at the end. To link everything together, it's simplest to
build, then make install, then copy the backup config.

![run-config](run-config.png "Run/Debug Configurations window with settings")

## Accessing the VM from outside the CERN network

By default, OpenStack VMs are only accessible from within the CERN network.
Although you may request opening the SSH port for external use, there's
practically zero chance this will fly with the security team.

In order to have the remote development work also from outside the CERN network,
you can use `lxtunnel.cern.ch` + SSH port forwarding of port 22. How this works:
- Create a port-forwarding tunnel via `lxtunnel.cern.ch`
- Instruct CLion to connect to `localhost:<port>`
- Due to the port forwarding, all packets sent to `localhost:<port>`
  will be forwarded to `your-fts-vm.cern.ch:22` via `lxtunnel.cern.ch`


```bash
$ ssh -o PreferredAuthentications=gssapi-with-mic -f -N USERNAME@lxtunnel.cern.ch -L 1083:<your-fts-vm>.cern.ch:22

# -o PreferredAuthentications=gssapi-with-mic -- use Kerberos authentication to connect to lxtunnel
# -L 1083:<your-fts-vm>.cern.ch:22 -- the port forwarding tunnel between localhost:1083 --> <your-fts-vm>.cern.ch:22 via lxtunnel
# -f -- instructs SSH to run in the background
# -N -- disables execution of commands on the remote machine (recommended for port forwarding)
```
