# Documentation for newcomers

This guide is intended to take you from a bare VM, to a working server using the
existing RPM published packages, to building from source in order to develop the
existing code. At each stage we should be able to run integration tests to
check everything is functioning as expected.

It is written by a Summer Student at CERN seeing the project for the first time.

## Theory

This section aims to give an overall view of the project, what each component
does individually and how they interact in order to form the whole FTS ecosystem.

### Client & Server for submitting transfers

The REST Server (written in Python3 using Flask) takes in submitted jobs and places
them on the database, queueing them.

The REST Client (also written in Python3) is the command line utility which
sends data to the server. The commands start with `fts-rest-*`.

> **Note**: There is a C++ alternative inside the fts3 repository,
but it is deprecated and lacking in features. The commands start with `fts-*`.

In the rest of this guide we will only use the Python REST implementation.

Repository: https://gitlab.cern.ch/fts/fts-rest-flask

### Transfer Server

Polls the database for work to do, transferring files via the _swiss army
knife_ that is [Gfal2](https://dmc-docs.web.cern.ch/dmc-docs/gfal2/gfal2.html).
Submissions are in the form of "Please transfer file X from Source to
Destination". Moving the file is done preferably via a 'Third party copy',
where the data goes directly from the source to the destination instead of streaming
via the FTS server. However, this is not always possible, in which case the FTS server
can fallback to streaming the data.

Another important concept is that of a link, which means a source/destination pair,
with transfers going in one direction. While scheduling transfers, the FTS server
tries to maximise throughput on the link. It does this by optimising transfer parameters
as well as playing with the number of simultaneous transfers on that link.

Repository: https://gitlab.cern.ch/fts/fts3

### Web Monitoring component

A web interface to monitor transfers on a particular server.
Example: https://fts3-devel.cern.ch:8449/fts3/ftsmon/#/

Repository: https://gitlab.cern.ch/fts/fts-monitoring

### Integration Tests

A set of Python files implementing integration tests on the whole system with
pytest. These are run regularly on production servers to ensure service
health, but also are used to validate pushes to the git repositories.

Repository: https://gitlab.cern.ch/fts/fts-tests

### Documentation for FTS ecosystem

When built, produces this website.

Repository: https://gitlab.cern.ch/fts/documentation

## Acting on existing packages

Here we will set up a working FTS Server, FTS-REST Server, database, and client
from prebuild packages. You will need to provision a few resources:
- A (virtual) machine running CentOS7
- Database running MySQL8

#### For CERN account holders 

You can get a CentOS7 VM via [CERN OpenStack](https://openstack.cern.ch) 
and a MySQL8 database via the [DBoD Service](https://dbod.web.cern.ch).

To access these resources you may need to jump through a few e-group hoops,
as well as subscribing to "E-groups", "LXPLUS and Linux", "Cloud Infrastructure"
and "DB on Demand" in the [CERN Resources portal](https://resources.web.cern.ch/resources/) > List Services.

When requesting the database, set the following:
```
Name: fts_yourname
Category: test
Admin e-group: fts-devel
Project: FTS
Database type: MySQL8
```
For the virtual machine, create an OpenStack compute node under your personal project
using the Alma9 image and set up some form of authentication (e.g.: ssh keys).

Now we can ssh into the server and start working!

### Get a X509 certificate and private key

There is (almost) no way to play with Gfal2 and/or FTS without an
[X509](https://en.wikipedia.org/wiki/X.509) certificate. And possible, but hard,
if you don't belong to a
[VO](<https://en.wikipedia.org/wiki/Virtual_organization_(grid_computing)>).

You can skip this section if you know what I am talking about, and you already
have your certificate.

The procedure to do this depends on your home institute.

#### For CERN account holders

Luckily, this step is straight forward. Go to
[ca.cern.ch](https://ca.cern.ch/ca/), sign in, click on "New Grid User
Certificate" and follow the instructions. You will get a
[p12](https://en.wikipedia.org/wiki/PKCS_12) archive which contains both the
certificate and the private key.

Download it, and extract both to a PEM format:

```bash
# Extract certificate
$ openssl pkcs12 -in "myCertificate.p12" -nokeys -out "usercert.pem"
# Extract private key
$ openssl pkcs12 -in "myCertificate.p12" -nocerts -out "userkey.pem"
```

> **Note**: Always protect your private key with a strong password. If someone
takes control of it, they will be able to impersonate you.

Store both PEM files wherever you see fit. The default location for most tools
is `$HOME/.globus/usercert.pem` and `$HOME/.globus/userkey.pem`, but you can
store them anywhere and set the environment variables `X509_USER_CERT` and
`X509_USER_KEY` to their full path instead.

Example for non-default location:
```bash
export X509_USER_CERT=$HOME/Certs/usercert.pem
export X509_USER_KEY=$HOME/Certs/userkey.pem
```

### Join a VO

If you are a developer, join the CERN `dteam` VO. To do this, install the p12 you got
before into your browser as a user certificate, and go to
[https://dteam-auth.cern.ch/start-registration](https://dteam-auth.cern.ch/start-registration).
Your browser will likely not trust the certificate of this host. You can either
accept the warning, or install the
[CERN Root CA](https://ca.cern.ch/cafiles/) beforehand

Follow the instructions and wait to be accepted into the VO. It is recommended
to assign your request to somebody from your home institute.

### Create your first proxy

#### Proxy generation configuration

Once you have obtained your **Grid User Certificate** (PKCS#12 archive) and
converted it into host/key certificates, make sure the correct permissions are
set:

```bash
$ chmod 644 $HOME/.globus/usercert.pem
$ chmod 400 $HOME/.globus/userkey.pem
```

After having set up properly your certificates, you will be ready to install the
packages required for the creation of a **VOMS Client**:

```bash
$ sudo dnf install voms-clients-java
```

Specifically, for the **ca-policy-egi-core** meta-package, you will have to
create the "EGI-trustanchors" repository file, having the below content.
(full details are described in the [EGI IGTF Release Installation Guide](https://docs.egi.eu/providers/operations-manuals/howto01_using_igtf_ca_distribution))

```
# place below in /etc/yum.repos.d/EGI-trustanchors.repo

[EGI-trustanchors]
name=EGI-trustanchors
baseurl=https://repository.egi.eu/sw/production/cas/1/current/
gpgkey=https://repository.egi.eu/sw/production/cas/1/GPG-KEY-EUGridPMA-RPM-3
gpgcheck=1
enabled=1
```

After that, you will be able to install the meta-package **ca-policy-egi-core**:

```bash
$ sudo dnf install ca-policy-egi-core
```

Last but not least, you will also need a few more files for both **vomses** and
**vomsdir**, which are already available via `lxplus` and can be obtained via
SFTP:

```
$ sftp <login>@lxplus.cern.ch

sftp> get /etc/vomses/dteam-voms-dteam-auth.cern.ch
Fetching /etc/vomses/dteam-voms-dteam-auth.cern.ch to dteam-voms-dteam-auth.cern.ch
dteam-voms-dteam-auth.cern.ch                               100%  105   196.3KB/s   00:00

sftp> get /etc/grid-security/vomsdir/dteam/voms-dteam-auth.cern.ch.lsc
Fetching /etc/grid-security/vomsdir/dteam/voms-dteam-auth.cern.ch.lsc to voms-dteam-auth.cern.ch.lsc
voms-dteam-auth.cern.ch.lsc                                 100%  102   260.6KB/s   00:00

sftp> quit
```

Move said files in the following locations:

```bash
$ sudo mkdir /etc/vomses
$ sudo mv dteam-voms-dteam-auth.cern.ch /etc/vomses/
$ sudo mkdir -p /etc/grid-security/vomsdir/dteam
$ sudo mv voms-dteam-auth.cern.ch.lsc /etc/grid-security/vomsdir/dteam/
```

Congratulations, you will be now able to create your **VOMS Proxy**:

```bash
$ voms-proxy-init --voms dteam
```

If everything has worked properly, you will get an output similar to the one
below:

```
Contacting voms-dteam-auth.cern.ch:443 [/DC=ch/DC=cern/OU=computers/CN=dteam-auth.cern.ch] "dteam"...
Remote VOMS server contacted succesfully.


Created proxy in /tmp/x509up_u$(id -u).

Your proxy is valid until Mon Jul 26 13:10:42 CEST 2024
```

#### Considerations on proxy certificates

`voms-proxy-init` created a temporary certificate/key pair (with a limited
lifetime), signed with your own private key and with an extension signed by
the CERN, certifying your belonging to the `dteam` VO.

These proxies are useful as you don't need to move around your private key. More
so, if they are exposed, the harm is reduced as they are limited in scope and
lifetime.

The default location for the proxy is `/tmp/x509up_u$(id -u)`. You can override it
with the environment `X509_USER_PROXY`.

You may see an error that looks like this after while:

```bash
$ voms-proxy-init --voms dteam
Enter GRID pass phrase for this identity:
Certificate validation error: Signature of a CRL corresponding to this certificates CA is invalid
User credential is not valid!
```

This is not because you have used the wrong passphrase for your key but instead
the Certificate Revocation List (CRL) on your machine needs to be updated. 
CRLs allow you to check whether a certificate, although valid, has been revoked 
in the meantime. You can update CRLs with the following command:

```bash
$ sudo dnf install fetch-crl
$ sudo fetch-crl -v
```

You can also have this command periodically run to keep the CRLs updated with:

```bash
$ sudo systemctl start fetch-crl-cron
```

Now you are ready to start playing with Gfal2 and FTS3!

### Play with gfal2-util

[gfal2-util](http://dmc-docs.web.cern.ch/dmc-docs/gfal2-util.html) are a set of
command line tools that allow you to interact with Grid Storages in a very similar
way as with standard UNIX tools.

Let's pick `https://eospublic.cern.ch/eos/opstest/dteam/` as the storage endpoint for our tests.

```bash
$ sudo dnf install python3-gfal2-util

# Note: Use your own user name
$ gfal-mkdir "https://eospublic.cern.ch/eos/opstest/dteam/${USER}"
$ gfal-ls "https://eospublic.cern.ch/eos/opstest/dteam/${USER}"
$ gfal-copy "file:///etc/hosts" "https://eospublic.cern.ch/eos/opstest/dteam/${USER}/hosts"
$ gfal-cat "https://eospublic.cern.ch/eos/opstest/dteam/${USER}/hosts"
```

By now, you should already have a file in remote. It should be easy to pick it up from
there. Check the tools man pages if you want to check what they do and the
different options.

### Submit your first FTS3 transfer with the REST client

If you have succeeded until here, good! You are about to submit your first FTS3
transfer, as you already have a file to transfer somewhere (unless you `gfal-rm`
it, in which case re-upload something!).

We need another endpoint to act as the destination. Let's use `eospps.cern.ch` for that.

Submitting an FTS transfer is as easy as follows:

```bash
# You may need the FTS3 production repo first
# Look at 'Server from existing packages locally' section
$ sudo dnf install fts-rest-client
```

```bash
$ fts-rest-transfer-submit -s "https://fts3-devel.cern.ch:8446" "https://eospublic.cern.ch/eos/opstest/dteam/${USER}/hosts" "https://eospps.cern.ch/opstest/dteam/${USER}/hosts"
```

`fts-rest-transfer-submit` is the tool to submit a transfer. `-s` is the option
to tell which FTS endpoint to use. We are using the development instance in this
case. Finally, on this basic usage, we specify the source (`eospublic`) and the
destination (`eospps`).

The output will be a job id, which can be used to track, or cancel, the
submitted job. Let's see what happened (we use the `-v` flag to show more
complete output):

```bash
$ fts-rest-transfer-status -v -s "https://fts3-devel.cern.ch:8446" "7666a96c-00f0-11e8-b523-02163e00f916"
```

```
FINISHED

  Source:      https://eospublic.cern.ch/eos/opstest/dteam/<user>/hosts
  Destination: https://eospps.cern.ch/eos/opstest/dteam/<user>/hosts
  State:       FINISHED
  Reason:
  Duration:    4
  Staging:     0
  Retries:     0
```

Cool! The file is now on the destination. Let's check:

```bash
$ gfal-cat "https://eospps.cern.ch/eos/opstest/dteam/${USER}/hosts"
```

Have a look at the [CLI documentation](../cli/cli.md) to see how to submit more
complex transfers: multiple transfers, checksum validation, etc.

### Web Monitoring

You will likely find useful in the future the
[Web Monitoring](https://fts3-devel.cern.ch:8449/fts3/ftsmon/#/) to have a look
at your tests and see how the server is performing.

For instance, for the previous transfer, you can see it in your browser. Copy
the job id into the search box in the top right corner, and hit enter.

![Web Monitoring Job View](webmon-job-view.png "Job View")

####  Obtain a root certificate on a CERN machine

You can get the certificate directly on a CERN machine using the following commands:

```bash
$ dnf -y install cern-get-certificate
$ cern-get-certificate --autoenroll --grid
$ cern-get-certificate --status --grid
```
You can now copy the host certificates to location as required (explained in next section).

```bash
$ cp "/etc/pki/tls/private/${HOSTNAME}.grid.key" "/etc/grid-security/hostkey.pem"
$ cp "/etc/pki/tls/certs/${HOSTNAME}.grid.crt" "/etc/grid-security/hostcert.pem"
```

> **Note**: You may need to change owner and mode bits (explained in next section).

```bash
$ chmod 644 /etc/grid-security/hostcert.pem
$ chmod 400 /etc/grid-security/hostkey.pem
```

You can check your certificates in a human readable form using following command:

```bash
$ openssl x509 -in /etc/grid-security/hostcert.pem -noout -text
```

#### Open the firewall on machines

You can open the firewall on machines using firewalld.
For REST:

```bash
$ firewall-cmd --zone=public --add-port=8446/tcp
$ firewall-cmd --permanent --zone=public --add-port=8446/tcp
```

For Web Monitoring:

```bash
$ firewall-cmd --zone=public --add-port=8449/tcp
$ firewall-cmd --permanent --zone=public --add-port=8449/tcp
```

## Server from existing packages locally

Now that we have tested on a development server, we're ready to start
the server on our own machine and test against it.

To set up the entire system, we need the Server, REST Server, database and
Web Monitoring component.  Much of this can be found in the [Quick Start](../install/quick.md) guide,
repeated here.

> **Note**: this section onwards assumes you are running as root
> or have all the necessary permissions

First we need to install the FTS and DMC production repositories:

```bash
$ curl https://fts-repo.web.cern.ch/fts-repo/fts3-el9.repo -o /etc/yum.repos.d/fts3-el9.repo
$ curl https://fts-repo.web.cern.ch/fts-repo/fts3-depend.repo -o /etc/yum.repos.d/fts3-depend-el9.repo
$ curl https://dmc-repo.web.cern.ch/dmc-repo/dmc-el9.repo -o /etc/yum.repos.d/dmc-el9.repo
$ yum install centos-release-scl-rh
```

```bash
$ dnf install fts-server fts-monitoring fts-rest-server fts-rest-client
$ dnf install fts-server-selinux fts-monitoring-selinux fts-rest-server-selinux
$ dnf install fts-mysql
```

We also need `ca-policy-egi-core` and `fetch-crl` but if you have followed this
guide through, you will already have these installed. If not refer back up this
guide.

We will need a host cert/key pair installed into
`/etc/grid-security/hostcert.pem` and `/etc/grid-security/hostkey.pem` owned and
readable only by root

```bash
# Get host certificate from https://ca.cern.ch/ca/ (requires CERN account)
$ openssl pkcs12 -in "hostCertificate.p12" -nokeys -out "hostcert.pem"
$ openssl pkcs12 -in "hostCertificate.p12" -nocerts -out "hostkey.pem"

$ mv hostcert.pem /etc/grid-security/hostcert.pem
$ mv hostkey.pem /etc/grid-security/hostkey.pem

$ chmod 644 /etc/grid-security/hostcert.pem
$ chmod 400 /etc/grid-security/hostkey.pem
```

Ensure ports `8446` for REST and `8449` for Web Monitoring are open.

We can now set up the database.

#### Oracle MySQL Database setup

You can install and run a local Oracle MySQL database using the following commands:

```bash
$ dnf install -y mysql mysql-server
$ systemctl enable --now mysqld
```

You can login to the database with root user by using the default password
for the first time. You need to hit this command and then enter the default password.

```bash
$ mysql -u root -p
```

Now you need to set up the root password:

```sql
> ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'Your Password';
```

You can use following command to create a user in Oracle MySQL database so that native MySQL authentication is used.
By Default, Oracle uses its own special Authentication method whose plugin is not installed by default during Oracle
MySQL installation process. So, to solve this problem hit this command in mysql.

```sql
---Create the database
> CREATE DATABASE fts3db;
--- Check user already exists or not
> SELECT User FROM mysql.user;
---Create user
> CREATE USER 'fts3user'@'%' IDENTIFIED BY 'Your Password';
---Alter the authentication method for the user
> ALTER USER 'fts3user'@'%' IDENTIFIED WITH mysql_native_password BY 'Your Password';
--- GRant the privilages to the user
> GRANT ALL PRIVILEGES ON fts3db.* TO 'fts3user'@'%'
```

```bash
$ mysql -h <db-hostname> -P <database-port> -u fts3user --password='Your Password' fts3db
```

```sql
---Apply FTS SQL schema
> source /usr/share/fts-mysql/fts-schema-8.0.0.sql
```

As an alternative to setting up a local database, if you have a CERN account, you can request a
MySQL8 database via the [DBoD Service](https://dbod.web.cern.ch).

Set the configuration in the files `/etc/fts3/fts3config` and
`/etc/fts3/fts3restconfig` to have the appropriate connection strings.

Now we can start the services with:

```bash
# Core FTS3
$ systemctl start fts-server
$ systemctl start fts-qos

# REST API and Web Monitoring
# May need to `setenforce 0` in development to see host key
$ systemctl start httpd
```

If everything went well, the status of the services should be "Active":

```bash
$ systemctl status fts-server
$ systemctl status fts-qos
$ systemctl status httpd
```

Should be active and happy! Otherwise, you've got some debugging to do!

The usual place where debugging starts is in the log files:
- FTS Server: `/var/log/fts3/fts3server.log`
- FTS QoS: `/var/log/fts3/fts3qos.log`
- FTS-REST: `/var/log/fts3rest/fts3rest.log`
- Apache logs: `/var/log/httpd/error_log` and `/var/log/httpd/fts3rest_error_log`

> **Note**: if problems persist, investigate also SELinux (the usual culprit)

You can now submit transfer jobs to your own server with
`fts-rest-transfer-submit -s <endpoint> <source> <dest>`. You can drop the `-s` flag
if you're running the transfer command from the same host as the `fts-rest-flask`
server.

By this point, you should have your own FTS development instance,
installed and configured from RPMs, up and running.

## Building / Running from source code

It is advisable to make a dedicated folder (e.g.: `/workspace`)
where to keep all projects (e.g.: `/workspace/fts-rest-flask`).
Do not use the `/root` directory as other system users won't have access,
which can cause hard to track bugs.

The installation instructions are often in the Readme of each project.
Ignore any references to vagrant, as these are outdated.

### FTS-REST-Flask (server)

The FTS-REST-Flask project is written in Python3, uses the Flask framework
and relies on Apache to handle the HTTP request and load the Flask module.
The goal of the development installation is to have Apache execute the Python code
straight from code repository.

Installation is currently laid out well in the `README.md` of the project,
but repeating here...

```bash
# Install python3-mod_wsgi
$ dnf config-manager --set-enabled crb
$ dnf install -y python3-mod_wsgi

$ git clone https://gitlab.cern.ch/fts/fts-rest-flask.git
$ cd fts-rest-flask/

# Add git pre-commit formatting for ease of use
$ pushd .gitlab-ci/
$ ./precommit_install.sh
$ popd

# Create virtual environment and install packages
$ virtualenv venv # might need: 'pip3 install virtualenv'
$ source venv/bin/activate

(venv) $ pip install --upgrade pip
(venv) $ pip install pip-tools
(venv) $ ./pipcompile.sh --sync
(venv) $ deactivate

# Install development httpd
# This tells the httpd process to:
# - set the Python home to the "venv" environment
# - place the repository on the Python path (code will be executed directly from the repository)
# - load the WSGI entrypoint from the repository 
$ cd src/fts3rest/
$ ./install-httpd-dev.sh

# If you have had the fts-rest-server installed from RPMs in the past, you may have a file
# /etc/httpd/conf.d/fts3rest.conf already installed, you will need to move this to a backup,
# otherwise the two WSGI processes will clash. You will be able to see the fts3rest_dev.conf
# the script just created in this folder too.
$ ls /etc/httpd/conf.d
$ mv /etc/httpd/conf.d/fts3rest.conf /etc/httpd/conf.d/fts3rest.conf.save
$ cat /etc/httpd/conf.d/fts3rest_dev.conf

# Set config file with relevant params
$ vim /etc/fts3/fts3restconfig

$ systemctl restart httpd
# ...Develop
$ systemctl restart httpd
# ...Develop
$ systemctl restart httpd
# etc...
```

### FTS Server

The FTS3 Server is a C++ daemon, using CMake for compilation and installation.
The software must be compiled and installed in order to deploy the latest changes.

#### Notes on compiler version

Building the FTS3 Server requires a modern compiler with support for C++17 features.
On older platforms, such as CC7, the default compiler is too old. To be able to compile,
one must use a modern development toolset.

Example for CC7:
```bash
$ yum install centos-release-scl
$ yum install devtoolset-8

# You can put the below line in your ~/.bash_profile to load it everytime
$ source scl_source enable devtoolset-8
```

#### Building the FTS Server

```bash
$ git clone https://gitlab.cern.ch/fts/fts3.git
$ cd fts3/

# Install dependencies for building FTS3 binaries
$ dnf builddep packaging/rpm/fts.spec

$ mkdir cmake-build/
$ cd cmake-build/

# Compile and install the project using CMake
$ cmake3 ../ -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX="" -DALLBUILD=ON -Wno-dev
$ make -j 4
$ make install

# Note: the CMake installation will wipe your "/etc/fts3/fts3config"
# It's recommended to have a backup and after each install simply do
$ \cp -fv /etc/fts3/fts3config.save /etc/fts3/fts3config

# Run the Server and QoS daemon
$ /usr/sbin/fts_server -r -n  # -r to skip some initial waits, -n to stay on the foreground
$ /usr/sbin/fts_qos -r -n
# Or run via systemd
$ systemctl restart fts-server fts-qos
```

### Running (Integration) Tests

When you have your own development instance, and you start changing things, not
only will you need to be able to submit simple transfers, but you will also need
to run the existing tests to make sure there are no regressions.

Besides, you will also need to add your own tests when fixing bugs or adding
features. This comes (or should come) before any commit, release, or anything.

#### Download the dependencies:

```bash
$ dnf install -y git python3 python3-devel openssl-devel swig gcc gcc-c++ python3-m2crypto ca-policy-lcg voms-clients-java
```

#### Clone the "tests" repository:

```bash
$ git clone "https://gitlab.cern.ch/fts/fts-tests.git"
$ cd "fts-tests"
```

#### Set up the Python environment

We can now use `./run-helper.sh` to prepare a Python virtual environment, set
everything up and run the pytest tests for us. We will, however, need to set some
environment variables first.

```bash
export FTS3_ENABLE_MSG=False # Stops sending info to broker
export BROKER="dashb-mb.cern.ch" # If above is true, send broker messages here
export FTS3_HOST="fts3-devel.cern.ch" # Host to test on
export SHORT_RUN=1 # Skip some tests
export NO_SIGNALS=1 # Skip additional tests for error signals
```

You can see there is a `run-helper.sh` that prepares a virtualenv and run all
tests for you. Remember you must have a valid proxy before.

If we want to run one manually, we can craft a virtual environment ourselves. We
still need to have the above environment variables. This code is from
the [gitlab-CI](https://gitlab.cern.ch/fts/fts-tests/-/blob/develop/.gitlab-ci.yml) pipeline,
which automatically runs these tests. These pipelines are a good place to look
to find how things are done if documentation is lacklustre.

```bash
$ python3 -m venv "/tmp/my-fts-tests"
$ source "/tmp/my-fts-tests/bin/activate"
$ pip install --upgrade pip
$ pip install "git+https://gitlab.cern.ch/fts/fts-rest-flask.git@develop"
$ pip install "pytest"
$ pip install "git+https://github.com/jasonrbriggs/stomp.py@pre-python2-removal"
```

If everything goes well, this should work:

```bash
# Remember! You must have created your proxy before
$ fts-rest-whoami -s "https://fts3-devel.cern.ch:8446"
```
#### Run the simplest test

```bash
$ ./fts_simple.py
```

The output should look something like

```
INFO     Submitted a new job with id aa94f800-0100-11e8-a0d4-02163e0170e3
INFO     Waiting for aa94f800-0100-11e8-a0d4-02163e0170e3
INFO     Poll aa94f800-0100-11e8-a0d4-02163e0170e3
INFO     Submitted a new job with id adbd2db8-0100-11e8-a65a-02163e00a077
INFO     Waiting for adbd2db8-0100-11e8-a65a-02163e00a077
INFO     Poll adbd2db8-0100-11e8-a65a-02163e00a077
.
----------------------------------------------------------------------
Ran 2 tests in 10.569s

OK
```

If everything is OK, congratulations! You now know how to verify if your
instance is properly set up, and write and run tests, which are the foundations
to start closing tickets, or cleaning up code.

You should spend some time reading the tests to understand what they are doing.
Anytime you see a use case, or permutation missing, do not hesitate to add it.

These tests are run on git pushes in the gitlab CI pipeline automatically, so you can
always look at the result there, rather than running it yourself if you don't mind
the wait.


### FTS Web Monitoring

The Web Monitoring component is composed of Python Django code and static files.
The installation setup is much simpler, simply copying the files into the right place,
mimicking the steps done by the RPM installation.

```bash
$ git clone https://gitlab.cern.ch/fts/fts-monitoring.git
$ cd fts-monitoring/

# Install dependencies for the Web Monitoring component
$ dnf builddep packaging/rpm/fts-monitoring.spec

# Mimic the RPM installation steps
$ \cp -rf src/* /usr/share/fts3web/
$ cp -v conf/fts3web.ini /etc/fts3web/
$ cp -v conf/httpd.conf.d/ftsmon.conf /etc/httpd/conf.d/
$ cp -v conf/fts3firewalld/ftsmon.xml /usr/lib/firewalld/services/

# Configure the relevant parameters
$ vim /etc/fts3web/fts3web.ini

# Ensures empty ssl.conf
$ if [ -f /etc/httpd/conf.d/ssl.conf ] ; then
  echo -n > /etc/httpd/conf.d/ssl.conf
fi
```

### Gfal2 (bonus)

Gfal2 is a low-level C/C++ client library to interact with Grid Storages. Using a plugin
architecture, it supports multiple protocols. The Gfal2 build system is managed via CMake.
Given the close relation between FTS and Gfal2, it's useful to have the installation detailed here.

```bash
$ git clone https://gitlab.cern.ch/dmc/gfal2.git
$ cd gfal2/

# Install dependencies for building Gfal2 binaries
$ dnf builddep packaging/rpm/gfal2.spec

$ mkdir cmake-build/
$ cd cmake-build/

# Compile and install the project using CMake
$ cmake3 ../ -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX="/usr" -DSYSCONF_INSTALL_DIR="/etc" -DPLUGIN_MOCK=True -DUNIT_TESTS=True -DFUNCTIONAL_TESTS=True -Wno-dev
$ make -j 4
$ make install

# Note: the CMake installation will wipe your "/etc/gfal2.d/*.conf" files
# It's recommended to have a backup and after each install simply do
$ \cp -fv /etc/gfal2.d.saved/* /etc/gfal2.d/
```

At this point, not only do you have your own development FTS instance,
but you can also modify the source code and have the changes deployed.

