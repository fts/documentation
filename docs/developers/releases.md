Versioning
==========
Until the version 3.2.33, every release increased the patch version, while the minor
was seldom touched: only twice, obviously.

Starting with 3.3.0, the same versioning scheme as used by gfal2 was adopted:

  * **Major version** remains untouched, since the API from the user's point of view *must*
remain backwards compatible.
  * **Minor version** increases for releases with new functionalities, big change sets, or behavioral changes (that aren't a fix).
  * **Patch version** increases for releases with only bug fixes.
  * **Release number** belongs to the packaging, *not* the software. Therefore, if the fix
applies only to the packaging, or is a minor configuration change shipped within the rpm,
a change of only release number would be better.

Release cycle
=============
  1. New features are committed into develop. Big new features are advised to go into
     their own branch and later merged into develop.
      * Keep documentation up to date
  2. When develop is ready for being a release candidate, merge develop into master.
    1. Remember to eventually increase the minor or patch version of develop
    2. Update release notes before the merge
    3. Tag the new version
  3. When master is stable and ready for release, follow the [cross-check list](checklist.md) first, and then
    1. Make sure the documentation is up-to-date
    2. Synchronize production repo with the pre-prod one, so the same artifacts
       are deployed in production

Hotfixes that need to go into production quickly can be committed directly into master.
Remember merging these fixes into develop, update release notes and [tag](tags.md).

If the hotfix is just a configuration change (can happen), then:

  1. Increase release number in the spec file

No need to update release notes or retag, but do add a new entry to the spec
changelog.

**Important:** If there is a critical bug, a patch release with *only* its fix *should* be done.
If there are more than one critical, their fixes *may* be released together, but do *not* release
non-critical bug fixes with critical bug fixes together. Critical fixes *must* go to production as fast
as possible, so the release should reduce the side effects.

Build system
============
Develop and master branches are linked with [Jenkins](https://jenkins-fts-dmc.web.cern.ch/). 
Commits to these branches will trigger a rebuild and a repo update.

**For develop**  
Each build involves an automatic increase of the release number (corresponding to the timestamp), 
so the development machines pick the latest and greatest change.

**For pre-production** (also known as release candidates)  
The release number is *not* automatically changed. Commits will trigger rebuilds as well, 
but if there is an existing RPM with the same name and version, the yum repo will *not* be updated 
and the build will fail. Therefore, changes to master should be accompanied by a version increase.

**For production** (no build from sources available)  
The same artifacts from pre-production should be used, so we are sure the exact 
same binaries are in place. When a release to production is done, use the [Sync Repos job][1] in Jenkins. 
This job will copy the RPM from pre-production to production and update the `repodata`.

By default, the pattern is `*.rpm`, so everything new is copied. 
If you want to limit the scope to a subcomponent (fts-rest, gfal2,...), 
change the pattern before the execution (i.e `gfal2*.rpm`)


Release notes
=============
Up until `v3.9.0`, release notes were written in `doc/vmajor.minor.md` files (i.e.: `doc/v3.8.md`)
in their respective projects. These files would contain release notes for all the
subsequent patches, as well. Release notes should always be taken from JIRA. 
Special mentions (such as external contributors) should also be included.

From `v3.9.0`, release notes are now posted on the [FTS Website][2], under the [Releases][3] section.
The website is managed by the [fts-jekyll][4] repository. Uploading a new version 
is handled by the pipeline, which recompiles the website after every commit.

To post new release notes, create a new entry under `fts-jekyll/_posts` with the correct file. 
As jekyll is sensitive to the file name, make sure to follow the naming convention:
```
_posts/<yyyy>-<mm>-<dd>-(FTS|REST)_<major>_<minor>_<patch>.markdown
```

Release notes content should come from [JIRA Releases][5]: JIRA > Project > Release Notes *(HTML format)*.

[1]: https://jenkins-fts-dmc.web.cern.ch/view/Prod/job/Sync%20Repos/
[2]: https://fts.web.cern.ch/fts/
[3]: https://fts.web.cern.ch/fts/releases.html
[4]: https://gitlab.cern.ch/fts/fts-jekyll
[5]: https://its.cern.ch/jira/projects/FTS
