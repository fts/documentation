Common Guidelines
=================

## Logging Levels

* **TRACE**: Very low level logging, like object creation, destruction, function calls, etc.

* **DEBUG**: Lines that would not be interesting to monitor, but are useful to develop and debug. For instance, "Retrieved X transfers from database", "Polling again in X seconds", etc.

* **INFO**: Informational messages about the normal function of FTS3. "Running this transfer", "finish that bringonline with that error" (storage or user errors would be INFO!)

* **WARNING**: Something that is not an ERROR, but worth checking (i.e. the node has gone out of memory so it enters auto-drain)

* **ERROR**: An error that affects FTS3 (could not connect to the database, could not open log), but the service can keep running. Probably transient.

* **CRITICAL**: Errors that either imply a tear down of the service (`SIGSEGV`, `SIGABRT`), unexpected exceptions, assertion errors (when we have those  ) Service stability may be compromised, and a shutdown would probably be a good idea in those case that don't trigger that.
