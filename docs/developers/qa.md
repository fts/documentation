
  Quality Assurance
======================


##Test Inventory

Three types of tests are implemented for the quality assurance in FTS3:

* Unit tests: Used for continuous integration and they run in every build. 
* Functional tests: Used for continuous integration and they run every night. These tests validate what they can from the full stack using the `mock` protocol. 
* Stress tests. These tests will be used for pre-production testing in the pilot in order to increment the load of transfers in the FTS3. It is recommended
to run them on the devel cluster before a merge into master. 
* Fault tolerance tests: The `mock` protocol is implemented by a plugin in gfal2 that allows to the client to decide which behaviour it wants, which is useful for testing. In long-term we could inject errors in FTS (i.e. forcing disconnections, latency, kill nodes, etc.).

Note: gfal2 tests are also important for FTS3, as they validate the interaction with real-world storages they run every night.

We use Sonarqube (http://baluarte.cern.ch:8080/) by considering unit tests and functional tests in order to check the code measurements for
FTS-REST, FTS3 and GFAL2:

* Lines of code/functions/files for each programming language.
* Technical debt pyramid: The technical debt is based on SQALE (Software Quality Assurance based on Lifecycle Expectations), which is based on rules and issues (Duplicated blocks, failed unit tests, insufficient branch coverage by unit tests, insufficient comment density, insufficient line coverage by unit tests and skipped unit tests). Then the technical debt provides the measurement of 8 quality characteristics (reusability, portability, maintainability, security, efficiency, changeability, reliability and testability).
* SQALE rating and Technical Debt Ratio: The technical debt ratio gives the ratio between the actual technical debt and the effort it would take to rewrite the whole source code from scratch. The latter is estimated based on either the number of lines of code or the overall complexity, which means that the value for this metric depends on the size of the project. And given the value of this ratio, the project gets a SQALE rating going from A (best grade) to E (worst grade).
* Hotspots by lines of code
* Complexity
* Unit tests coverage
* Duplications

Current status of the tests and coverage (01/02/2017):
* FTS-REST, 81.4% coverage
* FTS, 61.3% coverage
* GFAL2, 76.8% coverage
	

## Continuous Integration

We use [Jenkins](https://jenkins-fts-dmc.web.cern.ch/) for the continuous builds with the unit tests and the two configurations: SLC6 and CentOS7.
The instance is shared with the Data Management Clients.

For FTS3, there are basically two relevant families: Devel and RC.
* Devel builds from 'develop' branches. The generated RPMS have their release number overridden with r+timestamp (i.e. r201510141201), so every build has a higher version number that the previous one.
* RC builds from 'master' branches. The generated RPMS keep their release number.

The artifacts are copied to the respective repositories:
* http://fts-repo.web.cern.ch/fts-repo/

Note: Please, try to keep the tests blue and happy within your possibilities. For instance,
normally there are always gfal2 tests failing since real-world storages have real-world
problems and are unreliable. As long as the same tests don't remain failing several days
in a row, or the number of failures is high, do not worry too much.


#### RPMS
There are two things worth taking into account regarding RPMS:
  * If an RPM with the same name already exists, the build will fail and no artifacts will
be copied
    * For devel, since the release number if overridden, it doesn't matter
    * For master, this avoids re-building RPMS
  * RPMS with the same version but different release number are deleted to save space

Why do we want to avoid re-building RPMS for master? Because these artifacts are normally
automatically picked by

  * fts3-devel.cern.ch for develop branch
  * fts3-pilot.cern.ch for master branch

At CERN, there may be other clusters out there with auto update on.

If we allow rebuilding, we risk having machines with RPMS with the same version
but actually different artifacts, which makes troubleshooting, support and debugging
harder.


### Infrastructure

The test endpoints that will be used are the following ones:

	Tier	Site	   Available  Type   		Endpoint														  	
	Tier1	IN2P3-CC	2TB       dCache 	srm://ccsrm.in2p3.fr:8443/srm/managerv2?SFN=/pnfs/in2p3.fr/data/dteam/		
	Tier1	FZK-LCG2	10TB      dCache	srm://gridka-dCache.fzk.de:8443/srm/managerv2?SFN=/pnfs/gridka.de/dteam/	
	Tier1	*SARA-MATRIX tapePools dCache	srm://srm.grid.sara.nl:8443/srm/managerv2?SFN=/pnfs/grid.sara.nl/data/dteam	
	Tier1	JINR-T1     2TB       dCache 	srm://srm-cms.jinr-t1.ru:8443/srm/managerv2?SFN=/pnfs/jinr-t1.ru/data/dteam
	Tier1	PIC         1TB       dCache	srm://srm.pic.es:8443/srm/managerv2?SFN=/pnfs/pic.es/data/dteam
	Tier1	Taiwan-LCG2 3TB       DPM		srm://f-dpm001.grid.sinica.edu.tw:8446/srm/managerv2?SFN=/dpm/grid.sinica.edu.tw/home/dteam
	Tier1   CA-MCGILL
		    -CLUMEQ-T2  2TB       StoRM		srm://storm01.clumeq.mcgill.ca:8444/srm/managerv2?SFN=/dteam/
	 		CERN-EOS    1TB       EOS		root://eospps.cern.ch//eos/ppsscratch/
	 		CERN-CEPH   1TB       S3-CEPH	http://ftstests.cs3.cern.ch/

Note: "The attached pool groups have these sizes:generic_readtapepools 299073120 MiB generic_writetapepools   26214400 MiB http://dcmain.grid.sara.nl:2288/webadmin/poolgroups Space on our tape backend is virtually unlimited. They are shared with quite a few other VOs and VO groups, but not the big users like Atlas, LHCb and Alice. So if another VO becomes very active, it's possible that they cause your cached online replicas to be purged. If this setup causes problems for your test, please let me know so that we can think of a solution."

### Data
A collection of Atlas files is selected for these tests from different sizes. According to the Atlas transfers we will have the following percentage of file sizes:

	10% files < 1MB    
	50% 1MB < files < 1GB
	40% >1GB

Currently we have the following ATLAS files that will be distributed by following the percentages aforementioned. Each endpoint will have three directories indicating the file size. 

	27 files < 1MB   
	19 files < 1GB
	7276 files > 1GB

### Cluster

A cluster of 138 VM with 4 cores each one, half SLC6 and another half Centos7, will be deployed as clients/users to actually run the real transfers from the user point of view, they will be used with multiple threads in order to increase the load. The actual hostgroup is called s3csbenchmarks, however it will be changed to fts3suite.
This VMS will be controlled with Foreman and Openstack dashboards. 

### Multi-VOs and Certificates

A multi-vo scenario will be deployed. Two VOs are considered dteam and bitface.
The vo dteam will be used for the real transfers and bitface will be used by the fake transfers used as a surl a mock url.

A certificate robot is created and used by the clients/users to perform the real transfers. The DN is: 
	
	/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=ftssuite/CN=737188/CN=Robot: fts3 testsuite. 

This DN has also permissions to consume from /topic/transfer.fts_monitoring_complete and /topic/transfer.fts_monitoring_start. 

## Development and Monitoring

A gitlab project is created including the modified doppelganger project inside with the master slave implementation, the name  fts3TestSuite (https://gitlab.cern.ch/fts/fts3TestSuite/). 

Two modes could be implemented depending of the number of clients/users/threads that we include in the tests.
* Load (baseline):  run continuously 24/7. 
* Stress (peak): they start when a new release is provided in the pilot with 7 days of duration.

The tests will be monitored with Kibana and the fts dashboard by looking the load in the pilot, however also new statistics can be included to improve the data monitoring. Two proposals are already discussed:

* Detect inconsistencies in DB by doing transfer-pooling (count)
* Detect the delay or stack transfers (transfer time max 3 days) (count)
	
This data could be sent to Kibana in order to have the same monitoring system in a coherent a robust way. We could reuse the work started with Anna in Kibana, collecting the data from the logs (number of transfers per second, ..), include also the data collected by Mary from the broker messaging (error transfers, latency, ..) and the specific data from our tests. We have to check how multiple views work in Kibana, to have performance and issues views.

## Communication and Middleware readiness

Two types or communications can be identified:

* Sites and experiments for performance reasons: A mailing list could be created to include the sites (endpoints) involved and the "fts3-steering (Steering board of FTS 3 development)" in order to let them know when the stressing tests are on. 

* Sites for endpoint upgrades: It will be important to know when the type endpoints are upgraded to different versions to take that into account when running the tests. Maybe a person-link with the middleware readiness group could be interesting in order to check that with the endpoints and also help them with that (TBC).

# Recommendations for everyday quality assurance

The quality assurance recommendations are the following:
* Tests creation: Create unit tests for both fts-rest (python code) and fts (c++ code), however depending on the dependencies with the database, an alternative would be to create or update a functional tests to ensure the coverage of the new piece of code.
* Check per each Jira issue related to your commit the status of itself provided by Jenkins, in case of failure detect the error and fix it. 
* Currently, functional tests are run every night, however there is no correlation with the commits done in the previous day.
 I would encourage to check Jenkins the day after (overall when C++ code is committed) to see that the functional tests passed successfully. A Jira issue has been created to try to correlate the commits with the functional tests [FTS-429].

