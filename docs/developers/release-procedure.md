Release Procedure
=================

This page strives to provide a step-by-step guide on releases a new FTS package.
Before attempting a release, please make sure you have read the [Release Guidelines](releases.md).

1. Make sure all relevant [FTS JIRA][1] issues are `Closed` and the wanted `Fix version` is set
2. Make sure the `packaging/rpm/<name>.spec` file includes the correct package dependency versions
3. Increase the release number:
   - In the spec file
   - *FTS3*: In the main `CMakeLists.txt` file
   - *FTS-REST*: In the `setup.py` and `fts3rest/controllers/api.py` files
   - *FTS-REST-CLIENT*: In the `src/fts3/__init__.py` file
4. If the release requires a database schema change, make sure the expected schema version is correctly
   set in the `validateSchemaVersion()` function in `src/db/mysql/MySqlAPI.cpp`
5. Add author and description entries in the spec file changelog
6. Commit these changes on `develop` branch, preferably as one commit. E.g.: `Prepare release <version>`
7. Make sure the build/pipeline succeeds
8. Change to `master` branch and merge `develop`:
   - `git merge "develop" --message 'Merge branch "develop" for release <version>'`
   - Ideally, there should be no merge conflict. Otherwise, the development process went wrong somewhere
9. Tag the master branch with the new version: `git tag --annotate --message "Release <version>" v<version>`
   - FTS projects respect the following tag format: `v#.##.#`. Example: `v3.11.0`
   - Annotating tags is not mandatory, but encouraged
10. Push the `master` branch and new tag to remote:
    - `git push --atomic origin master <tag>`
11. Once the pipeline succeeds, the release candidate RPMs should already be uploaded to the [RC repository][2]
12. Install the new release on FTS3-Pilot
    - `wassh --cluster ftspilot/live --user root "yum update -y"`
13. If this is a new minor release (normally they also require a database schema change), 
    write the upgrade procedure under the [Upgrade](../install/upgrade.md) section
14. Make sure again the documentation is up-to-date:
    - REST-API changes reflect in the documentation
    - New big features are documented
15. Update the release notes in the [FTS Releases](https://fts.web.cern.ch/fts/releases.html) webpage
    - Mark the JIRA version as `Released` in the [JIRA Releases][3] page
    - Copy the [JIRA Releases][3] notes: JIRA > Releases > *Project* > Release Notes *(HTML format)*
    - The FTS webpage is maintained via the [fts-jekyll][4] repository
      - Create a new entry under the `fts-jekyll/_posts/` directory *(Note: jekyll is very sensitive to the naming convention)*
      - Write the release notes
16. Announce the release!

*Note: for deploying the RPMs to production, please read the [Release Guidelines > Build system](releases.md#build-system) section*
## PyPI Release Procedure

In addition to the RPMs release we also release the FTS3 Clients in [PyPI][5]. This subsection provides a guide on how to publish this package to PyPI.

1. To upload a package to PyPi a Python tool called Twine is used. The first step is to create a virtual environment and install it with pip.
```bash
   $ python3 -m pip install twine
```
2. To distribute a package in PyPI you need to provide a `tar` file with the source code and supporting files. In addition a [python wheel][6] should also be provided. To create these files, run the following command that will create a `dist/` directory with the two files.
```bash
   $ python3 setup.py sdist bdist_wheel
```
3. Check that `tar` file contains all the files expected:
```bash
   $ tar tzf fts3-3.12.0.tar.gz
   fts3-3.12.0/
   fts3-3.12.0/src/
   fts3-3.12.0/src/cli/
   fts3-3.12.0/src/cli/fts-rest-ban
   fts3-3.12.0/src/cli/fts-rest-delegate
   fts3-3.12.0/src/cli/fts-rest-delete-submit
   fts3-3.12.0/src/cli/fts-rest-server-status
   ...
 ```
4. Check that the package description follows the PyPI format:
```bash
   $ twine check dist/*
   Checking dist/fts3-3.12.0.post1.tar.gz: PASSED
   Checking dist/fts3-3.12.0.tar.gz: PASSED
```
5. To make sure everything works as expected, upload the package to [TestPyPI][7]. Twine will ask for a username and password. The FTS project owns an account called `fts-devel`. Use that one to upload the package.
```bash
   twine upload --repository-url https://test.pypi.org/legacy/ dist/*
```
6. Find the [fts3 package in TestPyPI][8]. Check that the version you just uploaded is there and that everything renders correctly. In addition you should also install the package in a virtual environment to make sure the installation works as expected.
```bash
   pip install -i https://test.pypi.org/simple/ fts3
```
7.  If happy with the result the final step is to upload the package to real [PyPI][5]. For that run the following command and authenticate with the `fts-devel` account. 
```bash
   twine upload dist/*
```

### Versioning in PyPI

The version of the clients in PyPI should be aligned with the version of the rpm package. Meaning that the same version should install the same software via rpm or PyPI.

The version in PyPI is determined by the `setup.cfg` file. Make sure to update it when doing a release.

It may happen that before having a final release you want to publish a release candidate version for getting the users to test the package. PyPI allows to publish release candidate versions by including a pre-release segment in the version identifier. This pre-release segment consists of an alphabetical identifier for the pre-release, along with a non-negative integer value. Even though any alphabetical identifier is allowed, the FTS projects normally use `rc` to identify a release candidate:

```
X.Y.Z.rcN  # Version X.Y.Z - Release Candidate N
```

It is important to not forget to properly identify release candidate versions because PyPI will not allow you to publish the same package version a second time.

If needed PyPI also allows to submit post releases. These releases should not bring software changes but you might need to use them if something goes wrong when publishing the initial release (for example, fixing documentation or release notes).

A post release is identified by the string `.post`, followed by a non-negative integer value.
```
X.Y.Z.postN  # Version X.Y.Z - Post-Release N
```

For more information on how to version a Python package refer to https://peps.python.org/pep-0440/#version-specifiers.

[1]: https://its.cern.ch/jira/projects/FTS/issues
[2]: https://its.cern.ch/jira/projects/FTS
[3]: https://fts-repo.web.cern.ch/fts-repo/rc
[4]: https://gitlab.cern.ch/fts/fts-jekyll
[5]: https://pypi.org/project/fts3/
[6]: https://realpython.com/python-wheels/
[7]: https://packaging.python.org/en/latest/guides/using-testpypi/
[8]: https://test.pypi.org/project/fts3/
