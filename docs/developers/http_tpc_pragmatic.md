# HTTP-TPC in Practice

This section will deep-dive into the HTTP-TPC details, showcasing all the
steps and mechanism that go into orchestrating an HTTP ThirdPartyCopy.

To fully demonstrate this, commands will be executed with the simplest `curl`
client, allowing everyone to reproduce them.

### Definition

#### Push vs Pull

An HTTP ThirdPartyCopy is a request initiated by a client with StorageA, asking
it to move a file between itself and StorageB:
  - When StorageA is the destination, this is called a **pull request**
  - When StorageA is the source, this is called a **push request**

When doing a **pull request**, the destination storage will download the file
from the source storage. This is done via an `HTTP GET` request.

When doing a **push request**, the source Storage will upload the file
to the destination storage. This is done via an `HTTP PUT` request.

> **Note:** Experience shows the **pull request**  is the preferred one. The general
consideration is that the destination knows better the resource constraints,
thus it is in the better position to give a meaningful response if the 
data movement cannot be done (e.g.: available resources exceeded).

#### Active party vs Passive party

The request is initiated by the client via the `HTTP COPY /<file-path>` method.
The storage which receives the COPY request is called the "active party". The
other involved storage will be the "passive party".

#### The COPY command

The `HTTP COPY /<file-path>` is addressed to the active party and triggers
the ThirdPartyCopy. The remaining details are specified via headers:
  - `Source: <source-URL>`: Where to download the file from (**pull request**)
  - `Destination: <destination-URL>`: Where to upload the file to (**push request**)
  - `Authorization: Bearer <token>`: Bearer token to authenticate with the active party
  - `TransferHeaderAuthorization: Bearer <token>`: Bearer token that the active party will use to authenticate with the passive party

#### The `TransferHeader` mechanism

During the HTTP-TPC, some information must be relayed from the triggering client
to the passive party. The convention is to use `TransferHeader<X>` naming.
Every `TransferHeader<X>` sent to the active party will be relayed to the
passive party as `<X>` header.

Example: The `TransferHeaderAuthorization` received by the active party 
will be sent as `Authorization` when the active party performs the request
with the passive party.

#### HTTP-TPC authentication (certificate delegation)

The next two sections are dedicated to the HTTP-TPC authentication. The triggering
client needs to have proper credentials with both the source and destination storage.
These credentials also have to be relayed from the active party to the passive party.

One possible approach is using x509 certificates and performing **certificate delegation**.
This means the triggering client delegates an x509 proxy certificate to the active party.
The active party storage will use this certificate when interacting with the passive party.
This solution is rather complex, as it requires the storages to perform SSL + x509 manipulation.
It also means the storage now has (a delegated copy of) your certificate, which implies
a rather high level of trust in the storage element. 

> Note: Not all Grid storages implemented HTTP-TPC with certificate delegation

#### HTTP-TPC authentication (bearer tokens)

A new solution was devised, namely to use bearer tokens. Bearer tokens are passed
via the `Authorization` header and are quite standard practice in the HTTP world.
This way, the triggering client is responsible with obtaining valid bearer tokens
for the active and passive parties. During the `HTTP COPY` command (and with the
help of the `TransferHeader` mechanism), the client can ensure bearer tokens are 
passed to both storages.

In theory, the bearer tokens can be anything. In practice, the bearer tokens used
are [macaroons][1] or JWT tokens. Within the Grid world, macaroons are widely used,
with a slow transition of transfers to JWT tokens. 

The initial client can perform a macaroon request with the storage. The client
presents an x509 certificate to authenticate, then requests a macaroon with
several capabilities (e.g.: read or write) for the chosen file. The storage
will reply back with the macaroon. From this moment on, the client can read/write
the file using the macaroon, instead, and no more x509 certificate is needed.
Before starting the HTTP-TPC, the client obtains macaroons for both the source
and the destination storages, allowing it to read/write the file.

#### Performance Markers

Finally, the last relevant concept is the "Performance Marker". Once the
client triggers an HTTP-TPC, it stays connected with the active party. 
Regularly (in practice, every ~5s), the active party will send back to the client 
status information on the data movement. This usually includes number of bytes
moved and time elapsed, but may also include the IP address of the involved 
passive party host.

### Anatomy of an HTTP-TPC request

This section will walk us through a complete HTTP-TPC request, demonstrated
with `curl`. At the end, the equivalent `gfal2` and `fts` commands will be shown.

The steps for a complete HTTP-TPC request are the following:
1. Obtain macaroon for the source storage (read permission)
2. Obtain macaroon for the destination storage (write permission)
3. Issue `HTTP COPY` command to active party
4. Monitor copy progress via "Performance Markers"

In order to execute the below commands, the following are required:
- `dteam` VO membership  
- EOSPPS access, which will be used as the source storage
- EOSPUBLIC access, which will be used as the destination storage

##### Environment setup

```bash
voms-proxy-init --voms dteam

export X509_USER_PROXY="/tmp/x509up_u$(id -u)"
export SRC_URL="https://eospps.cern.ch/eos/opstest/dteam/file.test"
export DST_URL="https://eospublic.cern.ch/eos/opstest/dteam/file.tpc"
```

##### I. Obtain macaroon for the source storage (read permission)

```bash
curl -s -X POST \
    --cert ${X509_USER_PROXY} \
    --key ${X509_USER_PROXY} \
    --cacert ${X509_USER_PROXY} \
    --capath /etc/grid-security/certificates/ \
    -H "Content-Type: application/macaroon-request" \
    --data '{"caveats": ["activity:LIST,DOWNLOAD"], "validity": "PT60M"}' \
  ${SRC_URL}

# Extract the 'macaroon' field into the ${SRC_MACAROON} variable
```

One-liner for convenience:

```bash
export SRC_MACAROON=$(curl -s -X POST --cert ${X509_USER_PROXY} --key ${X509_USER_PROXY} --cacert ${X509_USER_PROXY} --capath /etc/grid-security/certificates/ -H "Content-Type: application/macaroon-request" --data '{"caveats": ["activity:LIST,DOWNLOAD"], "validity": "PT60M"}' ${SRC_URL} | jq -r .macaroon)
```

##### II. Obtain macaroon for the destination storage (write permission)

```bash
curl -s -X POST \
    --cert ${X509_USER_PROXY} \
    --key ${X509_USER_PROXY} \
    --cacert ${X509_USER_PROXY} \
    --capath /etc/grid-security/certificates/ \
    -H "Content-Type: application/macaroon-request" \
    --data '{"caveats": ["activity:LIST,DOWNLOAD,MANAGE,UPLOAD,DELETE"], "validity": "PT60M"}' \
  ${DST_URL}

# Extract the 'macaroon' field into the ${DST_MACAROON} variable
```

One-liner for convenience:

```bash
export DST_MACAROON=$(curl -s -X POST --cert ${X509_USER_PROXY} --key ${X509_USER_PROXY} --cacert ${X509_USER_PROXY} --capath /etc/grid-security/certificates/ -H "Content-Type: application/macaroon-request" --data '{"caveats": ["activity:LIST,DOWNLOAD,MANAGE,UPLOAD,DELETE"], "validity": "PT60M"}' ${DST_URL} | jq -r .macaroon)
```

##### III. Issue `HTTP COPY` command to active party

**Pull request**

```bash
curl -s -L -X COPY \
    --cert ${X509_USER_PROXY} \
    --key ${X509_USER_PROXY} \
    --cacert ${X509_USER_PROXY} \
    --capath /etc/grid-security/certificates/ \
    -H "Source: ${SRC_URL}" \
    -H "Authorization: Bearer ${DST_MACAROON}" \
    -H "TransferHeaderAuthorization: Bearer ${SRC_MACAROON}" \
  ${DST_URL}
```

**Push request**

```bash
curl -s -L -X COPY \
    --cert ${X509_USER_PROXY} \
    --key ${X509_USER_PROXY} \
    --cacert ${X509_USER_PROXY} \
    --capath /etc/grid-security/certificates/ \
    -H "Destination: ${DST_URL}" \
    -H "Authorization: Bearer ${SRC_MACAROON}" \
    -H "TransferHeaderAuthorization: Bearer ${DST_MACAROON}" \
  ${SRC_URL}
```

One-liners for convenience:

```bash
# HTTP-TPC pull
curl -s -L -X COPY --cert ${X509_USER_PROXY} --key ${X509_USER_PROXY} --cacert ${X509_USER_PROXY} --capath /etc/grid-security/certificates/ -H "Source: ${SRC_URL}" -H "Authorization: Bearer ${DST_MACAROON}" -H "TransferHeaderAuthorization: Bearer ${SRC_MACAROON}" ${DST_URL}

# HTTP-TPC push
curl -s -L -X COPY --cert ${X509_USER_PROXY} --key ${X509_USER_PROXY} --cacert ${X509_USER_PROXY} --capath /etc/grid-security/certificates/ -H "Destination: ${DST_URL}" -H "Authorization: Bearer ${SRC_MACAROON}" -H "TransferHeaderAuthorization: Bearer ${DST_MACAROON}" ${SRC_URL}
```

##### IV. Monitor copy progress via "Performance Markers"

After the HTTP-TPC is started, the active party will regularly send "Performance Markers".
They usually have the following format:
```
Perf Marker
Timestamp: 1728647012
Stripe Index: 0
Stripe Bytes Transferred: 0
Total Stripe Count: 1
RemoteConnections: tcp:[<ip-address>]:443
End
```

### HTTP-TPC via Gfal2 CLI

The same above steps can be done using Gfal2 CLI tools. The below command
will obtain the macaroons and perform the HTTP-TPC request:

```bash
gfal-copy [-vv] [--just-copy] [--copy-mode <pull|push>] ${SRC_URL} ${DST_URL} 
```

Gfal2 command line options explained:
- The `-vv` increased the verbosity enough to print the HTTP requests and headers
- The `--just-copy` instructs Gfal2 to only obtain the macaroons and issue the `HTTP COPY` command, 
with no further checking whether destination file exists, overwrite, mkparent, etc...
- The `--copy-mode` handle to specify pull or push request (default pull) 

You can also use Gfal2 to obtain a macaroon from the storage endpoint:

```bash
gfal-token [--write] ${URL}
```

### HTTP-TPC via FTS

Finally, FTS always executes HTTP ThirdPartyCopy first. So submitting a transfer
to FTS is enough to trigger the data movement operation via HTTP-TPC:

```
fts-rest-transfer-submit -s https://fts3-pilot.cern.ch:8446/ ${SRC_URL} ${DST_URL}
```


[1]: https://theory.stanford.edu/~ataly/Papers/macaroons.pdf
