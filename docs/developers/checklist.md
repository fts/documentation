Release cross-check
===================

Patch releases with only a simple bugfix may go straight to production,
specially if the bug is being harmful.

For releases that include new functionalities, or a big set of bug fixes,
make sure these checks pass before pushing into production.

## Deployment
* The *artifacts* about to be released have been running in the pilot for enough time
* There has been at least *one* new node deployed in the pilot from scratch with the new version

## Validation
* Probes run against the pilot are green and stable
    * Errors are acceptable if their cause are properly understood
    * If the error is internal to FTS, a ticket must exist
* Functional tests pass
* Stress tests pass
* Migration scripts tested against dumps of other FTS instances
* Double check differences between MySQL versions (i.e default\_storage\_engine vs storage\_engine).

## Monitoring
* All nodes and services are properly monitored
* There are no critical log entries
* There are no error log entries, or they are understood (i.e. wrong logging level, transient errors,...)
    * If there are, a ticket should be created to fix them later
* Database load is acceptable
* Transfers are published in monit.cern.ch

## VO agreement
* ATLAS, CMS and LHCb have run transfers on the pilot
* ATLAS, CMS and LHCb give green light

## Final validation
* The release notes are up-to-date
* The release notes are published in [fts.web.cern.ch/releases](https://fts.web.cern.ch/fts/releases.html)
* fts3-steering is notified with the dates and expected impact
* Production qa nodes should be upgraded ~24 hours before the rest of the cluster *if possible*
* If there are database schema changes, validate them with samples from other WLCG FTS3 instances
  - At least for those that may have been manually modified (configuration tables)
* Double-check the Gfal2 versioned dependency is properly set
