Guidelines for C++
==================
While following similar guidelines for style is indeed nice, whether you put braces in the same
line or not, or if use tabs or not, is not really a killer.
Mostly, keep the style consistent within a function, file and logical module, in this order.
This is, try not to mix styles within a scope.

The following guidelines have little to do with this stylistics decisions, but are important
regardless of where the braces are. They have been mostly extracted from
[C++ Coding Standards](http://www.gotw.ca/publications/c++cs.htm), with some small
additions.

Still, if for consistency you would like to have a reference, at the end of this chapter
there are some very basics coments on style.

Specially important points are in bold.

  1. **Compile cleanly at high warning levels**
    * At least, for the code you control, so not to worry about external sources
  1. Use version control properly
    * **Meaningful commit messages**
    * **No jumbo or multipurpose commits**
  1. Follow the "Single responsibility principle" as much as possible
  1. **Correctness, simplicity and clarity come first**
    * Code is read way more often than written!
  1. Don't optimize prematurely
  1. **Minimize global and shared data**
  1. **Use RAII (specially smart pointers)**
  1. **Use `const` every time it makes sense**
    * Specially for parameters passed by reference or pointer, otherwise
without looking at the code is impossible to know what's going to happen to the object.
  1. **Avoid macros**
  1. **Avoid magic numbers**
    * Use a meaningful constant name
  1. **Declare variables as locally as possible**
    * i.e Do not create a data member that is used only internally by one method
  1. **Try to keep functions as short as possible**
  1. Avoid deep nesting
  1. Make headers self-sufficient
  1. Write `#include` guards, and optionally use `#pragma once`
  1. **Take parameters appropiately by value, pointer, or reference**
    * **Use `const` references for big types that will not be modified**
    * **Use pointers with semantics (reflect ownership)**
  1. Avoid providing implicit conversions
  1. Define and initialize member variables in the same order
  1. Prefer initialization to assigment in constructors
  1. Avoid calling virtual methods in constructors and destructors
  1. **Make base class destructors public virtual**
    * Otherwise leaks may occur!
  1. Destructors, deallocation and swap nevel fail
  1. Copy and destroy consistently
  1. Explicitly enable or disable copying
    * Very basic classes or struct which are mostly data-holders can ignore this
  1. **Do not write `using` in a header file or before an `#include`**
    * Seriously, don't, since they would pollute the namespace
  1. Avoid allocating and deallocating memory in different modules
    * Explicitly, this is. If smart pointers are used, then ownership is more clear.
  1. Don't write unintentionally nongeneric code
  1. Distinguish between errors and non-errors
  1. Design and wire error-safe code
  1. Prefer to use exceptions to report *errors*
  1. Throw by value, catch by const reference
  1. **Store only pointers as smart pointers in containers**
  1. Avoid `reinterpret_cast`
  1. Avoid `static_cast` on pointers
  1. Avoid casting away `const`
  1. Don't use C-style casts
  1. Don't memcpy or memcmp non-PODs

I would add:
  1. Outsource as much work to the compiler as possible, let it do its job
  1. **Do not copy and paste. Try to refactor.**
  1. **Components must have a clear responsability, and interact via their API's**
    * And not playing with each other's internal details 
  1. Try to foresee potential pieces of code that may have different valid implementations.
Coupling in these components hurt particularly bad.
    * For instance, the algorithm that takes decisions based on some data, and where and how
the data  is stored, should be kept separated
  1. **Do not abuse templates**
  1. Code must be understandable
    * **Use meaningful variable, function and type names**
      * The bigger the scope, the more descriptive (longer) the name should be
    * **Use the clearest and simplest construct that gets the job done**
    * **Declare variables near where they are going to be used**
  1. Do not repeat as comments what the code is doing
```c
// Return the sum of a and b
return a + b;
```
    * I can see that
  1. **Do** use comments for documenting
    1. **Functions and methods that are part of the public module/object API**
      * **What is supposed to do**
      * **Any special consideration of the parameters**
      * Obvious getters and setters can be skipped
    1. **Why something is/has been done** (specially workarounds)
      * For instance, `// We ignore this error code because A, B and C`
      * If there was a ticket for this, mention it. If not, there should probably have been

And last, but not least, remember the Boy Scout rule: 

> Always leave the campground cleaner than you found it.

## Coding style
Since style is a matter of personal taste[^1], just please keep consistency within a scope[^2].
Feel free to reformat a function or file you are modifying if it makes your work any easier.
Just remember to commit the reformatting and the actual change separately.

Good reads for style guides are:
  * [Linux kernel Coding Style](https://www.kernel.org/doc/Documentation/CodingStyle)
  * [Google C++ Guide](http://google-styleguide.googlecode.com/svn/trunk/cppguide.html)
  * [ID Code Style Conventions](ftp://ftp.idsoftware.com/idstuff/doom3/source/codestyleconventions.doc)
  * [C++ Core Guidelines](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md)
, by Bjarne Stroustrup himself.
    * If you are very bored and/or have a lot of time, really, since it is huge
    * Still ongoing

They do conflict with each other in some points, but they are interesting and battle-proven.

Just to avoid writting yet another guide, let's say you can basically
take as a reference the Google one, but have a look at the others anyway. They have really
good points (i.e. avoiding deep nesting)

Still, some details that are preferred for the FTS3 codebase:

### Indentation
  * Linux prefers tabs, set to 8
  * Google prefers 2 spaces
  * ID prefers 4 spaces

For FTS3, 4 spaces are recommended (ID style).
If you do use, regardless, tabs, do not mix them with spaces in any case!
(But please, do not use tabs)

### Braces
Personally, I also like K&R style, but most the Stroustrup variant:

```c
int main()
{
    while(x == y) {
        if (error) {
            do_something();
        }
        else {
            do_something_else();
        }
    }
}
```

Still, any [K&R](https://en.wikipedia.org/wiki/Indent_style#K.26R_style)
based style is just fine.

### Using directives
They are ok within .cpp files. Do **not** use them on header files.

### Exceptions
They are accepted, used and encouraged within FTS3. Of course, this means
using properly RAII to avoid leaks when an exception is thrown.

Catch an exception only if you know what can you do with it. Otherwise, let it
propagate upwards.

### Comments
Prefer `//` style comments. Use `///` for comments that document functions,
data members, etc... so Doxygen can pick them up.
If you like `/* */` more, that's ok too.

### Code on headers
Avoid it as much as possible. If you are using templates, there is pretty much
no way around it, but if there are no templates involved, the implementation
should go to its own .cpp file.

### Boilerplate
Copyright notice and license are a pain, but required. Here you have a template for
convenience:

```c
/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
```

If you see stuff like

```c
/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 */
```

that applies for code written in the [EMI](http://www.eu-emi.eu/) era.
Do not put it for new files.


## Lamentations
There are a couple of rules I would like to have, like:

  * Use of `override`
  * Use of `nullptr`

However, as long as we are stuck with GCC 4.4 in EL6 and 5, with its
[partial C++11 support](https://gcc.gnu.org/gcc-4.4/cxx0x_status.html),
we won't be able to use this. This is why we can't have nice things.

Still, we do have mutexes, threads and `auto`, so profit from that.


[^1] There is PEP-8 for Python, but there isn't anything similar for C++ anyway

[^2] Also, since there is a huge codebase already in place :)

