Installation guide
==================
This chapter includes documentation to setup and run an FTS3 service.
If you just want a set of steps to get the service up and running quickly, 
check the [Quick setup](install/quick.md) page.

Once you get the service up, you can check more detailed information on the different
details for each service:

  * [FTS3](install/fts3.md)
  * [Messaging](install/messaging.md)
  * [REST](../fts-rest/docs/install.md)
  * [Web Monitoring](install/fts3mon.md)
  * [Publishing GLUE2](install/glue2.md)
  * [User Management](install/user_management.md)

In case you want to use docker instead please refer to:
  * [FTS3 Docker](install/docker_containers.md)

Also, if you find yourself having trouble, have a look at the
[general recommendations](install/recommendations.md) and/or the
[troubleshooting guide](install/troubleshooting.md).

If you still have trouble, please, do not hesitate to contact us via
[fts-support@cern.ch](mailto:fts-support@cern.ch).

