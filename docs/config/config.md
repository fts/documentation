Configuration
=============

This documentation is about configuring the service parameters, as number
of actives, optimizer mode, etc...

For configuring the server (database connection, etc.), please, see
the [installation documentation](../install/fts3.md) instead.

## Global
These parameters can be configured globally (`*`) or per VO (i.e `dteam`).
If there is a configuration set for a VO, it will take precedence over
the global.

* **retry** Default number of retries per job. Can be overridden at submission time
* **max_time_queue** Default number of seconds a job can stay on the queue before
it is canceled automatically. Can be overridden at submission time
* **sec_per_mb** When a transfer has no timeout set, FTS will calculate it based
on the file size, multiplied by this factor, plus an offset to account for metadata
and checksum operations
* **global_timeout** Timeout to use if the transfer does not specify one. Leave it to
0 to let FTS calculate the timeout from the file size.
* **show_user_dn** If true, the user DN will be visible on the logs. It is
recommended to leave it as false

## Storage
These parameters can be configured globally (`*`) or per storage
(i.e. `gsiftp://host`).

If a storage has a specific configuration, always this one is used. Otherwise,
FTS will fallback to the wildcard setting.

The parameters are:

* **site** Site name
* **metadata** Free form text to bind to the storage configuration
* **ipv6** Set to true if you want to be able to use IPv6 for this storage.
* **udt** Set to true if you want to be able to use
[UDT](http://udt.sourceforge.net/) for this storage.
* **debug_level** Verbosity level
* **inbound_max_active** Maximum number of aggregated transfers inbound to this
storage
* **inbound_max_throughput** Limit the number of aggregated active inbound transfers
by throughput
* **outbound_max_active** Maximum number of aggregated transfers outbound from this
storage
* **outbound_max_throughput** Limit the number of aggregated active outbound transfers
by throughput

Since a transfer normally involves two storages, this configurations need to be
understood from this perspective. Also, some others are related to GFAL2 configuration
parameters.

### IPv6
The values can be `true`, `false` or `null` (undefined). Can be overridden per job.

1. If both are set to `true`, IPv6 will be enabled
2. If either one is explicitly set to `false`, IPv6 will be disabled
3. Otherwise, the setting for `*` will be used

These setting override the gfal2 specific values.

### UDT
Works in the same manner as IPv6. Mind that this setting only works for GridFTP.
The values can be `true`, `false` or `null` (undefined). Can *not* be overridden
per job.

1. If both are set to `true`, UDT will be enabled
2. If either one is explicitly set to `false`, UDT will be disabled
3. Otherwise, the setting for `*` will be used

These setting override the gfal2 specific values.

### Debug level
If any, or both, of the two storages involved on a transfer has this value set,
the maximum value will be used. Otherwise, if none is set, the value for `*`
will be used instead.
Can *not* be overridden per job.

For instance, if the source has a debug level of 2, and the destination of 1,
2 will be used.

## Link
A link can be configured between two endpoints (`source => destination`),
for only one (`source => *`, all destinations; `* => destination`, all sources),
or for all links (`* => *`).

The precedence order is:

1. `source => destination`
2. `source => *`
3. `* => destination`
4. `* => *`

The parameters are:

* **min_active** Minimum number of actives for the link
* **max_active** Maximum number of actives for the link
* **optimizer_mode** Optimizer mode for the link. Valid values are disabled,
conservative, normal, aggressive.
* **tcp_buffer_size** TCP buffersize
* **nostreams** Number of streams

You can leave as `null` (undefined) any of these parameters. The previous
precedence rule will be used in that case.

**Important**: When setting the minimum and maximum number of actives,
remind to validate it is consistent with the maximum number of outbound
(for the source) and inbound (for the destination) actives configured.
Otherwise, the storage limit may override the value set here.

## VO Shares
The number of actives for a given link can be shared between VOs by fair share
(default), or by weights. In this way, you can give a higher precedence to a given
VO on a given link.

The partition is done only for VOs with queued transfers, so if a higher priority
VO has no queued transfers, the other VOs can still use all available slots.

Example setting:

```
dteam: 10
atlas: 100
cms: 100
public: 5
```

Only the values for VOs with queued transfers are used, and normalized. This means,
if ATLAS and CMS have no queued transfers, only dteam and some other, then the
effective rates are:

```
dteam: 10 / 15 = 0.66
public: 5/15 = 0.33
```

*public* is a catch-all setting for all VOs not explicitly configured. Slots
assigned to it will be evenly-split across non configured VOs.

So, in the previous example, if there were three VOs (dteam, vo1, vo2), then

```
dteam: 0.66
vo1: 0.165
vo2: 0.165
```

**Caution**: If a VO is not configured, and there is no `public` value (or is 0),
all its transfers will fail since they can not be scheduled.

## Activity Shares
Once a slot is given to a given link and VO, this VO can prioritize its own
transfers using *activity shares*. This are basically a *tag* or *label*
assigned to each transfer, and then, depending on the weights, the slots are
split in the same manner as for the VO shares: only those queued, normalized.
The catch-all in this case is the `default` activity.
