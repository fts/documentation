WebFTS
======
WebFTS is a file transfer and management solution which allows users to invoke
reliable, managed data transfers on distributed infrastructures.

Created following simplicity and efficiency criteria, WebFTS allows the user to
access and interact with multiple storage elements. Their content becomes
browsable and different filters can be applied to get a set of files to be
transferred. Transfers can be invoked and capabilities are provided for checking
the detailed status of the different transfers and resubmitting any of them with
only one click.

The “transfer engine” used is FTS3, the service responsible for distributing the
majority of LHC data across WLCG infrastructure. This provides WebFTS with reliable,
multi-protocol (gridftp, srm, http, xrootd), adaptively optimised data transfers.

