Knowledge Base
==============

In this document we collect known issues and their workarounds, configuration recommendations, etc.

## Deployed Version
Only the last released version is supported. This means, there will be no fixes for a version 3.5, for instance, once 3.6 is already marked as 'production release', so it is strongly advised to run the latest version blessed with the 'production' stamp.

To know which one it is, you can visit the [Production Release](https://fts.web.cern.ch/sites/fts.web.cern.ch/themes/fts-webpage/releases-jekyll/releases/index.html) page.


### Globus libraries
FTS3 relies, of course, on other libraries. Globus is specially important, since it is the underlying library for GridFTP transfers, and authentication for other protocol (i.e SRM).
It is important to have an up to date version with the latest fixes, so it is strongly recommended to deploy the latest version available in EPEL stable.


## HTTP and XrootD TPC support

The support for XrootD and HTTP TPC in FTS is available since its first release, however only recently thanks to the WLCG DOMA TPC activities (https://twiki.cern.ch/twiki/bin/view/LCG/ThirdPartyCopy), a lot of enhancements have been implemented both on FTS (and the needed client libraries) and on the storages available in WLCG.

This means that we recommend to use HTTP and XRootD TPC only starting from recent versions of FTS and its dependencies.

Here the minimum software requirements:

- FTS v3.8.3

- gfal2-plugin-http and gfal2-plugin-xrootd v2.16.3

- Davix v0.7.2

- Xrootd-client v4.9.0

- x509-scitokens-issuer-client v0.7.0

All the above packages and dependencies are available in EPEL, except for the x509-scitokens-issuer-client which needs to be installed from the FTS repo. ( http://fts-repo.web.cern.ch/fts-repo/fts3-prod-el6.repo, http://fts-repo.web.cern.ch/fts-repo/fts3-prod-el7.repo)

There is only one configuration change to apply, in the file /etc/gfal2.d/http_plugin.conf:

ENABLE_STREAM_COPY=false

so HTTP stream copies are disabled and only TPC will be tried

## Default configuration
If you are running the latest FTS3 from [our repositories](http://fts-repo.web.cern.ch/fts-repo/), we already try to ship with reasonably good and performant defaults.
However, there are two things to take into account:

1. If you have modified manually any configuration file, they will *not* be overwritten when FTS3 is updated. So when upgrading, make sure you have a look at the release notes, and perhaps even diff your configuration against any `.rpmnew` file left by the installation.
2. Software that we do not ship (i.e. Apache), we can only recommend settings about them, but it is up to the sysadmin to apply them.


## FTS3 configuration files
Normally, `/etc/fts3/fts3config`, `/etc/fts3/fts3restconfig` and `/etc/fts3/fts-msg-monitoring.conf` are expected to be locally modified (at the very least, the database connections). They will *not* be overwritten by the newest one.

Other files may or may not be manually modified. If they haven't been, then the upgrade process will reset their defaults to the recommended ones. If they have been, then the sysadmin needs to have a look at the latest release notes, and the `.rpmnew` file.

These files are:

1. `/etc/fts3web/fts3web.ini`
2. `/etc/httpd/conf.d/fts3rest.conf`
3. `/etc/httpd/conf.d/ftsmon.conf`

### /etc/fts3web/fts3web.ini
Make sure `debug` is set to `false`

### /etc/httpd/conf.d/fts3rest.conf
You should be using the one provided in the rpm. Use it as a reference if you have your modified version.
It is recommended, in any case, to run mod_wsgi in daemon mode, limiting the `maximum-requests` to reduce the effect of "high watermark" effect in memory, and reduce any potential impact by memmory leaks.

### /etc/httpd/conf.d/ftsmon.conf
Same recommendations as for REST, although the value for `maximum-requests` should probably be lower.
It is strongly recommended to require a valid certificate to access the transfer logs via Apache.

## Apache hints
### MPM
MPM stands for "Multi-Processing Module". For Linux (the only supported platform for FTS3) there are three different options: [prefork](https://httpd.apache.org/docs/2.4/en/mod/prefork.html), [worker](https://httpd.apache.org/docs/2.4/en/mod/worker.html) and [event](https://httpd.apache.org/docs/2.4/en/mod/event.html).

We strongly suggest using the worker mpm for Apache 2.2, and either worker or event for Apache 2.4

The reason to discourage prefork is that one separate process is required per client, which increases the memory footprint.
Also, there are [known issues](https://its.cern.ch/jira/browse/FTS-343) when combining the mod_wsgi and httpd prefork versions available in RHEL6 and derivatives. The symptom are disappearing fts3rest and fts3wmon processes (mod_wsgi daemons).

### Modules
Even enabling worker or event, the default Apache configuration provided by RHEL6 and family is usually loaded with a lot of unnecessary modules (for FTS3, this is).
They increase the memory footprint of the Apache processes, so it is recommended to strip down the default configuration, removing unneeded modules (i.e. dav_module, cgi_module, speling_module, ...)
