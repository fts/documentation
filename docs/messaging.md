ActiveMQ Messaging
==================

FTS3 can publish transfer updates to an ActiveMQ broker, if configured to do so.
The [messaging configuration](install/messaging.md) section describes
how to enable FTS publishing of Monitoring Messages.
