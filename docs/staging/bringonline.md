The Bringonline Service
=======================

> Note: Starting with FTS 3.10, the Bringonline Service is replaced by the QoS Service.
  This documentation page is no longer maintained. 
  Please visit the QoS Service [documentation page](../qos-service/qos-service.md).

The Bringonline service is responsible for staging from tape, using the Gfal2 Bringonline API,
implemented for the SRM and XRootD (CERN-CTA) protocols.

The service is usually deployed on each node of the FTS cluster.
It queries the FTS Database in order to get files that have a staging action requested by the clients.

The service is optimized in order to send bulk requests to the storage system.

Each file is treated independently, so if the client wants to stage a bunch of files and then copy them to a 
destination, as soon as a single file part of a bulk request is available on disk, the file is queued 
for transfer and it will be eventually scheduled.

By checking the [State Machine](../state_machine.md) implemented by the Bringonline Service for each 
transfer, it's evident the link between the staging activity and the transfer activity.

Each transfer submitted by the client to be staged, has the `STAGING` state. Once the Bringonline daemon submits 
the staging request to the Storage, the transfer moves to the `STARTED` state. If the client requests only the 
staging of the file in the local buffer, the state either goes to `FAILED` or `FINISHED` depending on the 
outcome of the staging.
In case the client requests a further copy of the file to a different destination, once the staging is successfully
completed, the transfer state moves to the `SUBMITTED` state and then the `FTS Server Service` becomes responsible of the scheduling of
the  transfer as any other transfer scheduled by FTS3.

## Bringonline requests

Each Bringonline service instance every minute is retrieving the transfers that have been submitted for staging (`STAGING` state on the DB ) for each pair "voname, storage".

The service tries to send requests in bulk to the storages (200 files as bulk size by default). Since the clients 
can also requests the staging of few files at time, the Service before sending a request to the storage systems, waits for a configurable 
amount of time (5 min by default) to accumulate more files to send as bulk.

The number of active bringonline file requests per storage can be separately configured 
(more details in the [storage configuration](../../fts-rest/docs/config_alternatives.html).

In addition, the Service honours a maximum number of active bulk requests per storage (1000 by default). So by default, if each bulk request 
has a maximum of 200 files, the Service maintains max 200k bringonline requests per storage.

When either the configured storage limit or the max number of active bulk requests are reached, the Service stops sending bringonline requests
to the Storage, so the client requests are queued.

In the case of SRM protocol, together with the bringonline request the Service passes as parameter to the storage a copy pin lifetime.
The parameter expresses the lifetime of the disk copy on the storage system, once the file has been staged.
If not specified by the client, by default the Service uses a value of 28800 secs (8 hours)

N.B. The copy pin lifetime is not honoured by all storage implementation (i.e. Castor) therefore in some cases the disk copy can be garbage collected 
by the storage system to make room for new files regardless of the copy pin lifetime value configured by the client.

Starting with v3.9.4, once a file is successfully staged and transferred to the destination,
FTS will request a pin release on the SRM source storage.

## Bringonline polling

Once a staging request is accepted by the server, the service moves to *bringonline polling*.
This means the server will query the file to check whether it arrived on the disk buffer (i.e.: has a disk replica).
Polling is available for the SRM and XRootD (CERN-CTA) protocols and is done via bulk requests.
Polling time increases exponentially by a factor of 2, reaching a maximum of 10 minutes between polls.

The Service is also able to ignore bringonline polling errors up to a max configurable number (3 by default). This prevents to fail 
a long lasting request just for temporary network or storage glitches.

The behaviour of the Bringonline requests and polling are configurable, please check the [Configuration](configuration.md) 
section for more details. 
