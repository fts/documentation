Multihop
========

## What
Multihop transfers are intended to do transfers from point A to point B using an intermediate, or
several, C as relay.

## When
When the network between A and B is problematic or not performant enough, but the links between
A and C, and C and B are not.

Also, for protocol relays. For instance: S3 to an HTTP 3rd party copy compliant endpoint,
and then to a final GridFTP storage without HTTP 3rd party copy support.

```
s3://s3.site.com/path davs://intermediate.com/tmp
gsiftp://intermediate.com/tmp gsiftp://final.com/path
```

This would avoid tunneling transfers through the FTS3, which could be less performant depending
on the connectivity.

## How
Via the CLI, specify the `-m/--multihop` flag with `fts-transfer-submit` to make it explicit.

However the REST server can figure out it is a multihop job when there are several transfers,
and the destination of the transfer N is the source of the transfer N+1. This only works if the URL are *exactly* the same (it doesn't work for protocol relays).

Via REST, set the `multihop` parameter to `true`.

```json
{
    "files": [],
    "params": {
        "multihop": true
    }
}
```
