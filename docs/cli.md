Command Line Tools
==================
FTS3 can be used via a set [command line tools](cli/cli.md) called the FTS clients

We also provide some Python wrappers to facilitate the usage of the REST API. They are called
[easy bindings](fts-rest/docs/easy/README.md).
