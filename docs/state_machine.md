State machine
=============

## Job state machine
![Job state machine](jobstate.png "Job state machine")

  * *SUBMITTED* Initial state of a file as soon it's dropped into the database
  * *READY* One of the files within a job went to Ready state
  * *ACTIVE* One of the files within a job went to Active state
  * *FAILED* All files Failed
  * *FINISHED* All files Finished gracefully
  * *FINISHEDDIRTY* One or more files failed, but at least one succeeded
  * *CANCELED* Job canceled

## Transfer state machine
![Transfer state machine](transferstate.png "Transfer state machine")

  * *SUBMITTED* Initial state of a file as soon as it is dropped into the database
  * *READY* File is ready to become active
  * *ACTIVE* Transfer is running
  * *FAILED* Transfer failed, and the retries have been exhausted
  * *FINISHED* Transfer finished successfully
  * *STAGING* When staging of a file is requested
  * *STARTED* Bring online request sent to the server
  * *ARCHIVING* File is migrating to tape
  * *NOT_USED* For multiple replica jobs, those replicas that have not been used to transfer
  * *ON_HOLD* When either the storage or the destination is banned temporarily, so transfers won't go through.
  * *ON_HOLD_STAGING* Same as before, but for files that need to be staged first.
  * *CANCELED* Transfer has been canceled
