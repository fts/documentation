Appendix: Configure a GridFTP server
====================================
This documentation is for users that may be interested in copying data between
two storages, but may not have any server setup on the origin/destination
(i.e. NA62 use case).

This documentation is based on the notes from [Marco Boretto](https://gitlab.cern.ch/mboretto/).

## Manual Installation
### Add/enable required repositories

#### EGI Trustanchors
Create the file `/etc/yum.repos.d/egi-trustanchors.repo` with this content:

```
# EGI Software Repository - REPO META (releaseId,repositoryId,repofileId) - (12655,-,2283)

[EGI-trustanchors]
name=EGI-trustanchors
baseurl=http://repository.egi.eu/sw/production/cas/1/current/
enabled=1
gpgcheck=1
gpgkey=http://repository.egi.eu/sw/production/cas/1/GPG-KEY-EUGridPMA-RPM-3
```

#### EPEL
Make sure you have EPEL installed and enabled. For CentOS7

`yum install epel-release`

Note that if you are running a Scientific Linux machine, the file may be
provided by `sl-release` instead.

### Install the required RPMs
```bash
yum install ca-policy-egi-core
yum install globus-gridftp-server edg-mkgridmap globus-gridftp-server-progs
```

### Install host certificate and private key
The procedure to obtain them depends on where the machine is running. If
it is at CERN, you may use [ca.cern.ch](https://ca.cern.ch). Otherwise, you
will need to check with your institute.

An option may be [Let's Encrypt](https://letsencrypt.org/), but note this has
not been tested yet.

Put the host certificate under `/etc/grid-security/hostcert.pem`, and the
private key under `/etc/grid-security/hostkey.pem`. Make sure they are
owned by root, and have the mode `0600` (readable only by root).

### Grant access to your robot user/users
Now you are ready to run the service, but no one will be able yet to use it.
To grant access, you will need to create two files: `/etc/edg-mkgridmap.conf`
and `/etc/localgridmap.conf`. The first one will contain the configuration for
`edg-mkgridmap`, and the second one a set of hardcoded users' dn that will
be able to log in.

Ideally, you could use a Virtual Organization to group your users and `edg-mkgridmap`
would then automatically populate the list from it.

The content of `edg-mkgridmap.conf` should be

```conf
## This will populate the list from a VOMS server. This example if for the dteam
## VO.
# group "vomss://voms.hellasgrid.gr:8443/voms/dteam" dteam
## Hardcoded mappings
gmf_local /etc/localgridmap.conf
```

And for `localgridmap.conf`, a list of users' dn and local account. For
instance:

```conf
"/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=ftssuite/CN=737188/CN=Robot: fts3 testsuite" nobody
```

You can now run manually

```bash
edg-mkgridmap --conf=/etc/edg-mkgridmap.conf --output=/etc/grid-security/grid-mapfile
```

If everything goes well, `/etc/grid-security/grid-mapfile` will contain the mappings
you configured in `localgridmap.conf`, and, if you configured any vo, users
retrieved from it.

Add a cronjob to run this command periodically, and keep the list up to date
(not really required if you are only using hardcoded users).

```
17 */2 * * * /usr/sbin/edg-mkgridmap --conf=/etc/edg-mkgridmap.conf --output=/etc/grid-security/grid-mapfile --safe --cache —quiet
```

**Note:** The accounts into which you are mapping your users must exist on the
system, so make sure to run `adduser` or `addgroup` (or similar) accordingly.

### Firewall
If you are running a firewall (which you should), make sure you open the GridFTP
ports.

By default, port `2811` is used for the control channel, and the range `50000-51000`
for the data channels.

You can change both under `/etc/gridftp.conf`, via `port` and `$GLOBUS_TCP_PORT_RANGE`
respectively.

`ufw` can be handy to easily open these ports:

```bash
yum install ufw
ufw allow ssh # Make sure you don't lock yourself out!
ufw allow 2811/tcp
ufw allow 50000:51000/tcp # Or the combination you've chosen
ufw enable
```
### Enable and start the server
First, you may want to restrict which paths are accessible via GridFTP. On
`/etc/gridftp.conf`, for instance, add:

```
restrict_paths /data,/srv
```

And then enable and start:

```bash
chkconfig globus-gridftp-server on
service globus-gridftp-server start
```

### Test the server
From another machine, you may use `uberftp`, `globus-url-copy`, or `gfal2`.

```bash
gfal-ls gsiftp://host/
```
