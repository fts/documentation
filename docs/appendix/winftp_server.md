Appendix: Windows FTP Server
============================
Since [gfal 2.11](http://dmc.web.cern.ch/release/gfal2-2.11.0), the GridFTP
plugin has supported, experimentally, [plain FTP](https://its.cern.ch/jira/browse/DMC-763).

With this, gfal2, so FTS too, can communicate with vanilla FTP servers, as the
one available for Windows.

## Install IIS FTP Server
1. Open "Control Panel / Programs and Features"
2. Click on "Turn Windows features on or off"
3. Expand "Internet Information Services / Web Management Tools"
4. Mark everything under "FTP Server", and "IIS Management Console"
5. Click "Ok"

![FTP Server Install](winftp_install.png "Install FTP server")

Once this is done, the software is installed. Now we need to allow access on the
firewall, and add a new site.

## Allow access
1. Open "Control Panel / Windows Firewall"
2. Click on "Allow an app or feature through Windows Firewall"
3. Click on "Change settings" if required
4. Look for "FTP", and enable both "Private" and "Public" access.
5. Click "Ok"

![FTP Firewall](winftp_firewall.png "Configure the firewall")

Now the server is installed, and access allowed through the firewall.
We need to add and configure a new site.

## Add a new site
Now, we need to configure a folder to be accessible via FTP.

1. Go to the "Control Panel" and then, "Administrative Tools"
2. Open "Internet Information Services (IIS) Manager"
3. Expand "Sites" on the tree on the left
4. Right-click on "Sites" and select "Add FTP Site"
5. Select a name for the site (any meaningful name for you), and select the
folder you want to make accessible. Click "Next"
6. You can leave, in principle, the default values for "Binding"
7. Make sure you have checked "Start FTP site automatically"
8. Mark "No SSL", and click "next"
9. On the next window, you can select the authentication/authorization methods
for the server. "Anonymous" will allow anyone to connect (not necessarily to read
the files!). Do **not** allow anonymous access for servers accessible from the Internet
10. Click "Finish" to create the new site

To make sure this site works well with gfal2, you will need to enable Unix-like
directory listing, which 4 digit years.

1. Click on your new site
2. Double-click on "FTP Directory Browsing"
3. Select "Unix" for "Directory Listing Style", and "Four digit  years"

![FTP Unix](winftp_unix.png "Unix directory listing")

## Allow remote copies
The [File eXchange Protocol](https://en.wikipedia.org/wiki/File_eXchange_Protocol)
allows FTP servers to transfer files directly between them, playing with the
"Active/Passive" connection modes.

For security reasons, normally FTP servers do not allow a client to tell the
server to connect to a third party IP, since this could be used to scan the
internal network to an external attacker.

However, to copy files between an FTP and another GridFTP server we need to
enable this functionality.

It is **not** recommended to enable this setting on a server reachable by
anyone on the Internet. For machines inside CERN's LAN, as long as they are not
open to the outside, this is reasonable secure. Of course, this means these
machines can only be reached by an FTS running at CERN too.

For machines outside CERN's LAN, please, contact us and we will try to see
if there are better ways. For instance, [client certificates](https://docs.microsoft.com/en-us/iis/configuration/system.applicationhost/sites/site/ftpserver/security/authentication/clientcertauthentication),
although this hasn't been tested with gfal2.

To enable this functionality, from inside the "IIS Manager":

1. Click on the machine name (root of the tree on the left)
2. Click on "Configuration Editor"
3. On "Section", select "system.applicationHost" and then "sites"
4. Expand "siteDefaults/ftpServer/security/dataChannelSecurity"
5. Set both values to "false"

![FTP Server Channels](winftp_channel.png "Configure channels")

Now, you can try gfal2-util from some other node to make sure all the settings
work.
