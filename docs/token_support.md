OAuth2 Token Support
====================

Recent versions of the FTS software offer a large level of OAuth2 token support.
Initial versions (starting with FTS v3.10.0) demonstrated token-based transfers.
In FTS v3.13.0, the token-support was completely reworked and released as a beta-version.
The future FTS v3.14.0 version is expected to bring the final level of OAuth2 token support.

## General Considerations

Offering OAuth2 token support is part of the larger (WLCG, but not only)
goal of moving away from X509 proxy certificates. Moving to token-based transfers
would allow transfers to rely on bearer tokens used for authorization, as opposed
to current identity-based certificate access. Furthermore, tokens allow more
granular access, restricting capabilities to only one operation (e.g.: read, write),
or even more granular, a given path prefix or an exact path.

The tokens used in WLCG, generally accepted by FTS, are a variant of JWT tokens,
which means the token is composed of 3 parts: header, payload and signature.
They are also bearer tokens, as they grant access to the resource to whoever
holds and presents the token to the resource server. In this whole ecosystem,
an entity will provide the tokens, called TokenProvider. In the WLCG world,
that TokenProvider will be based on the [IAM implementation][1]. Besides IAM,
FTS has also shown integration with EGI CheckIn and attempts are made
to integrate it also with Fermilab's CILogon service.

Before proceeding to FTS token submission details, certain concepts have to be defined.

**Scope**

The mechanism by which tokens grant access to only certain capabilities is called
**scope**. The **scope** is a field in the JWT token whose value is usually
recognized across the entire ecosystem. Example of a scope granting read access
to a file provided by a WLCG storage endpoint:
```javascript
  "scope": "storage.read:/<path>/<file>"
```

**Audience**

Tokens may also have an optional field called audience (`aud`). Although optional,
when present, it specifies the resource to whom this token is addressed. Any service
which is not the intended resource must reject the token and fail authorization.

```javascript
  "aud": "fts3-pilot.cern.ch"
```

**Client ID and Client Secret**

When registering with a TokenProvider, at the end of the registration process,
you should be given a client-id and client-secret pair. This effectively acts
as your username and password for your service, where the `client_id` is the
public part and the `client_secret` is the secret part. This client id/secret pair
allows you to authenticate with the TokenProvider. Depending on what permissions/grants
you have, you would be able to obtain access tokens and/or execute certain workflows.

FTS is also registered as a service with each TokenProvider it supports
and has a client id/secret pair of its own, which the FTS administrators handle.

**Access token**

An access token is the token emitted by the TokenProvider, containing the
desired `scope` and `aud` fields. It will be used by the client to obtain
access to a given resource, from the resource server. In the case of FTS transfers,
the client will submit access tokens to the FTS service. Access tokens are usually short-lived,
their lifetime being measured in minutes (20 minutes, at one extreme)
to hours (12 hours, at the other extreme).

**Refresh token**

A refresh token is a special kind of token, tied to an access token. The refresh
token allows renewing the access token within the allowed scope and audience boundaries.
A refresh token (at least in IAM implementation) is also bound to the `client_id`
which requested it, which is why refresh tokens cannot be passed around. A refresh
token's lifetime is usually measured in days, with 30 days being a common value.

**Token Exchange**

The token world allows for a couple of workflows/grants to be executed. Not all of them
are given to all clients. FTS, as a service moving data on behalf of other clients,
needs certain privileges such as the `token-exchange` grant. Usually, this grant
is given explicitly by the TokenProvider administrators. A client with the
`token-exchange` grant is able to exchange an access token from a different client
and receive a refresh token.

FTS needs the `token-exchange` ability as access tokens are short-lived,
whereas refresh tokens usually last days, if not more. Given FTS cannot schedule
all transfers within the lifetime of the initial access token, it needs to be able to
obtain new access tokens when it needs them. This is done via refresh tokens
and the `token-exchange` grant.

**Token Refresh**

The `token-refresh` workflow/grant is the mechanism through which a client
in possession  of an access token and associated refresh token
can obtain a new access token, bound by the same allowed scopes and audience
restrictions as for the initial access token.

FTS makes heavy use of the `token-refresh` grant in order to ensure valid
access tokens will be available at the moment the data transfer is executed.

**Identity and VO membership**

As opposed to previous notions, which are more token-generic, the identity
and VO membership are concepts more related to WLCG and the way FTS operates.
FTS needs to associated ownership of the transfers to an identity, which is
usually defined as user identity + VO membership.

In the world of certificates, the `user identity = certificate DN` and
`VO membership = VO field (from VOMS data)`. In the world of OAuth2 tokens,
the `user identity = sub field` and `VO membership = issuer field`.

## Submitting Token-based Transfers

Transfers can be submitted to FTS using either the FTS CLI client or directly
via the submission REST API. The distinction between token-based and certificate-based
transfers is done at submission time. If a token is present in the
`Authorization: Bearer <token>` header, then the submission is considered
token-based. If not, then FTS will look for an X509 certificate.

An FTS token-based submission requires the following:
- An **FTS submission token**, used to identify the client
(via `sub` and `issuer` fields for user identity and VO)
- A **source token** for each transfer source
- A **destination token** for each transfer destination

Example using the FTS CLI:

```bash
$ fts-rest-transfer-submit --fts-access-token <fts-token> \
                           --src-access-token <src-token> \
                           --dst-access-token <dst-token> \
    -s https://fts3-pilot.cern.ch:8446/ <src> <dst>
```

Example submitting JSON directly via the REST API:
```bash
$ curl -X POST -H "Content-Type: application/json" -H "Accept: application/json" \
               -H "Authorization: Bearer <fts-token>" \
                --data @submission.json \
       https://fts3-pilot.cern.ch:8446/jobs

> POST /jobs HTTP/1.1
> Host: fts3-pilot.cern.ch:8446
> Content-Type: application/json
> Accept: application/json
> Authorization: Bearer <fts-token>
{
    "files": [
        {
            "sources": [ "<src>" ],
            "destinations": [ "<dst>" ],
            "source_tokens": [ "<src-token>" ],
            "destination_tokens": [ "<dst-token>" ],
        }
    ]
}
```

### FTS token-specific considerations

FTS expects **ALL tokens** to be JWTs, as well as to have the following timestamp
fields: `exp`, `iat` and `nbf`. FTS will only verify and validate the `<fts-token>`.
For the source and destination tokens, FTS does its best to treat them as opaque
throughout the whole system.

Depending on the FTS3 instance configuration, additional requirements may be
put on the `<fts-token>`. When configured to do so:
- The `fts` scope may be mandatory (`"scope": "fts"`)
- The audience may be mandatory and must be set to what the FTS instance
requires as audience. Usually, it's the FTS instance alias or "WLCG/any"  
  (`"aud": "fts3-pilot.cern.ch"` OR `"aud": "https://wlcg.cern.ch/jwt/v1/any"`)


[1]: https://indigo-iam.github.io/v/v1.7.1/docs/overview/
