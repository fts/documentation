# BDII
Berkley Database Information Index

# CURL
curl is an open source command line tool and library for transferring data with
url syntax.

# EPEL
Extra Packages for Enterprise Linux.

# `fts_url_copy`
FTS3 binary that actually runs the transfers. Built on top of gfal2.

# JSON
An open standard format that uses human-readable text to transmit data objects
consisting of attribute–value pairs.

# LHC
Large Hadron Collider

# MyOSG
One-Stop location for various OSG information

# OSG
Open Science Grid

# RHEL
RedHat Enterprise Linux

# SRM
Storage Resource Manager

# WLCG
Worldwide LHC Computing Grid

# CTA
CERN Tape Archive: XRootD tape system developed at CERN
