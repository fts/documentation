#!/bin/bash

function usage {
  echo "Generate FTS documentation from MD files. Requires \"gitbook v3.2.3\" environment to be already set up."
  echo ""
  echo "usage: $(basename $0) [<destination>]"
  echo "       <destination>  -- destination folder (default = \"/eos/workspace/f/fts/repo/www/docs-devel/\")"
  echo ""
  
  exit 1
}

if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
  usage
fi

set -ex
AREA="/eos/workspace/f/fts/repo/www/docs-devel"

if [[ $# -eq 1 ]]; then
  AREA="$1"
fi

mkdir -p "${AREA}"
echo "Generating FTS documentation to: ${AREA}"
./node_modules/gitbook-cli/bin/gitbook.js build --log=info --format=website "${PWD}" "${AREA}"

cat > "${AREA}/.htaccess" <<EOF
Options +Indexes
EOF
