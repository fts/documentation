#!/usr/bin/env bash
set -e

mkdir fts-rest/
pushd fts-rest > /dev/null

git init --quiet
git remote add --fetch origin "https://gitlab.cern.ch/fts/fts-rest.git" &> /dev/null
git config core.sparseCheckout true
echo "docs" >> .git/info/sparse-checkout
git pull --quiet origin develop

rm -rf .git
popd > /dev/null
