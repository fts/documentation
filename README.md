# FTS3 Documentation

FTS3 is the service responsible for globally distributing the majority of the
LHC data across the WLCG infrastructure. It is a low level data movement service,
responsible for reliable bulk transfer of files from one site to another while
allowing participating sites to control the network resource usage.

This documentation is for FTS v3.12. For previous versions, you can have a look
at the [FTS documentation archive](http://fts-docs-archive.web.cern.ch/fts-docs-archive/).
